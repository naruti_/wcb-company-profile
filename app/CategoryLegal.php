<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryLegal extends Model
{
  protected $table='category_legals';

  protected $fillable = [
      'name','created_at','update_at'
  ];

  public function legals()
  {
    return $this->hasMany(\App\Legal::class, 'id_category_legal');
  }
}
