<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Gallery extends Model
{
  protected $table='galeries';

  protected $fillable = [
      'name','img_name','img_ext','created_at','update_at', 'desc'
  ];
}
