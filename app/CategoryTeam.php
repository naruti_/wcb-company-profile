<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Team;


class CategoryTeam extends Model
{
  protected $table='category_teams';

  protected $fillable = [
      'name','created_at','update_at'
  ];

  public function teams()
  {
    return $this->hasMany(\App\Team::class, 'id_category_team');
  }
}
