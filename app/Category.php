<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
  protected $table='categories';

  protected $fillable = [
      'name','created_at','update_at'
  ];


	public function portfolios()
	{
		return $this->hasMany(\App\Portfolio::class, 'id_category');
	}
}
