<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Structure extends Model
{
    protected $table='structures';

    protected $fillable = [
        'name','created_at','update_at'
    ];
}
