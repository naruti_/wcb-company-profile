<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryRegulation;

class Regulation extends Model
{
  protected $table='regulations';

  protected $fillable = [
      'name','id_category_regulation','filename','fileext','created_at','update_at'
  ];

  public function category_regulation()
  {
    return $this->belongsTo(\App\CategoryRegulation::class, 'id_category_regulation');
  }
}
