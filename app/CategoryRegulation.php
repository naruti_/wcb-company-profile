<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Regulation;

class CategoryRegulation extends Model
{
  protected $table='category_regulations';

  protected $fillable = [
      'name','created_at','update_at'
  ];

  public function regulations()
	{
		return $this->hasMany(\App\Regulation::class, 'id_category_regulation');
	}
}
