<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Auth;

use App\Category;

class CategoryController extends Controller
{
  public function __construct()
  {
      // $this->middleware('auth');
  }

  public function index()
  {
      $categories = Category::all();
      return view('admin.categories.index',compact('categories'));
  }

  public function storeCategory(Request $request)
  {
      $data = $request->all();

      $newCategory = new Category;
      $newCategory->fill($data);
      $newCategory->save();
      $request->session()->flash('alert-message', 'Kategori berhasil ditambahkan');
      $request->session()->flash('alert-class', 'alert alert-success');
      $request->session()->flash('alert-icon', 'check');


      return redirect()->back();
 }

 public function editCategory($id)
 {
      // if(empty($re))
     $category = Category::find($id);

     return response()->json($category);
 }

 public function updateCategory(Request $request, $id)
 {
     $category = Category::find($id);
     if($category){
       $category->name = $request->name;
       $category->save();
     }
     $request->session()->flash('alert-message', 'Kategori berhasil diupdate');
     $request->session()->flash('alert-class', 'alert alert-success');
     $request->session()->flash('alert-icon', 'check');


     return redirect()->back();
 }

 public function deleteCategory($id)
 {
     $category = Category::find($id);
     if($category){
       $category->delete();
     }
     $request->session()->flash('alert-message', 'Kategori berhasil dihapus');
     $request->session()->flash('alert-class', 'alert alert-danger');
     $request->session()->flash('alert-icon', 'check');


     return redirect()->back();
 }

}
