<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Structure;
use App\Certificate;

use App\CategoryLegal;
use App\CategoryTeam;
use App\CategoryRegulation;
use App\Category;

use App\Legal;
use App\Team;
use App\Gallery;
use App\Portfolio;
use App\Regulation;




class FrontendController extends Controller
{
  public function index(){
    return view('frontend.beranda');
  }
  public function profil(){
    return view('frontend.profil');
  }

  public function struktur(){
    $struktur = Structure::first();
    // dd($struktur);
    return view('frontend.struktur',compact('struktur'));
  }

  public function sertifikat(){
    $sertifikats = Certificate::all();
    // dd($struktur);
    return view('frontend.sertifikat',compact('sertifikats'));
  }

  public function legalitas(){
    $categories = CategoryLegal::all();
    $legals = Legal::all();
    // dd($struktur);
    return view('frontend.legalitas',compact('categories','legals'));
  }

  public function timAhli(){
    $categories = CategoryTeam::all();
    $teams = Team::all();
    return view('frontend.tim',compact('categories','teams'));
  }
  public function lingkupPekerjaan(){
    return view('frontend.lingkup');
  }
  public function partner(){
    return view('frontend.partner');
  }
  public function galeri(){
    $galeries = Gallery::all();
    return view('frontend.galeri',compact('galeries'));
  }
  public function portofolio(){
    $categories = Category::all();
    $portfolios = Portfolio::all();
    return view('frontend.portofolio',compact('categories','portfolios'));
  }

  public function portofolioFilter($id){
    // dd($id)
    $categories = Category::all();

    $categoryFiltered = Category::findOrFail($id);
    $portfolios = $categoryFiltered->portfolios;
    // dd($portfolios);
    // $portfolios = Portfolio::all();
    return view('frontend.portofolioFilter',compact('categories','portfolios'));
  }

  public function peraturan(){
    $categories = CategoryRegulation::all();
    $regulations = Regulation::all();
    return view('frontend.peraturan',compact('categories','regulations'));
  }

  public function kontak(){
    return view('frontend.kontak');
  }
  public function terms(){
    return view('frontend.terms');
  }
  public function privacy(){
    return view('frontend.privacy');
  }

}
