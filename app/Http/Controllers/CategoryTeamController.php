<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\CategoryTeam;


class CategoryTeamController extends Controller
{
  public function store(Request $request)
  {
      $data = $request->all();

      $newCategory = new CategoryTeam;
      $newCategory->fill($data);
      $newCategory->save();
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-message', 'Kategori berhasil ditambahkan');
      $request->session()->flash('alert-class', 'alert alert-success');
      $request->session()->flash('alert-icon', 'check');


      return redirect()->back();
 }

 public function edit($id)
 {
      // if(empty($re))
     $category = CategoryTeam::find($id);

     return response()->json($category);
 }

 public function update(Request $request, $id)
 {
     $category = CategoryTeam::find($id);
     if($category){
       $category->name = $request->name;
       $category->save();
     }
     $request->session()->flash('alert-type', 2);
     $request->session()->flash('alert-message', 'Kategori berhasil diupdate');
     $request->session()->flash('alert-class', 'alert alert-success');
     $request->session()->flash('alert-icon', 'check');


     return redirect()->back();
 }

 public function delete(Request $request, $id)
 {
     $category = CategoryTeam::find($id);
     if($category){
       $category->delete();
     }
     $request->session()->flash('alert-type', 4);
     $request->session()->flash('alert-message', 'Kategori berhasil dihapus');
     $request->session()->flash('alert-class', 'alert alert-danger');
     $request->session()->flash('alert-icon', 'check');


     return redirect()->back();
 }
}
