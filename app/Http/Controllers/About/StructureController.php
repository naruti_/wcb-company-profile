<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Structure;

class StructureController extends Controller
{
  public function index()
  {
      $struktur = Structure::first();
      return view('admin.about.structure_index',compact('struktur'));
  }

  public function upload(Request $request)
  {
      $this->validate($request,[
        'img_structure' => 'required|mimes:jpeg,jpg,png'
      ],[
        'mimes' =>'Upload gambar galeri hanya dengan gambar dengan extension .jpg,.jpeg, atau .png saja'
      ]);
      $image = $request->file('img_structure');
      $imageName = 'struktur-organisasi.'.$image->getClientOriginalExtension();
      $imageStruktur = Structure::first();
      if(empty($imageStruktur)){
        $imageStruktur = new Structure;
        echo "masuk";

      }
      $imageStruktur->name = $imageName;
      $imageStruktur->save();
      $image->storeAs('public/images/struktur',$imageName);
      $request->session()->flash('alert-message', 'Struktur organisasi berhasil diupload');
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-icon', 'check');
      return back();
  }

  public function delete(Request $request)
  {
    $struktur = Structure::first();
    if($struktur){
      $struktur->delete();
      $request->session()->flash('alert-message', 'Struktur organisasi telah dihapus');
      $request->session()->flash('alert-type', 4);
      $request->session()->flash('alert-icon', 'check');
    }
    return back();
  }

}
