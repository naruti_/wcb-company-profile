<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Partner;


class PartnerController extends Controller
{
  public function index()
  {
      $partners = Partner::all();
      return view('admin.about.partner_index',compact('partners'));
  }

  public function store(Request $request){
    $data = $request->all();
    $newPartner = new Partner;
    $newPartner->fill($data);
    $newPartner->save();
    $request->session()->flash('alert-message', 'Partner berhasil ditambahkan');
    $request->session()->flash('alert-type', 2);
    $request->session()->flash('alert-icon', 'check');
    return redirect()->back();

  }

  public function edit($id){
    $partner = Partner::find($id);
    return response()->json($partner);
  }

  public function update(Request $request, $id){
    $partner = Partner::find($id);
    if($partner){
      $data = $request->all();
      $partner->fill($data);
      $partner->save();
      $request->session()->flash('alert-message', 'Partner berhasil Diperbaharui');
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-icon', 'check');

    }


    return redirect()->back();
  }


  public function delete(Request $request, $id)
  {
      $partner = Partner::find($id);
      if($partner){
        $partner->delete();
        $request->session()->flash('alert-message', 'Partner telah dihapus');
        $request->session()->flash('alert-type', 4);
        $request->session()->flash('alert-icon', 'check');
      }
      return redirect()->back();
  }
}
