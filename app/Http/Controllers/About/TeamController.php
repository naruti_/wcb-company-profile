<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CategoryTeam;
use App\Team;

class TeamController extends Controller
{
  public function index()
  {
      $categories = CategoryTeam::all();
      $teams = Team::all();
      // dd($categories);
      return view('admin.about.team_index',compact('categories','teams'));
  }

  public function store(Request $request)
  {


    $newTeam = new Team;

    $data = $request->all();
    $newTeam->fill($data);
    $newTeam->save();
    $request->session()->flash('alert-message', 'Tim berhasil ditambahkan');
    $request->session()->flash('alert-type', 2);
    $request->session()->flash('alert-icon', 'check');

    return redirect()->back();
  }

  public function edit($id)
  {
      $team = Team::find($id);
      return response()->json($team);
  }

  public function update(Request $request, $id)
  {
      $team = Team::find($id);
      if($team){
        $data = $request->all();
        $team->fill($data);
        $team->save();
      }
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-message', 'Tim berhasil diupdate');
      $request->session()->flash('alert-icon', 'check');


      return redirect()->back();
  }

  public function delete(Request $request,$id)
  {
      $team = Team::find($id);
      if($team){
        $team->delete();
        $request->session()->flash('alert-type', 4);
        $request->session()->flash('alert-message', 'Tim berhasil dihapus');
        $request->session()->flash('alert-icon', 'check');
      }


      return redirect()->back();
  }
}
