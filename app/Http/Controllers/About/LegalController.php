<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\CategoryLegal;
use App\Legal;

class LegalController extends Controller
{
  public function index()
  {
      setlocale(LC_ALL, 'IND');
      $categories = CategoryLegal::all();
      $legals = Legal::all();
      // dd
      // dd($categories);
      return view('admin.about.legal_index',compact('categories','legals'));
  }

  public function store(Request $request)
  {


    $newTeam = new Legal;

    $data = $request->all();
    // dd($data);
    $newTeam->fill($data);
    $newTeam->save();
    $request->session()->flash('alert-message', 'Tim berhasil ditambahkan');
    $request->session()->flash('alert-type', 2);
    $request->session()->flash('alert-icon', 'check');

    return redirect()->back();
  }
  public function edit($id)
  {
      $legal = Legal::find($id);
      return response()->json($legal);
  }

  public function update(Request $request, $id)
  {
      $legal = Legal::find($id);
      if($legal){
        $data = $request->all();
        $legal->fill($data);
        $legal->save();
      }
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-message', 'Tim berhasil diupdate');
      $request->session()->flash('alert-icon', 'check');


      return redirect()->back();
  }

  public function delete(Request $request,$id)
  {
      $legal = Legal::find($id);
      if($legal){
        $legal->delete();
        $request->session()->flash('alert-type', 4);
        $request->session()->flash('alert-message', 'Tim berhasil dihapus');
        $request->session()->flash('alert-icon', 'check');
      }


      return redirect()->back();
  }
}
