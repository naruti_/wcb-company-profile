<?php

namespace App\Http\Controllers\About;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Certificate;


class CertificateController extends Controller
{
  public function index()
  {
      $certificates = Certificate::all();
      return view('admin.about.certificate_index',compact('certificates'));
  }

  public function store(Request $request){
    $data = $request->all();
    $newCertificate = new Certificate;
    $newCertificate->fill($data);
    $newCertificate->save();
    $request->session()->flash('alert-message', 'Sertifikasi berhasil ditambahkan');
    $request->session()->flash('alert-type', 2);
    $request->session()->flash('alert-icon', 'check');
    return redirect()->back();

  }

  public function edit($id){
    $certificate = Certificate::find($id);
    return response()->json($certificate);
  }

  public function update(Request $request, $id){
    $certificate = Certificate::find($id);
    if($certificate){
      $data = $request->all();
      $certificate->fill($data);
      $certificate->save();
      $request->session()->flash('alert-message', 'Sertifikasi berhasil Diperbaharui');
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-icon', 'check');

    }


    return redirect()->back();
  }


  public function delete(Request $request, $id)
  {
      $certificate = Certificate::find($id);
      if($certificate){
        $certificate->delete();
        $request->session()->flash('alert-message', 'Sertifikasi telah dihapus');
        $request->session()->flash('alert-type', 4);
        $request->session()->flash('alert-icon', 'check');
      }
      return redirect()->back();
  }

}
