<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Portfolio;

class PortfolioController extends Controller
{
    public function __construct()
    {
        // $this->middleware('auth');
    }

    public function index()
    {
      $categories = Category::all();
      $portfolios = Portfolio::all();
      // dd($categories);
        return view('admin.portfolios.index',compact('categories','portfolios'));
    }

    public function storePortfolio(Request $request)
    {

      $this->validate($request,[
        'year' => 'required|date_format:"Y"'
      ],[
        // 'mimes' =>'Upload file peraturan hanya dengan file extension .pdf, .doc, .docx, .ppt, .pptx saja'
      ]);
      $newCategory = new Portfolio;

      $data = $request->all();
      $newCategory->fill($data);
      $newCategory->save();
      $request->session()->flash('alert-message', 'Pekerjaan berhasil ditambahkan');
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-icon', 'check');

      return redirect()->back();
    }

    public function editPortfolio($id)
    {
         // if(empty($re))
        $portfolio = Portfolio::find($id);
        // $idCategory = $portfolio->category()->first()->id;
        // $portfolio->setAttribute('id_category', 'blablabla');


        return response()->json($portfolio);
    }

    public function updatePortfolio(Request $request, $id)
    {
        $portfolio = Portfolio::find($id);
        if($portfolio){
          $data = $request->all();
          $portfolio->fill($data);
          $portfolio->save();
        }


        return redirect()->back();
    }

    public function deletePortfolio($id)
    {
        $portfolio = Portfolio::find($id);
        if($portfolio){
          $portfolio->delete();
        }


        return redirect()->back();
    }
}
