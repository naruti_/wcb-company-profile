<?php

namespace App\Http\Controllers\Experience;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Gallery;

use Illuminate\Support\Facades\Storage;


class GalleryController extends Controller
{
  public function index()
  {
      $galeri = Gallery::all();
      return view('admin.experiences.galeri_index',compact('galeri'));
  }

  public function store(Request $request){
    $this->validate($request,[
      'img_gallery' => 'required|mimes:jpeg,jpg,png'
    ],[
      'mimes' =>'Upload gambar galeri hanya dengan gambar dengan extension .jpg,.jpeg, atau .png saja'
    ]);
    if($request->hasFile('img_gallery')){
      $newGallery = new Gallery;
      $newGallery->name = $request->name;
      $newGallery->desc = $request->desc;
      $image = $request->file('img_gallery');
      $newGallery->img_ext = $image->getClientOriginalExtension();
      $fileName = preg_replace('/\s+/', '_', $request->name).'.'.$image->getClientOriginalExtension();
      $newGallery->img_name = $fileName;
      $newGallery->save();
      $image->storeAs('public/images/galeri',$fileName);
      $request->session()->flash('alert-message', 'Galeri berhasil ditambahkan');
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-icon', 'check');
    }
    return back();

  }

  public function edit($id){
    $gallery = Gallery::find($id);
    return response()->json($gallery);
  }

  public function update(Request $request, $id){
    $this->validate($request,[
      'desc' => 'required|max:191'
    ],[
      'mimes' =>'Upload gambar galeri hanya dengan gambar dengan extension .jpg,.jpeg, atau .png saja'
    ]);
    $galeri = Gallery::find($id);
    $old_name = $galeri->img_name; //img_name
    $old_nama = $galeri->name; // name

    $galeri->name = $request->name;
    $img_dir = 'public/images/galeri/';
    // dd($old_name, $request->name);
    $old_name = $galeri->img_name;
    $galeri->name = $request->name;
    $img_dir = 'public/images/galeri/';

    if($request->hasFile('img_gallery')){
      $image = $request->file('img_gallery');
      $imgExt = $image->getClientOriginalExtension();
      $newFileName = preg_replace('/\s+/', '_', $request->name).'.'.$imgExt;
      // dd($img_dir.$old_name);
      // Storage::move('public/images/galeri/'.$galeri->img_name, 'public/images/galeri/'.$newFileName);
      Storage::delete($img_dir.$old_name);
      $image->storeAs($img_dir,$newFileName);
      $galeri->img_name = $newFileName;
      $galeri->img_ext = $imgExt;
      $galeri->desc = $request->desc;



    } elseif ($request->name == $old_nama){
      // dd('masuk sini');
      $galeri->desc = $request->desc;
    }

    else{
      $imgExt = $galeri->img_ext;
      $newFileName = preg_replace('/\s+/', '_', $request->name).'.'.$imgExt;
      // dd($galeri->img_name, $newFileName);
      Storage::move('public/images/galeri/'.$galeri->img_name, 'public/images/galeri/'.$newFileName);
      $galeri->img_name = $newFileName;

    }

    $galeri->save();
    $request->session()->flash('alert-message', 'Gambar berhasil Diperbaharui');
    $request->session()->flash('alert-message', 'Sertifikasi berhasil Diperbaharui');
    $request->session()->flash('alert-type', 2);
    $request->session()->flash('alert-icon', 'check');

    return redirect()->back();
  }


  public function delete(Request $request, $id)
  {
      $galeri = Gallery::find($id);
      $image_dir = 'public/images/galeri/';
      $image_name = $galeri->img_name;
      if($galeri){
        Storage::delete($image_dir.$image_name);
        $galeri->delete();
        $request->session()->flash('alert-message', 'Galeri telah dihapus');
        $request->session()->flash('alert-type', 4);
        $request->session()->flash('alert-icon', 'check');
      }
      return redirect()->back();
  }

}
