<?php

namespace App\Http\Controllers\Experience;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Category;
use App\CategoryRegulation;
use App\Regulation;
use Carbon\Carbon;

use Illuminate\Support\Facades\Storage;





class RegulationController extends Controller
{
  public function index()
  {
      $categories = CategoryRegulation::all();
      $regulations = Regulation::all();
      // dd($categories);
      return view('admin.experiences.regulation_index',compact('categories', 'regulations'));
  }

  public function store(Request $request)
  {
    if($request->hasFile('file_regulation')){

      $this->validate($request,[
        'file_regulation' => 'required|mimes:pdf,doc,docx,ppt,pptx'
      ],[
        'mimes' =>'Upload file peraturan hanya dengan file extension .pdf, .doc, .docx, .ppt, .pptx saja'
      ]);
      $newRegulation = new Regulation;
      $newRegulation->name = $request->name;
      $id_category = $request->id_category;
      $newRegulation->id_category_regulation = $id_category;
      $file = $request->file('file_regulation');
      $carbonTime = Carbon::now();
      $fileName =[
        'category' => CategoryRegulation::find($id_category)->first()->name,
        'name' => preg_replace('/\s+/', '-', $request->name),
        'extension' => $file->getClientOriginalExtension(),
        'time' => $carbonTime->year.$carbonTime->month.$carbonTime->day.$carbonTime->hour.$carbonTime->minute.$carbonTime->second,
      ];
      $newRegulation->fileName = $fileName['category'].'_'.$fileName['name'].'_'.$fileName['time'].'.'.$fileName['extension'];
      // dd($newRegulation->fileName);
      $newRegulation->fileext = $fileName['extension'];
      // dd($newRegulation);
      $newRegulation->save();
      $file->storeAs('public/files/regulations/',$newRegulation->fileName);
      $request->session()->flash('alert-message', 'Peraturan Lingkungan Hidup berhasil ditambahkan');
      $request->session()->flash('alert-type', 2);
      $request->session()->flash('alert-icon', 'check');
    }

    return redirect()->back();
  }

  public function edit($id){
    $regulation = Regulation::find($id);
    return response()->json($regulation);
  }

  public function update(Request $request, $id){
    $this->validate($request,[
      'file_regulation' => 'required|mimes:pdf,doc,docx,ppt,pptx'
    ],[
      'mimes' =>'Upload file peraturan hanya dengan file extension .pdf, .doc, .docx, .ppt, .pptx saja'
    ]);
    $regulation = Regulation::find($id);
    $old_name = $regulation->filename;
    $regulation->name = $request->name;
    $regulation->id_category_regulation = $request->id_category;
    $file_dir = 'public/files/regulations/';

    if($request->hasFile('file_regulation')){
      $file = $request->file('file_regulation');

      $carbonTime = Carbon::now();
      $fileName =[
        'category' => CategoryRegulation::find($regulation->id_category_regulation)->first()->name,
        'name' => preg_replace('/\s+/', '-', $request->name),
        'extension' => $file->getClientOriginalExtension(),
        'time' => $carbonTime->year.$carbonTime->month.$carbonTime->day.$carbonTime->hour.$carbonTime->minute.$carbonTime->second,
      ];
      $newFileName = $fileName['category'].'_'.$fileName['name'].'_'.$fileName['time'].'.'.$fileName['extension'];

      Storage::delete($file_dir.$old_name);
      $file->storeAs($file_dir,$newFileName);

      $regulation->filename = $newFileName;
      $regulation->fileext = $fileName['extension'];
    }
    else{
      dd("masuk");
      $file_ext = $regulation->fileext;

      $carbonTime = Carbon::now();
      $fileName =[
        'category' => CategoryRegulation::find($regulation->id_category_regulation)->first()->name,
        'name' => preg_replace('/\s+/', '-', $request->name),
        'extension' => $file_ext,
        'time' => $carbonTime->year.$carbonTime->month.$carbonTime->day.$carbonTime->hour.$carbonTime->minute.$carbonTime->second,
      ];

      $newFileName = $fileName['category'].'_'.$fileName['name'].'_'.$fileName['time'].'.'.$fileName['extension'];
      $regulation->filename = $newFileName;
      Storage::move($file_dir.$old_name, $file_dir.$newFileName);
    }
    $regulation->save();
    $request->session()->flash('alert-message', 'Sertifikasi berhasil Diperbaharui');
    $request->session()->flash('alert-type', 2);
    $request->session()->flash('alert-icon', 'check');

    return redirect()->back();
  }


  public function delete(Request $request, $id)
  {
      $regulation = Regulation::find($id);
      if($regulation){
        $regulation->delete();
        $request->session()->flash('alert-message', 'Galeri telah dihapus');
        $request->session()->flash('alert-type', 4);
        $request->session()->flash('alert-icon', 'check');
      }
      return redirect()->back();
  }
}
