<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Portfolio extends Model
{
  protected $table='portfolios';

  protected $fillable = [
      'name','id_category','client','location','year','created_at','update_at'
  ];

  public function category()
	{
		return $this->belongsTo(\App\Category::class, 'id_category');
	}
}
