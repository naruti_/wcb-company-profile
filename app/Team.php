<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\CategoryTeam;


class Team extends Model
{
  protected $table='teams';

  protected $fillable = [
      'name','id_category_team','bidang','pendidikan','created_at','update_at'
  ];

  public function category_team()
  {
    return $this->belongsTo(\App\CategoryTeam::class, 'id_category_team');
  }
}
