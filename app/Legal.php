<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Legal extends Model
{
  protected $table='legals';

  protected $fillable = [
      'name','id_category_legal','nomor','tgl_terbit','created_at','update_at'
  ];

  public function category_legal()
  {
    return $this->belongsTo(\App\CategoryLegal::class, 'id_category_legal');
  }
}
