<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Certificate extends Model
{
  protected $table='certificates';

  protected $fillable = [
      'name','no_certificate','qualification','kta','created_at','update_at'
  ];
}
