# ************************************************************
# Sequel Pro SQL dump
# Version 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.26-MariaDB)
# Database: adminCompany
# Generation Time: 2018-06-09 05:17:44 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table admins
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admins`;

CREATE TABLE `admins` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `admins_email_unique` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table categories
# ------------------------------------------------------------

DROP TABLE IF EXISTS `categories`;

CREATE TABLE `categories` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `categories_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;

INSERT INTO `categories` (`id`, `name`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'Studi Amdal',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(2,'Studi UKL/UPL',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(3,'Studi Monitoring Lingkungan',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(4,'Studi EBA',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(5,'Studi Lainnya',1,'2018-06-09 04:46:38','2018-06-09 04:46:38');

/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category_legals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_legals`;

CREATE TABLE `category_legals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_legals_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `category_legals` WRITE;
/*!40000 ALTER TABLE `category_legals` DISABLE KEYS */;

INSERT INTO `category_legals` (`id`, `name`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'Kementrian Lingkungan Hidup',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(2,'Ikatan Konsultan Indonesia',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(3,'Direktorat Jenderal Pajak',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(4,'PEMERINTAH KOTA BANDUNG',1,'2018-05-28 10:51:46','2018-05-28 10:54:08'),
	(5,'DEPARTEMEN ENERGI DAN SUMBER DAYA MINERAL',1,'2018-05-28 10:52:57','2018-05-28 10:52:57'),
	(6,'KAMAR DAGANG DAN INDUSTRI',1,'2018-05-28 10:54:27','2018-05-28 10:54:27');

/*!40000 ALTER TABLE `category_legals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category_regulations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_regulations`;

CREATE TABLE `category_regulations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_regulations_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `category_regulations` WRITE;
/*!40000 ALTER TABLE `category_regulations` DISABLE KEYS */;

INSERT INTO `category_regulations` (`id`, `name`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'Perautan Menteri Negara Lingkungan Hidup',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(2,'Peraturan Pemerintah',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(3,'Undang-Undang',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(4,'Bakumutu Laboratorium - Air',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(5,'Bakumutu Laboratorium - K3',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(6,'Bakumutu Laboratorium - Material',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(7,'Bakumutu Laboratorium - Udara',1,'2018-06-09 04:46:38','2018-06-09 04:46:38');

/*!40000 ALTER TABLE `category_regulations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table category_teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `category_teams`;

CREATE TABLE `category_teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `category_teams_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `category_teams` WRITE;
/*!40000 ALTER TABLE `category_teams` DISABLE KEYS */;

INSERT INTO `category_teams` (`id`, `name`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'Biologi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(2,'Sosial',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(3,'Geodesi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(4,'Geologi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(5,'Pertambangan',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(6,'Hidrologi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(7,'Hidrooceanografi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(8,'Kehutananan',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(9,'Kesehatan Masyarakat',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(10,'Kimia',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(11,'Lingkungan',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(12,'Mekanik Tanah',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(13,'Pertanian',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(14,'Planologi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(15,'Sipil',1,'2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(16,'Transportasi',1,'2018-06-09 04:46:38','2018-06-09 04:46:38');

/*!40000 ALTER TABLE `category_teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table certificates
# ------------------------------------------------------------

DROP TABLE IF EXISTS `certificates`;

CREATE TABLE `certificates` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `no_certificate` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `qualification` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kta` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `certificates` WRITE;
/*!40000 ALTER TABLE `certificates` DISABLE KEYS */;

INSERT INTO `certificates` (`id`, `name`, `no_certificate`, `qualification`, `kta`, `created_at`, `updated_at`)
VALUES
	(1,'Iwan Setiawan','001465/SKPA-P2/LSK-INTAKINDO/IX/2015','Ketua Tim Penyusun Dokumen AMDAL','K.001.07.09.10.000005',NULL,NULL),
	(2,'Aziz Rahman','001470/SKPA-P2/LKS-INTAKINDO/VIII/2015','Ketua Tim Penyusun Dokumen AMDAL','K.001.07.09.10.000004',NULL,NULL),
	(3,'Bambang Kusharyadi','001473/SKPA-P2/LSK-INTAKINDO/VIII/2015','Anggota Tim Penyusun Dokumen AMDAL','A.001.07.09.10.000007','2018-05-28 11:20:34','2018-05-28 11:20:54'),
	(4,'Dadan Ramdan','001284/SKPA-P1/LSK-INTAKINDO/IX/2014','Anggota Tim Penyusun Dokumen AMDAL','A.039.08.11.10.000472','2018-05-28 11:21:36','2018-05-28 11:21:36'),
	(5,'Endang Mulyadi','001565/SKPA-P1/LSK-INTAKINDO/XII/2015','Ketua Tim Penyusun Dokumen AMDAL','A.059.12.12.14.000578','2018-05-28 11:22:14','2018-05-28 11:22:14'),
	(6,'A.059.12.12.14.000578','001448/SKPA-P2/LSK-INTAKINDO/IX/2015','Ketua Tim Penyusun Dokumen AMDAL','K.001.07.09.10.000003','2018-05-28 11:22:54','2018-05-28 11:22:54'),
	(7,'Haikal Suhaidi','001469/SKPA-P2/LSK-INTAKINDO/VIII/2015','Anggota Tim Penyusun Dokumen AMDAL','A.001.07.09.04.000002','2018-05-28 11:23:55','2018-05-28 11:23:55'),
	(8,'Arie Fitria Indrayana','001449/SKPA-P2/LSK-INTAKINDO/IX/2015','Anggota Tim Penyusun Dokumen AMDAL','A.001.07.09.10.000008','2018-05-28 11:24:33','2018-05-28 11:24:33'),
	(9,'Joko Edi Santosa','001435/SKPA-P1/LSK-INTAKINDO/IX/2015','Ketua Tim Penyusun Dokumen AMDAL','K.001.07.09.10.000006','2018-05-28 11:25:30','2018-05-28 11:25:30'),
	(10,'Neneng Nurbaeti Amin','000563/SKPA/LSK-INTAKINDO/IV/2012','Tenaga Ahli Penyusun Dokumen AMDAL ; Anggota Tim Penyusun 11/G- 46/26-04-2012/000592','A.046.02.12.007.000531','2018-05-28 11:26:11','2018-05-28 11:26:32');

/*!40000 ALTER TABLE `certificates` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table galeries
# ------------------------------------------------------------

DROP TABLE IF EXISTS `galeries`;

CREATE TABLE `galeries` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `img_ext` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table legals
# ------------------------------------------------------------

DROP TABLE IF EXISTS `legals`;

CREATE TABLE `legals` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `id_category_legal` int(10) unsigned NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tgl_terbit` date NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `legals_name_unique` (`name`),
  UNIQUE KEY `legals_nomor_unique` (`nomor`),
  KEY `legals_id_category_legal_foreign` (`id_category_legal`),
  CONSTRAINT `legals_id_category_legal_foreign` FOREIGN KEY (`id_category_legal`) REFERENCES `category_legals` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `legals` WRITE;
/*!40000 ALTER TABLE `legals` DISABLE KEYS */;

INSERT INTO `legals` (`id`, `name`, `id_category_legal`, `nomor`, `tgl_terbit`, `status`, `created_at`, `updated_at`)
VALUES
	(1,'Sertifikasi Tanda Registrasi Kompetensi',1,'S-285/SETJEN/SLK/PKTL-4/3/2016','2016-03-08',1,'2018-05-28 10:45:20','2018-05-28 10:45:20'),
	(2,'Kartu Tanda Anggota',2,'10591/P/0684.JB','2016-12-31',1,'2018-05-28 10:46:55','2018-05-28 10:46:55'),
	(3,'SBU Konsultan Lainnya',2,'4-3273-04-008-1-10-010142','2016-02-26',1,'2018-05-28 10:47:51','2018-05-28 10:47:51'),
	(4,'SBU Spesialis',2,'3-3273-05-008-1-10-010142','2016-02-26',1,'2018-05-28 10:48:28','2018-05-28 10:48:28'),
	(5,'SBU Perencanaan Rekayasa',2,'1-3273-02-008-1-10-010142','2016-02-26',1,'2018-05-28 10:49:07','2018-05-28 10:49:07'),
	(6,'Surat Pengukuhan Pajak',3,'PEM-CU 338/WPJ-09/KP.0303/2007','2007-10-31',1,'2018-05-28 10:49:52','2018-05-28 10:49:52'),
	(7,'Surat Keterangan Terdaftar Pajak',3,'PEM-CU 337/WPJ-09/KP.0303/2007','2007-10-31',1,'2018-05-28 10:50:35','2018-05-28 10:50:35'),
	(8,'Pokok Wajib Pajak',3,'01.583.632.3-429.000','1992-08-19',1,'2018-05-28 10:51:24','2018-05-28 10:51:24'),
	(9,'Surat Keterangan Domisili Perusahaan',4,'045/DP/Kel.Mjlg/V/2013','2013-05-23',1,'2018-05-28 10:55:15','2018-05-28 10:55:15'),
	(10,'Surat Ijin Gangguan (HO)',4,'536/SI-6671/KPMD/2007','2007-11-27',1,'2018-05-28 10:56:27','2018-05-28 10:56:27'),
	(11,'Kartu Herregistrasi IG/ITU',4,'503/IG-HERR9220/BPPT','2013-12-23',1,'2018-05-28 10:57:13','2018-05-28 10:57:13'),
	(12,'Izin Usaha Jasa Konstruksi',1,'1-3273-010142-1-000205','2014-10-10',1,'2018-05-28 10:58:02','2018-05-28 10:58:02'),
	(13,'Surat Ijin Usaha Perdagangan',4,'0067/IUP-UB/III/2016/BPPT','2016-03-07',1,'2018-05-28 10:58:39','2018-05-28 10:58:39'),
	(14,'Tanda Daftar Perusahaan',4,'101117003861','2012-11-09',1,'2018-05-28 10:59:28','2018-05-28 10:59:28'),
	(15,'Surat Keterangan Terdaftar DIRJEN MIGAS',5,'1006/SKT-02/DMT/2010','2010-12-08',1,'2018-05-28 11:00:14','2018-05-28 11:00:14'),
	(16,'Kartu Tanda Anggota Biasa',6,'20101-007400','2010-09-21',1,'2018-05-28 11:01:06','2018-05-28 11:01:06');

/*!40000 ALTER TABLE `legals` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table migrations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `migrations`;

CREATE TABLE `migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `migrations` WRITE;
/*!40000 ALTER TABLE `migrations` DISABLE KEYS */;

INSERT INTO `migrations` (`id`, `migration`, `batch`)
VALUES
	(65,'2014_10_12_000000_create_users_table',1),
	(66,'2014_10_12_100000_create_password_resets_table',1),
	(67,'2018_02_18_095244_create_admins_table',1),
	(68,'2018_02_26_084301_create_categories_table',1),
	(69,'2018_03_02_145458_create_portfolio_table',1),
	(70,'2018_04_04_064336_create_structures_table',1),
	(71,'2018_04_06_031432_create_certificates_table',1),
	(72,'2018_04_06_164146_create_galeries_table',1),
	(73,'2018_04_15_114612_create_category_regulations_table',1),
	(74,'2018_04_15_114621_create_regulations_table',1),
	(75,'2018_04_17_201713_create_category_team_table',1),
	(76,'2018_04_17_201726_create_team_table',1),
	(77,'2018_04_17_221406_create_category_legals_table',1),
	(78,'2018_04_17_221415_create_legals_table',1),
	(79,'2018_05_23_024408_create_partners_table',1),
	(80,'2018_06_07_154949_create_foreign_keys',1);

/*!40000 ALTER TABLE `migrations` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table partners
# ------------------------------------------------------------

DROP TABLE IF EXISTS `partners`;

CREATE TABLE `partners` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `desc` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `nomor` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alamat` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `kontak` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table password_resets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password_resets`;

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  KEY `password_resets_email_index` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table portfolios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `portfolios`;

CREATE TABLE `portfolios` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_category` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `client` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `location` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `year` int(10) unsigned NOT NULL,
  `status` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `portfolios_id_category_foreign` (`id_category`),
  CONSTRAINT `portfolios_id_category_foreign` FOREIGN KEY (`id_category`) REFERENCES `categories` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=209 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `portfolios` WRITE;
/*!40000 ALTER TABLE `portfolios` DISABLE KEYS */;

INSERT INTO `portfolios` (`id`, `id_category`, `name`, `client`, `location`, `year`, `status`, `created_at`, `updated_at`)
VALUES
	(10,1,'Penyusunan Laporan Studi ANDAL, RKL  & RPL  Kegiatan Penambangan Batubara  KP – 359, KP – 360 dan KP 361','PT. Injatama','Bengkulu',2010,1,'2018-05-29 02:15:45','2018-05-29 02:15:45'),
	(11,1,'Penyusunan Laporan Studi ANDAL, RKL  & RPL  Kegiatan Penambangan Batubara  KP – 397','PT. Injatama','Bengkulu',2010,1,'2018-05-29 02:16:16','2018-05-29 02:16:16'),
	(12,1,'Penyusunan Laporan Studi ANDAL, RKL  & RPL  Kegiatan Penambangan Batubara  KP – 398','PT. Injatama','Bengkulu',2010,1,'2018-05-29 02:16:45','2018-05-29 02:16:45'),
	(13,1,'Penyusunan Laporan Studi ANDAL, RKL  & RPL  Kegiatan Rencana Pembangunan  Dermaga','PT. Injatama','Bengkulu',2010,1,'2018-05-29 02:17:27','2018-05-29 02:17:27'),
	(14,1,'Studi ANDAL, RKL & RPL Kegiatan Perkebunan dan Pabrik Kelapa Sawit','PT. Agrinusa Persada Mulia','Prop. Papua',2010,1,'2018-05-29 02:18:14','2018-05-29 02:18:14'),
	(15,1,'Studi ANDAL, RKL & RPL Kegiatan Perkebunan dan Pabrik Kelapa Sawit','PT. Agrinusa Persada Mulia','Prop. Papua',2010,1,'2018-05-29 02:18:48','2018-05-29 02:18:48'),
	(16,1,'Penyusunan Laporan  Studi , Andal, RKL dan RPL   KegiatanIndustri Pulp, kertas , fasilitas  penunjangnya dan Power Plant','PT. Riau Andalan Pulp and  Paper','Pekanbaru, Riau',2010,1,'2018-05-29 02:19:28','2018-05-29 02:19:28'),
	(17,1,'Studi AMDAL Jalan Highway  LintasTimurSeksi – 2 (Paket ST-08/2009)','Dinas Bina Marga dan Cipta  Karya','Aceh',2010,1,'2018-05-29 02:19:49','2018-05-29 02:19:49'),
	(18,1,'Studi ANDAL, RKL & RPL That Cover all  activities in Bentu Block and KorinciBaru  Block','Kalila (Bentu – KorinciBaru) Ltd','Pekanbaru, Riau',2011,1,'2018-05-29 02:20:10','2018-05-29 02:20:10'),
	(19,1,'Studi ANDAL,RKL & RPL  KegiatanPabrikKelapaSawit (PKS)','PT. SalonokLadang Mas','Palangkaraya, Kalimantan Tengah',2011,1,'2018-05-29 02:21:34','2018-05-29 02:21:52'),
	(20,1,'StudiRevisi AMDAL Kegiatan Landfill','PT. Indo – Bharat Rayon','Kab. Purwakarta',2011,1,'2018-05-29 02:22:18','2018-05-29 02:22:18'),
	(21,1,'Studi ANDAL, RKL & RPL  KegiatanPengembanganIndustri','PT. Indo – Bharat Rayon','Kab. Purwakarta',2011,1,'2018-05-29 02:22:52','2018-05-29 02:22:52'),
	(22,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan Perkebunan  KaretdanFasiltiasPenunjangnya','PT. Montelo','Kab. Mapi, Propinsi Papua',2011,1,'2018-05-29 02:33:02','2018-05-29 02:33:02'),
	(23,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan Multifuel Boiler (MB) 25  Kapasitas 1 x 250 MW','PT. Pindo Deli Pulp and Paper  Mills','Kab. Siak, Propinsi Riau',2011,1,'2018-05-29 02:33:54','2018-05-29 02:33:54'),
	(24,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan Multifuel Boiler (MB) 24  dan 26 Kap. 2 x 150 MW','PT. Indah Kiat Pulp & Paper  Tbk','Kab. Siak, Propinsi Riau',2011,1,'2018-05-29 02:34:31','2018-05-29 02:34:31'),
	(25,1,'Studi ANDAL, RKL & RPL  KegiatanPewarnaTekstil','PT. DyStarColours Indonesia','Serang, Provinsi Banten',2011,1,'2018-05-29 02:35:27','2018-05-29 02:35:27'),
	(26,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan COndoteldan Club House  di Area Dago Golf Bandung','PT. Panghegar Kana Legacy','Dago, Kota Bandung',2011,1,'2018-05-29 02:36:06','2018-05-29 02:36:06'),
	(27,1,'Suplemen  AMDAL Rencana  Pembangunan JalanTolCikampek –  PalimananJawa Barat','PT. Lintas Marga Sedaya','Kab. Majalengka&Kab.  Cirebon ProvinsiJawa  Barat',2011,1,'2018-05-29 02:37:34','2018-05-29 02:37:34'),
	(28,1,'Studi ANDAL, RKL & RPL Kegiatan LNG  Receiving Terminal','PT. Trans Lintas Nusa Gemilang','Kabupaten Bekasi',2011,1,'2018-05-29 02:38:05','2018-05-29 02:38:05'),
	(29,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan PLTA Yahwei –  UrumukadanTransmisi (102 Km)','PT. Papua Power Indonesia','Provinsi Papua',2011,1,'2018-05-29 02:38:48','2018-05-29 02:38:48'),
	(30,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan Multifuel Boiler (MB) 25  Kapasitas 1 x 150 MW','PT. PindoDelip Pulp & Paper  Mills','Provinsi Riau',2011,1,'2018-05-29 02:39:28','2018-05-29 02:39:28'),
	(31,1,'Studi ANDAL, RKL & RPL Kegiatan  Pembangunan Multifuel Boiler (MB) 24  dan 26 Kapasitas 2  x 150 MW','PT. Indah Kiat Pulp & Paper  Tbk.','Provinsi Riau',2011,1,'2018-05-29 02:39:53','2018-05-29 02:39:53'),
	(32,1,'Studi ANDAL, RKL & RPL  KegiatanRencana Pembangunan Open  Access','PT. Pertamina (Persero) RU II  Dumai','Dumai',2011,1,'2018-05-29 02:40:24','2018-05-29 02:40:24'),
	(33,1,'Studi ANDAL, RKL & RPL  JalanAlahanPanjang – KiliranJao','DinasPrasaranaJalandan Tata  RuangProvinsi Sumatera Barat','Padang',2012,1,'2018-05-29 02:40:56','2018-05-29 02:40:56'),
	(34,1,'Studi AMDAL Rencana Pembangunan  JaringanTransmisi (SUTT) 150 kV PLTP  Hululais – PekalonganProvinsi Bengkulu','PT. PLN (Persero)','Bengkulu',2012,1,'2018-05-29 02:41:31','2018-05-29 02:41:31'),
	(35,1,'Studi ANDAL, RKL & RPL Reklamasi untuk Kawasan Industri','PT. Sumber Bina Sukses','Kab. Serang',2012,1,'2018-05-29 02:42:01','2018-05-29 02:42:01'),
	(36,1,'Studi ANDAL, RKL & RPL Rencana Kegiatan Pembangunan Kawasan Industri','PT. Sinar Texindo Utama','Kab. Serang',2012,1,'2018-05-29 02:42:30','2018-05-29 02:42:30'),
	(37,1,'Studi ANDAL, RKL & RPL Kegiatan Perkebunan  Tebu dan Pabrik Pengolahannya (Wilmar Group)','PT. Anugrah Rezeki Nusantara','Kab. Merauke',2012,1,'2018-05-29 02:43:06','2018-05-29 02:43:06'),
	(38,1,'Studi ANDAL, RKL & RPL Kegiatan Reklamasi untuk Kawasan Indsutri','PT. Sumber Bina Sukses','Kab. Serang',2012,1,'2018-05-29 02:44:10','2018-05-29 02:44:10'),
	(39,1,'Studi ANDAL, RKL & RPL Kegiatan Kawasan Industri','PT. Prasidha Inti Jaya','Kab. Cirebon',2012,1,'2018-05-29 02:44:41','2018-05-29 02:44:41'),
	(40,1,'Addendum ANDAL, RKL & RPL Kegiatan Pengembangan Lapangan Minyak dan Gas Bumi','EMP Malacca Strait','Kabupaten Kepulauan  Meranti',2012,1,'2018-05-29 02:45:16','2018-05-29 02:45:16'),
	(41,1,'Studi AMDAL Jalan Sungai Rawa – Tanjung PAL – Teluk Lanus','Dinas Pekerjaan Umum Kabupaten Siak','Kabupaten Siak',2012,1,'2018-05-29 02:45:46','2018-05-29 02:45:46'),
	(42,1,'Studi AMDAL Jalan Lubuk Dalam – Meredan (3-P)','Dinas Pekerjaan Umum Kabupaten Siak','Kabupaten Siak',2012,1,'2018-05-29 02:46:15','2018-05-29 02:46:15'),
	(43,1,'Pelaksanaan Pekerjaan Konsultansi Pekerjaan AMDAL D.I Peureulak Kab. Aceh Timur (5.000 Ha)','Kementerian Pekerjaan Umum, Direktorat Jenderal Sumber Daya Air','Banda Aceh',2012,1,'2018-05-29 02:46:56','2018-05-29 02:46:56'),
	(44,1,'Studi AMDAL Peningkatan Jalan Sebulu Menuju Jalan Propinsi Kec. Sebulu','Dinas Bina Marga dan Sumber Daya Air','Tenggarong',2013,1,'2018-05-31 08:33:22','2018-05-31 08:33:22'),
	(45,1,'Stidi AMDAL untu Proyek PLTM di Kabupaten Masamba, Luwu Utara – Sulawesi Selatan','PT. Sangsaka Hidro Kasmar','Sulawesi Selatan',2013,1,'2018-05-31 08:33:59','2018-05-31 08:33:59'),
	(46,1,'Studi AMDAL Pembangunan Jaringan Transmisi SUTT 150 kV PLTU Nagan Raya – Gardu Induk Blangpidie dan Pembangunan Gardu Induk Blangpidie','PT. PLN Persero Unit Pembangunan Jaringan Sumatera I','Aceh',2013,1,'2018-05-31 08:34:30','2018-05-31 08:34:30'),
	(47,1,'Studi AMDAL RencanaPembangunan Jaringan Transmisi SUTT 150 kV PLTU Nagan Raya - Gardu Induk Blangpidie dan Pembangunan Gardu Induk Blangpidie','PT. PLN Persero Unit Pembangunan Jaringan Sumatera I','Aceh',2013,1,'2018-05-31 08:35:39','2018-05-31 08:35:39'),
	(48,1,'Pengawasan (Supervisi) Pelaksanaan ANDAL, RKL & RPL KegiatanTambangBatubara dan Dermaga yang berlokasi di Ketahun, Kabupaten Bengkulu Utara, Provinsi Bengkulu.','PT. INJATAMA','Provinsi Bengkulu',2013,1,'2018-05-31 08:36:38','2018-05-31 08:36:38'),
	(49,1,'PenyusunanStudi AMDAL, RencanaKegiatan PREDE TRANSORT yang berlokasi di TelukTinopo, KabupatenMentawai, Provinsi Sumatra Barat','PT. INJATAMA','Provinsi Sumatera Barat',2013,1,'2018-05-31 08:37:04','2018-05-31 08:37:04'),
	(50,1,'Studi AMDAL KegiatanPembangunanMall, Ruko, PlayGround dan Waterpark','PT. Cilegon Cipta Sejati','Provinsi Banten',2013,1,'2018-05-31 08:38:18','2018-05-31 08:38:18'),
	(51,1,'Studi AMDAL KegiatanPembangunanPerumahan Modern yang berlokasi di KecamatanKosambi, KabupatenTangerang','PT. Kukuh Mandiri Lestari','Kabupaten Tangerang',2013,1,'2018-05-31 08:39:59','2018-05-31 08:39:59'),
	(52,1,'Studi AMDAL PT. Prisma Inti Semestaseluas 1000 HA, di kawasanIndustri Modern Cikande','PT. Prisma Inti Semesta','Serang',2013,1,'2018-05-31 08:41:22','2018-05-31 08:41:22'),
	(53,1,'Studi AMDAL PT. The New Asia Indsutrial Estate seluas 325 Ha, di KawasanIndustri Modern Cikande','PT. The New Asia Industrial Estate','Serang',2013,1,'2018-05-31 08:41:51','2018-05-31 08:41:51'),
	(54,1,'Studi AMDAL PLTA Lasolo','PT. Sangsaka Hidro Tiara','Kabupaten Konawe Utara, Provinsi Sulawesi Tenggara',2013,1,'2018-05-31 08:42:25','2018-05-31 08:42:25'),
	(55,1,'Studi AMDAL PengolahanKelapaSawit','PT. Dharma Buana Lestari','Distrik Pantai Timur dan Pantai Timur Barat, Kabupaten Sarmi Provinsi papua',2013,1,'2018-05-31 08:42:48','2018-05-31 08:42:48'),
	(56,1,'Studi AMDAL Kegiatan PLTM','PT. Sangsaka Hidro Kasmar','Provinsi Sulawesi Selatan',2013,1,'2018-05-31 08:43:20','2018-05-31 08:43:20'),
	(57,1,'Studi AMDAL RencanaKegiatan Rede Transport','PT. Injatama','Kabupaten Mentawai, Sumatera Barat',2013,1,'2018-05-31 08:44:17','2018-05-31 08:44:17'),
	(58,1,'Studi AMDAL KegiatanRencanaPengawasanSupervisi','PT. Injatama','Kabupaten Bengkulu Utara, Provinsi Bengkulu',2013,1,'2018-05-31 08:45:01','2018-05-31 08:45:01'),
	(59,1,'Studi AMDAL Pekerjaan Penyusunan Dampak Lingkungan','PT. Prisma Inti Semesta','Kabupaten Serang',2013,1,'2018-05-31 08:46:21','2018-05-31 08:46:21'),
	(60,1,'Studi AMDAL PekerjaanPenyusunanDampakLingkung an (Modern Cikande)','PT. The New Asia Industrial Estate','Kabupaten Serang',2013,1,'2018-05-31 08:47:05','2018-05-31 08:47:05'),
	(61,1,'Studi AMDAL UPHHK-HK Jabon, Accasia dan Eucalyptus(Kawasan Modern Cikande)','PT. Wahana Samdura Sentosa','Kabupaten Merauke Provinsi Papua',2013,1,'2018-05-31 08:47:59','2018-05-31 08:47:59'),
	(62,1,'Studi AMDAL Kegiatan Perkebunan Tebu dan Pabrik Pengolahannya.','PT. Angugerah Rejeki Nusantara','Kabupaten Merauke, provinsi papua.',2013,1,'2018-05-31 08:48:43','2018-05-31 08:48:43'),
	(63,1,'Studi AMDAL untuk Reklamasi','PT. Tirta Wahana Bali Internasional.','Provinsi Bali',2013,1,'2018-05-31 08:49:17','2018-05-31 08:49:17'),
	(64,1,'Stdui AMDAL Penembangan Pasar Mangkurawang','Dinas Cipta Karya dan Tata Ruang','Tenggarong',2013,1,'2018-05-31 08:49:51','2018-05-31 08:49:51'),
	(65,1,'Pekerjaan Pengawasan dan Monitoring ANDAL, RKL & RPL.','PT. Injatama','Ketahun, Kabupaten Bengkulu Utara, Provinsi Bengkulu.',2014,1,'2018-05-31 08:53:59','2018-05-31 08:53:59'),
	(66,1,'Studi Addendum AMDAL Pengembangan Resor Dago Pakar dan Monitoring RKL-RPL Semester I.','PT. Resor Dago Pakar','Kecamatan Cimenyan, Kabupaten Bandung.',2014,1,'2018-05-31 08:54:35','2018-05-31 08:54:35'),
	(67,1,'Studi AMDAL Pengembangan Industri Tekstile.','PT. Daliatex Kusuma','Jl. Moch Toha No. 307 km 7.3 Kabupaten Bandung.',2014,1,'2018-05-31 08:55:16','2018-05-31 08:55:16'),
	(68,1,'Penyusunan Adendum ANDAL, RKL&RPL Kegiatan Idnsutri Gula Rafinasi','PT. Duta Sugar Internasional','Jl. Raya BojonegaraMerak Provinsi Banten.',2014,1,'2018-05-31 08:55:46','2018-05-31 08:55:46'),
	(69,1,'Studi AMDAL Kegiatan Perkebunan Kelapa Sawit dan Fasilitas Pendukung.','PT. Agro Bukit','Jakarta',2014,1,'2018-05-31 08:56:16','2018-05-31 08:56:16'),
	(70,1,'Studi ANDAL, RKL & RPL Kegiatan Pembangunan Water Treatment Plant dan Fasilitas Penunjangnya.','PT. Modern Industrial Estate','Jl. Raya Serang Km 68, Cikande serang 42186',2014,1,'2018-05-31 08:56:39','2018-05-31 08:56:39'),
	(71,1,'Studi AMDAL Kegiatan Pengurusan SKKLH dan Pengurusan Ijin Lingkungan Rencana Kegiatan Kawasan Industri Jababeka Tahap VII.','PT. Grahabuana Cikarang','Cikarang Bekasi',2014,1,'2018-05-31 08:57:08','2018-05-31 08:57:08'),
	(72,1,'Studi AMDAL Dalam Rangka Penyusunan Revisi Dokumen Analisa Mengenai Dampak Lingkungan.','PT. Multipersada Gatramegah','Kabupaten Murung Raya, Provinsi Kalimantan Tengah.',2014,1,'2018-05-31 08:57:39','2018-05-31 08:57:39'),
	(73,1,'Studi AMDAL Kegiatan Perkebunan Tebu dan Pabrik Pengolahannya.','PT. Global Papua Abadi','Distrik Kurik dan Tanah Miring, Kab. Merauke, Provinsi Papua.',2014,1,'2018-05-31 08:58:06','2018-05-31 08:58:06'),
	(74,1,'Studi AMDAL Perkebunan Kelapa Swit','PT. Wira Antara','Distrik Kaureh Kabupaten Jayapura.',2014,1,'2018-05-31 08:58:51','2018-05-31 08:58:51'),
	(75,1,'Studi AMDAL','PT. Daya Indah Nusantara','Spring Tower 01-12, Jl. K.L Yos Sudarso, Tanjung Mulia Medan.',2014,1,'2018-05-31 08:59:36','2018-05-31 08:59:36'),
	(76,1,'Study DELH RuasJalanTatui – Mambo DistrikKosiwo','Balai Lingkungan Hidup Kabupaten Kepulauan Yapen','Jl. Sumatera No. 3 Serui',2014,1,'2018-05-31 09:00:14','2018-05-31 09:00:14'),
	(77,1,'Study AMDAL PasarYoutefaJayapura, Kota Jayapura, Provinsi Papua','Diskoperindagkop Kota Jayapura','Jln. Balai Kota No. 1 Entrop, Kota Jayapura',2014,1,'2018-05-31 09:00:43','2018-05-31 09:00:43'),
	(78,1,'Study AMDAL TPA dan IPLT Koyakoso Kota Jayapura','Balai Lingkungan Hidup Kota Jayapura','DistrikAbepura, Kota Jayapura',2014,1,'2018-05-31 09:01:28','2018-05-31 09:01:28'),
	(79,1,'Studi AMDAL Kegiatan Pembangunan Perkebunan dan Pabrik Pengolahan Kelapa Sawit.','PT. Internusa Jaya Sejahtera','Distrik Muting, Ulilin dan Elikobel Kab. Merauke. Provinsi Papua.',2014,1,'2018-05-31 09:02:07','2018-05-31 09:02:07'),
	(80,1,'Studi ADDENDUM AMDAL (ANDAL, RKL & RPL) Rencana Peningkatan Kapasitas Produksi Pulp Dari 2,8 Juta Ton/Tahun menjadi 3,1 Juta Ton/Tahun','PT. Indah Kiat Pulp & Paper Tbk. Perawang Mill','Desa Pinang Sebatang, Kecamatan Tualang, Kabupaten Siak, Provinsi Riau',2015,1,NULL,NULL),
	(81,1,'Studi AMDAL Rencana Kegiatan Industri Ban Luar dan Dalam Kendaraan PT. KENDA RUBBER INDONESIA','PT. KENDA RUBBER INDONESIA','Jl Raya Cikande Rangkasbitung KM. 5 Desa Kareo, Kec. Jawilan, Kabupten Serang, Provinsi Banten',2015,1,'2018-06-04 21:33:05','2018-06-04 21:33:05'),
	(82,1,'Studi AMDAL Rencana Kegiatan Izin Pemanfaatan Hasil Hutan Kayu – Hutan Alam (IUPHHK-HA)','PT. Jati Dharma indah Plywood Industries','Kabupaten Nabire, Provinsi Papua.',2015,1,'2018-06-04 21:33:39','2018-06-04 21:33:39'),
	(83,1,'Studi AMDAL Kegaitan Pembangunan Perumahan dan Fasilias Komersial','PT. CIPURA NUSA MITRA','Kota Can Kabupaten Cirebon',2015,1,'2018-06-04 21:33:59','2018-06-04 21:33:59'),
	(84,1,'Studi AMDAL Kegiatan Penambangan Pasir Laut','PT. BANDA ALAM LAUTAN JAYA','Perairan Laut Provinsi Banten',2015,1,'2018-06-04 21:34:22','2018-06-04 21:34:22'),
	(85,1,'Biaya Pelaksanaan Pekerjaan “Banten LNG Receiving Terminal AMDAL” PT. Bumi Sarana Migas, Tokyo Gas Co. Ltd and Mitsui & Co. Ltd','PT. Bumi Sarana Migas','Pulo Ampel, Provinsi Banten',2015,1,'2018-06-04 21:34:49','2018-06-04 21:34:49'),
	(86,1,'Studi AMDAL Rencana Kegiatan Pembangunan Jalan Koridor Angkutan Kayu Sepanjang 20,6 km dari Area PT. Fajar Surya Swadaya ke Mill','PT. Atlantik Bina Persada','Desa Muan Kelurahan Buluminung dan Sotek, Kecamatan Penajam, Kabupaten Penajam paser Utara, provinsi Kalimantan Timur',2015,1,'2018-06-04 21:35:07','2018-06-04 21:35:35'),
	(87,1,'Studi AMDAL dan Izin Lingkungan Pembangunan Perumahan dan Area Komersial Terpadu Citraland Jayapura.','Citra Land Jayapura','Kelurahan Entrop, Distrik Jayapura Selatan Kota Jayapura',2015,1,'2018-06-04 21:35:56','2018-06-04 21:35:56'),
	(88,1,'Studi AMDAL Kegiatan Perumahan CitraLand Cirebon','PT. Ciputra Nusa Mitra','Cirebon',2015,1,'2018-06-04 21:36:19','2018-06-04 21:36:19'),
	(89,1,'tudi AMDAL Kegiatan Pembangunan Rumah Tinggal, Resport dan Tempat Rekreasi Hutan Lindung Luas Lahan ± 20 Ha.','PT. Bukit Surya Nusantara','Desa Ciwaruga dan Sariwangi, Kecamatan Parongpong, Kabupaten Bandung Barat',2015,1,'2018-06-04 21:36:43','2018-06-04 21:36:43'),
	(90,1,'Studi AMDAL Kegiatan Rencana Pengolahan Limbah B3 dengan Incenerator dan Peleburan Logam.','PT. Horas Miduk','Desa/Kel. Padabeunghar, Kecamatan Jampang Tengah Kabupaten Sukabumi, Provinsi Jawa Barat',2015,1,'2018-06-04 21:37:04','2018-06-04 21:37:04'),
	(91,1,'Studi AMDAL Kegiatan Perkebunan Kelapa Sawit.','PT. Sariwarna Adi Perkasa','Kabupaten Nabire, Provinsi Papua.',2015,1,'2018-06-04 21:37:24','2018-06-04 21:37:24'),
	(92,1,'Studi AMDAL Rencana Kegiatan Kawasan Komersial dan perumahan Summarecon.','PT. Mahkota Permata Perdana','Kelurahan Rancabolang & Cisaranten Kidul, Kecamatan Gede Bage, Kota Bandung, Provinsi Jawa Barat',2015,1,'2018-06-04 21:37:44','2018-06-04 21:37:44'),
	(93,1,'Studi FAMDAL Rencana Kegiatan Pembangunan Dermaga untuk Industri Kayu Serpih dan TUKS','PT. Agra Bareksa Indonesia','Kelurahan Jenebora Dan Gersik, Kecamatan Penajam, Kabupaten Penajam Paser Utara, Provinsi Kalimantan Timur',2016,1,'2018-06-04 21:38:09','2018-06-04 21:38:09'),
	(94,1,'Studi AMDAL Kegiatan Perkebunan Kelapa Sawit dan pabrik Pengolahan Kelapa Sawit','PT. Patria Agri Lestari','Distrik Senggi, Kabupaten Keerom, Provinsi Papua',2016,1,'2018-06-04 21:38:35','2018-06-04 21:38:35'),
	(95,1,'Studi AMDAL Perkebunan Kelapa Sawit dan Pengolahan Kelapa Sawit','PT. Semarak Agri Lestari','Distri Senggi, Kabupaten Keerom, Provinsi Papua',2016,1,'2018-06-04 21:39:05','2018-06-04 21:39:05'),
	(96,1,'Studi AMDAL Rencana Pembangunan dan Pengoperasian Pabrik Gula Modern, Petak Lahan tanam dan Sistem Transportasi Pada Lahan Seluas 5500 HA','PT. Muaria Samba Manis','Kabupaten Sumba Timur, Provinsi Nusa Tenggara Timur',2016,1,'2018-06-04 21:39:26','2018-06-04 21:39:26'),
	(97,1,'Studi AMDAL For Development Plan For Wind Power Plant (PLTB)','PT. UPC Sukabumi Bayu Energi','Kabupaten Sukabumi',2016,1,'2018-06-04 21:39:46','2018-06-04 21:39:46'),
	(98,1,'Studi AMDAL kegiatan industri gula rafinasi','PT. Duta Sugar Internasional','Desa argawana, kecamatan pulo ampel Kab. Serang – prov.banten',2016,1,'2018-06-04 21:40:45','2018-06-04 21:40:45'),
	(99,1,'Rencana Kegiatan Dampak Lingkungan (AMDAL) Penambangan Batu Bara','PT.INJATANA','Desa Putri Hijau,Kabupaten Bengkulu Utara Provinsi Bengkulu',2016,1,'2018-06-04 21:41:07','2018-06-04 21:41:07'),
	(100,1,'P senyusunan Studi Analisa Dampaak Lingkungan (AMDAL) Perkebunan Kelapa Sawit','PT.Bio budidaya nabati','Di distrik senggi,kabupaten keerom provinsi papua',2016,1,'2018-06-04 21:41:30','2018-06-04 21:41:30'),
	(101,1,'P penyusunan AMDAL TPA dan IPLT Karadiri Distrik Wanggar Kabupaten Nabire','TPA dan IPLT','Kabupaten Nabire Provinsi Papua',2016,1,'2018-06-04 21:41:52','2018-06-04 21:41:52'),
	(102,1,'Metode Monitoring AMDAL Periode Semester II','PT. ABCKOGEN','JL. H. R. Rasuna said kav,1 kuningan jakarta',2016,1,'2018-06-04 21:42:13','2018-06-04 21:42:27'),
	(103,1,'Metode Monitoring AMDAL Periode Sementara II','Pt. Raffles Pacific Harvest','Desa tarogong kaler garut jawabarat',2016,1,'2018-06-04 21:42:48','2018-06-04 21:42:48'),
	(104,1,'Penyusan Dokumen Studi AMDAL Terpadu Taman Wisata Alam (TWA) 15ha, Pembangunan Pembangkit Listrik Tenaga Mini Hidro (PLTM) 2x 1,5 Mw Dan Akses Jalan 3,2 Km Ke Lokasi PLTM Wabudori','Dinas Kehutan Dan Lingkungan Hidup','JL. Raya sorendiwesupiori kabupaten supiori papua.',2016,1,'2018-06-04 21:43:17','2018-06-04 21:43:17'),
	(105,1,'Penyusunan AMDAL Pembangunan Sor Baribis Kabupaten Majalengka','Dinas Bina Marga Dan Cipta Karya','JL.K. H. Abdul Halim Majalengka',2016,1,'2018-06-04 21:43:53','2018-06-04 21:43:53'),
	(106,1,'Monitoring ANDAL,RKL,RPL PT. NEW ASIA INDUSTRY ESTATE','PT. New Asia Industry Estate','JL.Raya Jakarta-Serang Cikand Serang Banten',2016,1,'2018-06-04 21:44:12','2018-06-04 21:44:12'),
	(107,1,'Setudi Analisa Mengenai Dampak Lingkungan (KA),(ANDAL),(RKL,RPL) Dan Kajian Geohidrologi Pembangunan Mall,Apartement Dan Hotel “Bandung Central Park','PT. Jabar Bumi Konstruksi','Kota Bandung',2016,1,'2018-06-04 21:44:34','2018-06-04 21:44:34'),
	(108,1,'Setudi Analisa Mengenai Dampak Lingkungan (KA),(ANDAL),(RKL,RPL) Dan Kajian Geohidrologi Pembangunan Mall,Apartement Dan Hotel “Bandung Central Park','PT. Jabar Bumi Konstruksi','Kota Bandung',2016,1,'2018-06-04 21:44:34','2018-06-04 21:44:34'),
	(109,1,'Penyusunan Amdal Perkebunan Kelapa Sawit','Pt. Semarak Agri Lestari','Kab.Kerom, Prov . Papua',2016,1,'2018-06-04 21:44:55','2018-06-04 21:44:55'),
	(110,1,'Rencana Pembangunan Jalan Koridor Angkuran Kayu','TP.Atlantik Bina Persada','Kec. Penajam, Kab. Penajam Paser Utara Kalimantan Timur',2016,1,'2018-06-04 21:45:15','2018-06-04 21:45:15'),
	(111,1,'Studi AMDAL Rencana Kegiatan Penambangan Pasir Laut','PT. Gahana Lautan Prima','Gd. Menara Jamsostek, Jl. Jend. Gatot Subroto No. 38 Jakarta Selatan',2016,1,'2018-06-04 21:45:37','2018-06-04 21:45:37'),
	(112,1,'Studi AMDAL Pengembangan Gedung Rumah Sakit Umum Daerah R. Syamsudin, S.H. Kota Sukabumi.','RSUD R. Syamsudin, S.H. Kotamadya Sukabumi','RSUD R. Syamsudin, S.H. Kotamadya Sukabumi',2017,1,'2018-06-04 21:46:00','2018-06-04 21:46:00'),
	(113,1,'Studi AMDAL Kegiatan Industri Gula Rafinasi','PT. Duta Suhar International','Jl. Kuningan Mulia Blok 9B Jakarta',2017,1,'2018-06-04 21:46:22','2018-06-04 21:46:22'),
	(114,1,'Studi ADD AMDAL Tipe B','PT. Pupuk Kujang Indonesia','Jl. Jend. A. Yani No. 39 Cikampek',2017,1,'2018-06-04 21:46:41','2018-06-04 21:46:41'),
	(115,1,'Studi AMDAL Rencana Kegiatan Penambangan Batubara','PT. Bencoolen Mining','Jakarta',2017,1,'2018-06-04 21:47:03','2018-06-04 21:47:03'),
	(116,1,'Studi AMDAL Rencana Kegiatan Penambangan Batubara','PT. Injatama','Jakarta',2017,1,'2018-06-04 21:47:24','2018-06-04 21:47:24'),
	(117,1,'Consultancy Services For ESIA Matenggeng Pumped Storage Project, Indonesia – JV Agreement','PT. PLN (Persero)','Jakarta',2017,1,'2018-06-04 21:47:48','2018-06-04 21:47:48'),
	(118,1,'Studi AMDAL','PT. Feng Tay Enterprises','Jl. Raya Banjaran KM 14.6, Bandung',2017,1,'2018-06-04 21:48:11','2018-06-04 21:48:11'),
	(119,1,'Studi AMDAL Kebun Karet dan Pengolahan Karet','PT. Air Muring','Kabupaten Bengkulu Utara, Provinsi Bengkulu',2017,1,'2018-06-04 21:48:31','2018-06-04 21:48:31'),
	(120,1,'Revisi Addendum AMDAL','PT. Angles Products','Jl. Raya Bojonegara Serang - Banten',2017,1,'2018-06-04 21:48:49','2018-06-04 21:48:49'),
	(121,1,'Studi AMDAL Rencana Penambangan Emas di Seluma, Provinsi Bengkulu','PT. Energi Swa Dinamika','Jl. Cisanggiri V No. 8 Kebayoran Baru, Jakarta Selatan',2017,1,'2018-06-04 21:49:24','2018-06-04 21:49:24'),
	(122,1,'Studi AMDAL Rencana Kegiatan Pembangunan Villa, Condotel dan Golf Club House','PT. KURNIA PARAHYANGAN SEJAHTERA','Jl. Let. Jend. MT. Haryono Kav. 2-3 Pancoran',2017,1,'2018-06-04 21:49:44','2018-06-04 21:49:44'),
	(123,2,'Studi UKL & UPL Kegiatan Pengerukan Alur Kapal dan Pemeliharaan Kolam Dermaga','PT. Samudra Marine Indonesia','Serang, Propinsi Banten',2010,1,'2018-06-04 22:00:54','2018-06-04 22:00:54'),
	(124,2,'Studi UKL & UPL Pengembangan Lapangan Migas Terbatas Ringgit','Kondur Petroleum S.A','Kab. Kepulauan Meranti, Provinsi Riau',2010,1,'2018-06-04 22:01:20','2018-06-04 22:01:20'),
	(125,2,'Study UKL & UPL Must Be Initiated Prior To Commencing Construction Activities To Determine The Menas/Efforts To Anage The Invronmental Impact Related To The Activities','Kondur Petroleum S.A - Field','Kurau, Kepualaun Meranti Riau',2011,1,'2018-06-04 22:01:37','2018-06-04 22:03:21'),
	(126,2,'Studi UKL & UPL Kegiatan Pemboran Sumur Exploitasi LGK #29, LGK #30, LGK #31','PT. SPR Langgak','Rokan Hulu, Provinsi Riau',2011,1,'2018-06-04 22:02:01','2018-06-04 22:02:59'),
	(127,2,'Studi UKL dan UPL Kegiatan Pemboran Sumur Gas Ekplorasi Mantiko Kabupaten Kampar','BOB PT. Bumi Siak Pusako','Zamrud, Kab. Siak',2011,1,'2018-06-04 22:02:26','2018-06-04 22:03:09'),
	(128,2,'Studiy UKL & UPL That Cover All Activities Exploration Wells Included Cores Hole Drilling In GMB Tabulako – Tanah Laut Bumbu – South Kalimantan','PT. Artha Widya Persada','Kalimantan Selatan',2011,1,'2018-06-04 22:03:55','2018-06-04 22:03:55'),
	(129,2,'Study UKL & UPL WKP GMB Sangatta II That Cover All Activities Exploration Wells Included Cores Hole Drilling In GMB Sangatta II Kutai Timur - Kalimantan Timur','PT. Visi Multi Artha','Kutai Timur – Kalimantan Timur',2011,1,'2018-06-04 22:04:15','2018-06-04 22:04:15'),
	(130,2,'Studi DELH Kegiatan Kawasan Industri Modern Cikande','PT. Puncak Ardimulia Reality','Serang',2011,1,'2018-06-04 22:04:40','2018-06-04 22:04:40'),
	(131,2,'Studi UKL & UPL Kegiatan TWA Cimanggu','Perum Perhutani Unit III Jawa Barat dan Banten','Kab. Bandung',2011,1,'2018-06-04 22:05:01','2018-06-04 22:05:01'),
	(132,2,'Studi UKL & UPL Pengembangan Lapangan Gas Bentu','Kalila (Bentu) Ltd','Kab. Kampar, Riau',2011,1,'2018-06-04 22:05:31','2018-06-04 22:05:31'),
	(133,2,'Studi UKL & UPL Rencana Kegiatan Seismik 3D di Lapangan Linda Sele','BN Oil Holdico Ltd','Kab. Sorong, Papua Barat',2011,1,'2018-06-04 22:05:50','2018-06-04 22:06:53'),
	(134,2,'Studi UKL & UPL Rencana Kegiatan Pemboran Sumur Eksploitasi di Lapangan Linda Sele','BN Oil Holdico Ltd','Kab. Sorong, Papua Barat',2011,1,'2018-06-04 22:06:11','2018-06-04 22:06:38'),
	(135,2,'Studi UKL & UPL Rencana Kegiatan Pemboran Sumur Eksploitasi dan Eksplorasi Langgak Field','SPR Langgak','Kab. Rokan Hulu & Kab. Kampar, Riau',2011,1,'2018-06-04 22:06:30','2018-06-04 22:06:30'),
	(136,2,'Studi UKL & UPL Pengembangan Terbatas Lapangan Migas Sungai Gelam “Jambi Area”','TAC PT. PERTAMINA EP – PT. Insani Mitrasani Gelam','Kab. Muaro Jambi,',2011,1,'2018-06-04 22:07:36','2018-06-04 22:07:36'),
	(137,2,'Studi UKL & UPL Pembangunan LPG Plant di Lapangan Kuat','Kondur Petroleum SA','Kab. Kepulauan Meranti, Riau',2011,1,'2018-06-04 22:08:01','2018-06-04 22:08:01'),
	(138,2,'Studi UKL & UPL Rencana Pembangunan Industri Serat Buatan','PT. Indorama Polychem Indonesia','Kab. Purwakarta, Jawa barat',2011,1,'2018-06-04 22:08:23','2018-06-04 22:08:23'),
	(139,1,'Studi UKL & UPL Rencana Pengerukan Sungai Ketahun dan Pembangunan Dermaga 2','PT. Injatama','Kab. Bengkulu Utara, Bengkulu',2011,1,'2018-06-04 22:08:42','2018-06-04 22:08:42'),
	(140,2,'Studi UKL & UPL Rencana Pembangunan Industri Ban','PT. Hankook Tire Indonesia','Kab. Bekasi, Jawa barat',2011,1,'2018-06-04 22:09:07','2018-06-04 22:09:07'),
	(141,2,'Studi UKL & UPL Rencana Kegiatan Pembangunan dan Pengoperasian Fasilitas Pipa Gas Bawah Laut Kuarau - Lalang','EMP Malacca Strait SA','Kab. Kepulauan Meranti dan Kab. Siak, Riau',2012,1,'2018-06-04 22:09:27','2018-06-04 22:09:41'),
	(142,2,'Environmental Permit Obtaining from Kabupaten Kepulauan Meranti for Kuat LPG Plant Development','EMP Malacca Strait S.A','Pekanbaru',2012,1,'2018-06-04 22:10:11','2018-06-04 22:10:11'),
	(143,2,'Studi UKL & UPL Kegiatan Pembangunan Fasilitas Pipa Gas Bawah Laut Kurau - Lalang','Kondur Petroleum S. A','Pekanbaru',2012,1,'2018-06-04 22:10:33','2018-06-04 22:10:33'),
	(144,2,'Studi UKL & UPL FTTM ITB','Pejabat Pembuat Komitmen Institu Teknologi Bandung','Bandung',2013,1,'2018-06-04 22:10:50','2018-06-04 22:11:00'),
	(145,2,'Studi UKL & UPL SBM ITB','Pejabat Pembuat Komitmen Institu Teknologi Bandung','Bandung',2013,1,'2018-06-04 22:11:22','2018-06-04 22:11:22'),
	(146,2,'Studi UKL & UPL Kegiatan Pengolahan Minyak Kelapa Sawit','PT. Multimas Nabati Asahan','Serang',2013,1,'2018-06-04 22:11:50','2018-06-04 22:11:50'),
	(147,2,'Studi UKL & UPL Eksplorasi Panas Bumi','PT. Tangkuban Parahu Geothermal Power','Kab. Bandung Barat',2013,1,'2018-06-04 22:12:10','2018-06-04 22:12:10'),
	(148,2,'Studi UKL & UPL Kawasan Indsutri Modern Cikande','PT. Samator','Kabupaten Serang',2013,1,'2018-06-04 22:12:32','2018-06-04 22:12:32'),
	(149,2,'Studi UKL & UPL','PT. Siemens Industrial Power','Bandung',2013,1,'2018-06-04 22:12:55','2018-06-04 22:12:55'),
	(150,2,'Studi UKL-UPL Kegiatan Industri Pemotongan kertas Tissue.','PT. The Univenus','Provinsi banten',2013,1,'2018-06-04 22:13:13','2018-06-04 22:13:13'),
	(151,2,'Studi UKL-UPL Pembangunan Gedung Asrama Mahasiswa Sangkuriang Tahap II Institut Teknologi Bandung.','ITB (Institut Teknologi Bandung)','Jalan Ganesa No. 10 Bandung',2014,1,'2018-06-04 22:13:36','2018-06-04 22:13:36'),
	(152,2,'Pelaksanaan Pekerjaan Studi UKLUPL.','PT. Moririn Living Indonesia','Jl. Raya CicalengkaMajalaya KM 2 Kabupaten Bandung, Provinsi Jawa Barat.',2014,1,'2018-06-04 22:13:56','2018-06-04 22:13:56'),
	(153,2,'Pelaksanaan Pekerjaan Studi UKLUPL Kegiatan Pembangunan Jembatan Cinambo','IMMANUEL GINTING','Gedebage-Bandung',2015,1,'2018-06-04 22:14:16','2018-06-04 22:14:16'),
	(154,2,'Pelaksanaan Pekerjaan Studi UKLUPL Kegiatan Pergudangan.','PT. KAO Indonesia','Jalan Kopo-Katapang km. 11,2, Kabupaten Bandung rovinsi Jawa Barat.',2015,1,'2018-06-04 22:14:41','2018-06-04 22:14:41'),
	(155,2,'Pelaksanaan Pekerjaan Studi UKLUPL Kegiatan Pengumpul Limbah B3 dan Pengurusan Izin Pengumpul Limbah B3.','PT. Energi prima Nusantara','Jalan Minangkabau No. 28 Manggarai-Jakarta Selatan',2015,1,'2018-06-04 22:14:59','2018-06-04 22:14:59'),
	(156,2,'Penyusunan Upaya Pengelolaan Lingkungan Hidup Ukl Dan Upaya Pemantauwan Lingkungan Hidup (Upl) Rencana Kegiatan Pembangunan Industri Pemotongan Kayu PT. Sineira Rimba Belantara','PT. ATLANTIK BINA PERSADA','Di Kelampai, Desa Kedongorang, Kab.Ketapang Prov.Kalimantan Barat',2016,1,'2018-06-04 22:15:28','2018-06-04 22:15:28'),
	(157,2,'Penyusunan Studi Upaya Pengelolaan Lingkungan Dan Upaya Pemantauan (UKP-UPL) Dan Ijin Lingkungan Kegiatan Industry','PT. Korin Jungwoo','Kebon Suuk Cicalengka Kabupaten Bandung Provinsi Bandung',2016,1,'2018-06-04 22:15:51','2018-06-04 22:15:51'),
	(158,2,'Penyusunan Dokumen (UKL.UPL Rencana Pembangunan Dermaga (LOGPOND)','PT. WIJAYA SENTOSA','KAB.TELUK WONDAMA, PROV. PAPUA BARAT',2016,1,'2018-06-04 22:16:13','2018-06-04 22:16:13'),
	(159,2,'Penyusunan Dokumen (UKL-UPL) Rencana Kegiatan Pengembangan Pabrik','PT. YAKULT INDONESIA PERSADA','Kawasan Industri Indolakto, Desa Pesawahan, Kecamatan Cicurug, Kabupaten Sukabumi, Propinsi Jawa Barat',2017,1,'2018-06-04 22:16:31','2018-06-04 22:16:31'),
	(160,2,'Penyusunan Dokumen (UKL-UPL) Rencana Kegiatan Eksplorasi Penambangan Nikel','PT. IRIANA MUTIARA MINING','Kabupaten Sarmi, Provinsi Papua',2017,1,'2018-06-04 22:16:52','2018-06-04 22:16:52'),
	(161,3,'Monitoring Kegiatan Fly Ash','Monitoring Kegiatan Fly Ash','Pelalawan, RIau',2010,1,'2018-06-04 22:17:28','2018-06-04 22:17:28'),
	(162,3,'Monitoring UKL & UPL Kegitan Indusri Textile','PT. Shinko Toyobo Gistex','Cileunyi Kab. Bandung',2010,1,'2018-06-04 22:17:46','2018-06-04 22:17:46'),
	(163,3,'Monitoring Kualitas Udara Emisi & Ambien','PT. Indorama Synthetics Tbk.','Purwakarta',2010,1,'2018-06-04 22:18:07','2018-06-04 22:18:07'),
	(164,3,'Monitoring Kualitas Udara Emisi & Ambien','PT. Indorama Synthetics Tbk.','Purwakarta',2011,1,'2018-06-04 22:18:27','2018-06-04 22:18:27'),
	(165,3,'Monitoring Kualitas Air','PT. Indorama Synthetics Tbk.','Purwakarta',2011,1,'2018-06-04 22:18:53','2018-06-04 22:18:53'),
	(166,3,'Monitoring UKL & UPL Kegitan Indusri Textile','PT. Shinko Toyobo Gistex','Cileunyi Kab. Bandung',2011,1,'2018-06-04 22:19:10','2018-06-04 22:19:10'),
	(167,3,'Monitoring Kualitas Udara Emisi & Ambien PLTU','PT. Indorama Synthetics Tbk.','Purwakarta',2011,1,'2018-06-04 22:19:28','2018-06-04 22:19:28'),
	(168,3,'Monitoring Kualitas Udara Emisi & Ambien PLTU','PT. Riau Andalan Pulp & Paper','Pelalawan, Riau',2011,1,'2018-06-04 22:19:52','2018-06-04 22:19:52'),
	(169,3,'Monitoring Study AMDAL Kegiatan Pembangunan Industri Pulp, Chip, Wood Pellet & Fasilitas Pendukungnya','PT. Medcopapua Indonesia Lestari','Provinsi Papua',2011,1,'2018-06-04 22:20:13','2018-06-04 22:20:13'),
	(170,3,'Monitoring Studi AMDAL Kegiatan Izin Pemanfaatan Hasil Hutan Kayu Pada Hutan Tanaman (IUPHHK-HT) HTI Pulp & Fasilitas Pendukungnya','PT. Selaras Inti Semesta','Provinsi Papua',2011,1,'2018-06-04 22:20:55','2018-06-04 22:20:55'),
	(171,3,'Environmental Monitoring Mosesa Petroleum 2012','PT. EMP - Tonga','Sumatera Utara',2012,1,'2018-06-04 22:21:14','2018-06-04 22:21:14'),
	(172,3,'Pemantauan Lingkungan Dalam Rangka Proper 2012 Semester II dan Pemboran Tahun 2012','MP Malacca Strait S.A','Kurau – Pekanbaru',2012,1,'2018-06-04 22:21:30','2018-06-04 22:21:30'),
	(173,3,'Monitoring Kualitas Udara & Emisi','PT. Indorama Synthetics Tbk.','Kab. Purwakarta',2012,1,'2018-06-04 22:21:50','2018-06-04 22:21:50'),
	(174,3,'Monitoring Kualitas Air','PT. Indorama Synthetics Tbk.','Kab. Purwakarta',2012,1,'2018-06-04 22:22:14','2018-06-04 22:22:14'),
	(175,3,'Monitoring Kualitas Udara & Emisi','PT. Indorama Synthetics Tbk.','Kab. Purwakarta',2013,1,'2018-06-04 22:22:38','2018-06-04 22:22:38'),
	(176,3,'Monitoring Kualitas Air','PT. Indorama Synthetics Tbk.','Kab. Purwakarta',2013,1,'2018-06-04 22:22:58','2018-06-04 22:22:58'),
	(177,3,'Monitoring UKL UPL Industri Garment','PT. Shinko Toyobo Gistex Garment','Kab. Bandung',2013,1,'2018-06-04 22:23:19','2018-06-04 22:23:32'),
	(178,3,'Studi Monitoring Kualitas Udara Emisi & Ambien','PT. Indorama Synthetics Tbk.','Kembang-Kuning, Ubrug Jatiluhur - Purwakarta',2015,1,'2018-06-04 22:24:37','2018-06-04 22:25:29'),
	(179,3,'Studi Monitoring Kualitas Air','PT. Indorama Synthetics Tbk.','kembang kuning, ubrug Jatiluhur, Purwakarta',2015,1,'2018-06-04 22:26:20','2018-06-04 22:26:20'),
	(180,3,'Studi Monitoring Kualitas Udara Emisi & Ambien','PT. Indorama Synthetics Tbk.','kembang kuning, ubrug Jatiluhur, Purwakarta',2016,1,'2018-06-04 22:27:49','2018-06-04 22:27:49'),
	(181,3,'Studi Monitoring Kualitas Air','PT. Indorama Synthetics Tbk.','kembang kuning, ubrug Jatiluhur, Purwakarta',2016,1,'2018-06-04 22:28:41','2018-06-04 22:28:41'),
	(182,3,'Jasa pembuatan laporan RKL/RPL semester 1 dan 2','PT. Transportasi gas indonesia','Jl. Kebon Sirih Raya No.1 Jakarta',2016,1,'2018-06-04 22:29:04','2018-06-04 22:29:04'),
	(183,3,'Studi Monitoring Kualitas Udara Emisi & Ambien','PT. Indorama Synthetics Tbk.','kembang kuning, ubrug Jatiluhur, Purwakarta',2017,1,'2018-06-04 22:29:31','2018-06-04 22:29:31'),
	(184,3,'Studi Monitoring Kualitas Air','PT. Indorama Synthetics Tbk.','kembang kuning, ubrug Jatiluhur, Purwakarta',2017,1,'2018-06-04 22:29:53','2018-06-04 22:29:53'),
	(185,3,'Studi Monitoring UKL & UPL','PT. PLN (Perseo) PUSHARLIS UWP IV','Jl. Raya Dayeuhkolot Km, 9 Bandung',2017,1,'2018-06-04 22:30:29','2018-06-04 22:30:29'),
	(186,3,'Monitoring Studi AMDAL Semester I & II Tahun 2017','PT. Wijaya Karya Bangunan Gedung','Jl. Tera No. 28 Bandung',2017,1,'2018-06-04 22:30:46','2018-06-04 22:30:46'),
	(187,5,'Pemantauan Kondisi Sosial, Ekonomi, Budaya dan Persepsi Masyarakat Desa Sekitar Kebun/Pabrik Tahun 2010','PT. Perkebunan Nusantara V (Persero)','Pekanbaru',2010,1,'2018-06-04 22:31:33','2018-06-04 22:31:33'),
	(188,5,'Penyusunan Lajporan Studi Deliniasi Mikro Usaha Pemanfaatan Hasil Hutan Kayu Dalam Hutan Tanaman','PT. Selaras Inti Semesta','Merauke',2010,1,'2018-06-04 22:31:54','2018-06-04 22:31:54'),
	(189,5,'Studi Dampak Rencana Kegiatan di Operasi CPI','PT. Chevron Pacific Indonesia','Pekanbaru',2011,1,'2018-06-04 22:32:17','2018-06-04 22:32:17'),
	(190,5,'Studi Harmonisasi, Persepsi Masyarakat dan Kesempatan Kerja','BOB PT. BSP – Pertamina Hulu','Pekanbaru',2011,1,'2018-06-04 22:32:42','2018-06-04 22:32:42'),
	(191,5,'Pelaksanaan Jasa Proper Maintenance Lapangan Tiaka, Blok Toili,','JOB Pertamina – Medco E&P Tomori Sulawesi','Sulawesi Tengah',2011,1,'2018-06-04 22:33:04','2018-06-04 22:33:04'),
	(192,5,'Penyusunan Dokumen Evaluasi Lingkungan Hidup (DELH)','PT. Perkebunan Nusantara VIII (Persero)','Bandung',2011,1,'2018-06-04 22:33:24','2018-06-04 22:33:24'),
	(193,5,'Penyusunan Dokumen Evaluasi Lingkungan Hidup (DELH) Kegiatan Kawasan Industri Modern Cikande, Luas Lahan 250 Ha meliputi Desa Cijeruk, Nambo Ilir dan Barengkok','PT. Puncak Ardimulia Reality','Kab. Serang',2011,1,'2018-06-04 22:33:46','2018-06-04 22:33:46'),
	(194,5,'Environmental Social and Health Impact Assessment (ESHIA)','PT. Chevron Pacific Indonesia','Rumbai',2011,1,'2018-06-04 22:34:04','2018-06-04 22:34:04'),
	(195,5,'Memberikan Jasa Kunsultasi untuk Survei Pengukuran Topografi dan Pemetaan 66 Ha','PT. Riau Andalan Pulp and Papaer','Provinsi Riau',2013,1,'2018-06-04 22:34:25','2018-06-04 22:34:25'),
	(196,5,'Pengurusan Ijin Lokasi','PT. Conch Cemen Indoensia','Kecamatan Puloampel Kabupaten Serang, Provinsi Banten',2014,1,'2018-06-04 22:34:45','2018-06-04 22:34:45'),
	(197,5,'Preparation Assistance on Livelihood Restoration Program Form Patimban Port Develpment Project','IDEA Consultants, Inc.','Serang',2017,1,'2018-06-04 22:35:06','2018-06-04 22:35:06'),
	(198,5,'Penyusunan Dokumen Evaluasi Lingkungan Hidup (DELH) Kampus Institut Teknologi Bandung','Institut Teknologi Bandung','Jl. Ganesa No. 10 Bandung',2017,1,'2018-06-04 22:35:30','2018-06-04 22:35:30'),
	(199,5,'Penyusunan Dokumen Evaluasi Lingkungan Hidup (DELH) Rencana Kegiatan Pembangunan Logpond dan Dermaga PT. FSS','PT. Atlantik Bina Persada','Jl. Batik Jonas Nomor 29 Bandung',2017,1,'2018-06-04 22:35:53','2018-06-04 22:35:53'),
	(200,5,'nyusunan Dokumen Evaluasi Lingkungan Hidup (DELH)','PT. PG Rajawali I','Jl. Dr. Wahidin No. 46 Cirebon',2017,1,'2018-06-04 22:36:14','2018-06-04 22:36:14'),
	(201,5,'Penyusunan Dokumen Kajian Lingkungan Untuk Keperluan Kelengkapan Izin Lokasi','PT. Kurnia Parahyangan Sejahtera','Jl. Let. Jend. MT. Haryono Kav. 2-3',2017,1,'2018-06-04 22:36:33','2018-06-04 22:36:33'),
	(202,1,'Pekerjaan Penyusunan Dokumen Fisibility Study Rencana Kegiatan Penambangan Emas','PT. Energi Swa Dinamika','Jl. Cisanggiri V No. 8 Kebayoran Baru, Jakarta Selatan',2017,1,'2018-06-04 22:37:11','2018-06-04 22:37:11'),
	(203,4,'Study Environmental Baseline Assessment','PT. Mosesa Petroleum','Padang, Sumatera Barat',2010,1,'2018-06-04 22:38:20','2018-06-04 22:38:20'),
	(204,4,'Study Environmental Baseline Assessment GMB Tabulako','PT. Artha Widya Persada','Kalimantan Selatan',2010,1,'2018-06-04 22:38:47','2018-06-04 22:38:47'),
	(205,4,'Study Environmental Baseline Assessment GMB Sangatta II','PT. Visi Multi Artha','Kalimantan Timur',2010,1,'2018-06-04 22:39:07','2018-06-04 22:39:07'),
	(206,4,'Study Environmental Baseline Assessment','PT. Tropic Energi Pandan','Palembang',2010,1,'2018-06-04 22:39:39','2018-06-04 22:39:39'),
	(207,4,'Study Environmental Baseline Assessment Atas Rencana Pemboran Di Lapangan Langgak','PT. SPR Langgak','Rokan Hulu – Kampar , Provinsi Riau',2011,1,'2018-06-04 22:40:02','2018-06-04 22:40:02'),
	(208,4,'Study Environmental Baseline Asessment','PT. Mentari Pambuang International','Kalimantan Tengah',2012,1,'2018-06-04 22:40:21','2018-06-04 22:40:21');

/*!40000 ALTER TABLE `portfolios` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table regulations
# ------------------------------------------------------------

DROP TABLE IF EXISTS `regulations`;

CREATE TABLE `regulations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_category_regulation` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `filename` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `fileext` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `regulations_id_category_regulation_foreign` (`id_category_regulation`),
  CONSTRAINT `regulations_id_category_regulation_foreign` FOREIGN KEY (`id_category_regulation`) REFERENCES `category_regulations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table structures
# ------------------------------------------------------------

DROP TABLE IF EXISTS `structures`;

CREATE TABLE `structures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;



# Dump of table teams
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teams`;

CREATE TABLE `teams` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `id_category_team` int(10) unsigned NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bidang` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `pendidikan` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `teams_id_category_team_foreign` (`id_category_team`),
  CONSTRAINT `teams_id_category_team_foreign` FOREIGN KEY (`id_category_team`) REFERENCES `category_teams` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `teams` WRITE;
/*!40000 ALTER TABLE `teams` DISABLE KEYS */;

INSERT INTO `teams` (`id`, `id_category_team`, `name`, `bidang`, `pendidikan`, `created_at`, `updated_at`)
VALUES
	(1,1,'Drs. Bambang Kusharyadi','Ahli Biologi Terestial','S1 Biologi, UNSOED','2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(2,1,'Drs. Azis Rahman','Ahli Biologi Terestial','S1 Biologi, UNSOED','2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(3,2,'Dra. Neneng Nurbaeti Amin, M.Se','Ahli Sosekbud','S2 Universitas Indonesia','2018-06-09 04:46:38','2018-06-09 04:46:38'),
	(4,1,'Heri Mulyadi, S.Si','Ahli Biologi Terestrial','S1 Biologi, UPI','2018-05-28 11:29:34','2018-05-28 11:29:34'),
	(5,1,'Irman Ruhimat. S.Si, MSi','Ahli Biologi Aquatiq','S2 Biologi, ITB','2018-05-28 11:30:06','2018-05-28 11:30:06'),
	(6,1,'Dadan Ramdan S.Si','Ahli Biologi Aquatiq','S1 Biologi. UNPAD','2018-05-28 11:30:37','2018-05-28 11:30:37'),
	(7,1,'Haikal Suhaidi S.Si','Ahli Biologi Aquatiq','S1 Bilogi, UNPAD','2018-05-28 11:31:05','2018-05-28 11:31:05'),
	(8,1,'Drs. Mohammad Taufiq Afiff, M.Sc.','Ahli Biologi','S2 Biologi, ITB','2018-05-28 11:31:29','2018-05-28 11:31:29'),
	(9,2,'Dra. Neneng Nurbaeti Amin, M.Se','Ahli Sosekbud','S2 Universitas Indonesia','2018-05-28 11:32:01','2018-05-28 11:32:01'),
	(10,2,'Ir. Engkus Kusnadi Wikarta, M.SP','Ahli Sosial Ekonomi dan Budaya','S1 ITB','2018-05-28 11:33:03','2018-05-28 11:33:03'),
	(11,1,'Joko Edi Santosa SE','Ahli Sosial Ekonomi dan Budaya','S1 Ekonomi, UNWIKU','2018-05-28 11:33:41','2018-05-28 11:33:41'),
	(12,2,'Jonyanis','Ahli Sosial Ekonomi dan Budaya','S1 Bidang Sosial Universitas Riau','2018-05-28 11:34:16','2018-05-28 11:34:16'),
	(13,2,'Endang Mulyadi. A.K.,S.Hut, M.Si','Ahli Sosekbudkesmas','S2 MAGISTER SAIN','2018-05-28 11:35:15','2018-05-28 11:35:15'),
	(14,2,'R. Aisah Nur Aisah, S.Sos','Ahli Sosial Ekonomi','S1 Ekonomi, UNPAD','2018-05-28 11:35:51','2018-05-28 11:35:51'),
	(15,2,'Dr. Ir. Urip Rahmani, M.Si','Ahli Sosial Ekonomi','S3 Ekonomi Pertanian, IPB','2018-05-28 11:36:58','2018-05-28 11:36:58'),
	(16,2,'Irba Djaja, SP, M.Si','Ahli  Sosial Ekonomi','S2 Lingkungan, UNHAS','2018-05-28 11:37:51','2018-05-28 11:37:51'),
	(17,3,'Ir. Wahyono, M.Sc','Ahli Geodesi','S2 Geologi, UGM','2018-05-28 11:38:30','2018-05-28 11:38:30'),
	(18,3,'Ir. Fejri Rahman','Ahli Geodesi','S1 Geodesi, Universitas Institut Teknologi','2018-05-28 11:39:06','2018-05-28 11:39:06'),
	(19,4,'Ir. Djajin Praptoharjo','Ahli Geologi','S1 Geologi, UPN, Jogjakarta','2018-05-28 11:39:27','2018-05-28 11:39:27'),
	(20,4,'Ir. Ivan Wisnuviantoro','Ahli Geologi','S1 Geologi, UNPAD','2018-05-28 11:40:12','2018-05-28 11:40:12'),
	(21,4,'Syarief Hidayat, M.Sc','Ahli Geologi','S2 Geologi, Belanda','2018-05-28 11:40:56','2018-05-28 11:40:56'),
	(22,4,'Ir. Agus Setiabudi, M.Sc','Ahli Geologi Kelautan','S2 Geologi, ITB','2018-05-28 11:41:19','2018-05-28 11:41:19'),
	(23,1,'Ir. Sudarmin','Ahli Geologi Lingkungan','S1 Geologi, UGM','2018-05-28 11:41:44','2018-05-28 11:41:44'),
	(24,4,'Ir. Rustam','Ahli Geologi Lingkungan','S1, Geologi UNPAD','2018-05-28 11:42:13','2018-05-28 11:42:13'),
	(25,5,'Ir. Dedi Bisri Dipl. WM,MT','Ahli Pertambangan','S1 Pertambangan, UNPAD','2018-05-28 11:42:40','2018-05-28 11:42:40'),
	(26,5,'Ir. Adip Mustofa','Ahli Pertambangan','S1 Teknik Geologi','2018-05-28 11:43:12','2018-05-28 11:43:12'),
	(27,6,'Ir. Mukit','Ahli Hidrologi','S1 Pengairan, IPB','2018-05-28 11:43:36','2018-05-28 11:43:36'),
	(28,7,'Ir. Hery Wahyono','Ahli Hidrooceanografi','S1 Metrologi, ITB','2018-05-28 11:44:06','2018-05-28 11:44:06'),
	(29,7,'Ir. Mohamad Ali','Ahli Hidrooceanografi','S1 Metrologi, ITB','2018-05-28 11:44:36','2018-05-28 11:44:36'),
	(30,7,'Bram Fatahillah, ST., MT.','Ahli Hidrooceanografi','S2 Geodesi dan Geomatika, ITB','2018-05-28 11:45:06','2018-05-28 11:45:06'),
	(31,8,'Ir. Achmad Kosasih, M.Sc','Ahli Kehutanan','S2 Kehutana, IPB','2018-05-28 11:45:27','2018-05-28 11:45:27'),
	(32,8,'Ir. Ahmad','Ahli Kehutanan (Vegetasi)','S1 Kehutanan, IPB','2018-05-28 11:45:47','2018-05-28 11:46:30'),
	(33,8,'Ir. Djoko Prasetyonohadi','Ahli Kehutanan (Konservasi Sumber Daya Hutan)','S1 Kehutanan, IPB','2018-05-28 11:46:08','2018-05-28 11:46:52'),
	(34,9,'Indri Apriliani, S.KM.','Ahli Kesehatan Masyarakat','S1 Kesehatan, STIKES','2018-05-28 11:47:23','2018-05-28 11:47:23'),
	(35,9,'Heris Ristiwan, S.KM.','Ahli Kesehatan Masyarakat','S1 Kesehatan, UNSIL','2018-05-28 11:47:52','2018-05-28 11:47:52'),
	(36,10,'Drs. Iwan Setiawan','Ahli Kimia','S1 Kimia, UNPAD','2018-05-28 11:48:18','2018-05-28 11:48:18'),
	(37,10,'Dra. Ijah Hadijah','Ahli Kimia','S1 Kimia, UNPAD','2018-05-28 11:48:42','2018-05-28 11:48:42'),
	(38,11,'Dr. Hamonang Siregar, M.Sc','Ahli Lingkungan','S3 Lingungan, UNPAD','2018-05-28 11:49:19','2018-05-28 11:49:19'),
	(39,11,'Arie Fitria Indrayana, ST','Ahli Lingkungan','S1 Lingkungan, WANAYA MUKTI','2018-05-28 11:49:47','2018-05-28 11:49:47'),
	(40,11,'DR. Ir. Rudy Laksmono Widjatno, MT','Ahli Lingkungan','S1 Lingkungan, ITB','2018-05-28 11:50:16','2018-05-28 11:50:16'),
	(41,11,'R. Eka Rosida, ST','Ahli Teknik Lingkungan','S1 Lingkungan, ITENAS','2018-05-28 11:50:43','2018-05-28 11:50:43'),
	(42,12,'Ir. Haryono','Ahli Mekanik Tanah','S1, Pertanian IPB','2018-05-28 11:51:13','2018-05-28 11:51:13'),
	(43,13,'Ir. Maroplo Sianipar','Ahli Pertanian (Sosekbud)','S1 Sosek, IPB','2018-05-28 11:51:39','2018-05-28 11:51:39'),
	(44,13,'Ir. Yuswana','Ahli Pertanian','S1 Pertanian, Gajahmada','2018-05-28 11:52:07','2018-05-28 11:52:07'),
	(45,14,'Ir. Hari Wibowo','Ahi Planologi','S1 Planologi, ITB','2018-05-28 11:52:41','2018-05-28 11:52:41'),
	(46,15,'Ir. Didin Rahmat Z','Sipil','S1 Sipil, UNBRAW','2018-05-28 11:53:20','2018-05-28 11:53:20'),
	(47,15,'Yose Inanda, ST','Ahli Kebisingan','S1 Lingkungan, UNAND','2018-05-28 11:53:57','2018-05-28 11:53:57'),
	(48,15,'Tien Wachyuni, ST','Ahli Sipil Transportasi','S1 Sipil, UNLA','2018-05-28 11:54:18','2018-05-28 11:54:18'),
	(49,16,'Ir. Rahman','Ahli Transportasi','S1 Planologi, UNISBA','2018-05-28 11:54:42','2018-05-28 11:54:42');

/*!40000 ALTER TABLE `teams` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table users
# ------------------------------------------------------------

DROP TABLE IF EXISTS `users`;

CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`)
VALUES
	(1,'Admin WCB','adminwcb@email.com','$2y$10$w9OFDCLV0btfuhuJcnJDWOWNmWH5VTQVSmxL0v/zahpKbKI2XXr6O',NULL,NULL,NULL);

/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
