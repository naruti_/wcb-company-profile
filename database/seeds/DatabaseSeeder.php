<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call([
       UsersTableSeeder::class,
       CertificatesTableSeeder::class,
       CategoryLegalsTableSeeder::class,
       LegalsTableSeeder::class,
       CategoryTeamsTableSeeder::class,
       TeamsTableSeeder::class,
       CategoriesTableSeeder::class,
       CategoryRegulationsTableSeeder::class,



     ]);

    }
}
