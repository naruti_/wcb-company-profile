<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;

class CategoryLegalsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('category_legals')->insert([
          'name' => 'Kementrian Lingkungan Hidup',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_legals')->insert([
          'name' => 'Ikatan Konsultan Indonesia',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_legals')->insert([
          'name' => 'Direktorat Jenderal Pajak',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
    }
}
