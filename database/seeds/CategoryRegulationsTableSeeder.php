<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;


class CategoryRegulationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('category_regulations')->insert([
          'name' => 'Perautan Menteri Negara Lingkungan Hidup',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_regulations')->insert([
          'name' => 'Peraturan Pemerintah',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_regulations')->insert([
          'name' => 'Undang-Undang',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_regulations')->insert([
          'name' => 'Bakumutu Laboratorium - Air',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_regulations')->insert([
          'name' => 'Bakumutu Laboratorium - K3',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_regulations')->insert([
          'name' => 'Bakumutu Laboratorium - Material',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('category_regulations')->insert([
          'name' => 'Bakumutu Laboratorium - Udara',
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
    }
}
