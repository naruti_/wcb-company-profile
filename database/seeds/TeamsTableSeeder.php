<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Carbon;


class TeamsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('teams')->insert([
          'name' => 'Drs. Bambang Kusharyadi',
          'pendidikan' => 'S1 Biologi, UNSOED',
          'bidang' => 'Ahli Biologi Terestial',
          'id_category_team' => 1,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('teams')->insert([
          'name' => 'Drs. Azis Rahman',
          'pendidikan' => 'S1 Biologi, UNSOED',
          'bidang' => 'Ahli Biologi Terestial',
          'id_category_team' => 1,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
      DB::table('teams')->insert([
          'name' => 'Dra. Neneng Nurbaeti Amin, M.Se',
          'pendidikan' => 'S2 Universitas Indonesia',
          'bidang' => 'Ahli Sosekbud',
          'id_category_team' => 2,
          'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
          'updated_at' => Carbon::now()->format('Y-m-d H:i:s')
      ]);
    }
}
