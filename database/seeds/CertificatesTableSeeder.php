<?php

use Illuminate\Database\Seeder;

class CertificatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('certificates')->insert([
          'name' => 'Iwan Setiawan',
          'no_certificate' => '001465/SKPA-P2/LSK-INTAKINDO/IX/2015',
          'qualification' => 'Ketua Tim Penyusun Dokumen AMDAL',
          'kta' => 'K.001.07.09.10.000005'
      ]);

      DB::table('certificates')->insert([
          'name' => 'Aziz Rahman',
          'no_certificate' => '001470/SKPA-P2/LKS-INTAKINDO/VIII/2015',
          'qualification' => 'Ketua Tim Penyusun Dokumen AMDAL',
          'kta' => 'K.001.07.09.10.000004'
      ]);
    }
}
