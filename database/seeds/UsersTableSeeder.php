<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      DB::table('users')->insert([
          'name' => 'Admin WCB',
          'email' => 'adminwcb@email.com',
          'password' => bcrypt('adminwcb'),
      ]);
    }
}
