<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCategoryRegulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create($this->tableName(), function (Blueprint $table)  {
          $table->increments('id');
          $table->string('name',191)->unique();
          $table->tinyInteger('status')->default(1);
          $table->timestamps();
        });
    }

    public function tableName(){
      return 'category_regulations';
    }

    public function down()
    {
        Schema::dropIfExists($this->tableName());
    }
}
