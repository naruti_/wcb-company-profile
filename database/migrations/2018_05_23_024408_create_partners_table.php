<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePartnersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create($this->tableName(), function (Blueprint $table)  {
           $table->increments('id');
           $table->string('name',191);
           $table->string('desc',191);
           $table->string('nomor',191);
           $table->string('alamat',191);
           $table->string('kontak',191);
           $table->tinyInteger('status')->default(1);
           $table->timestamps();
         });
     }

     public function tableName(){
       return 'partners';
     }

     public function down()
     {
         Schema::dropIfExists($this->tableName());
     }
}
