<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGaleriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create($this->tableName(), function (Blueprint $table) {
             $table->increments('id');
             $table->string('name');
             $table->string('img_name');
             $table->string('img_ext');
             $table->string('desc');
             $table->timestamps();
         });
     }

     public function tableName(){
       return 'galeries';
     }
     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists($this->tableName());
     }
}
