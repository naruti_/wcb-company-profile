<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePortfolioTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create($this->tableName(), function (Blueprint $table) {
             $table->increments('id');
             $table->integer('id_category')->unsigned();;
             $table->string('name',191);
             $table->string('client',191);
             $table->string('location',191);
             $table->integer('year')->unsigned();
             $table->tinyInteger('status')->default(1);
             $table->timestamps();
         });
     }

     public function tableName(){
       return 'portfolios';
     }

     /**
      * Reverse the migrations.
      *
      * @return void
      */
     public function down()
     {
         Schema::dropIfExists($this->tableName());
     }
}
