<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTeamTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create($this->tableName(), function (Blueprint $table)  {
           $table->increments('id');
           $table->integer('id_category_team')->unsigned();;
           $table->string('name',191);
           $table->string('bidang',191);
           $table->string('pendidikan',191);
           $table->timestamps();
         });
     }

     public function tableName(){
       return 'teams';
     }

     public function down()
     {
         Schema::dropIfExists($this->tableName());
     }
}
