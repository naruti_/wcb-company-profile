<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateForeignKeys extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table('portfolios', function (Blueprint $table) {
      $table->foreign('id_category')->references('id')->on('categories')
             ->onDelete('cascade')
             ->onUpdate('cascade');
      });

      Schema::table('legals', function (Blueprint $table) {
      $table->foreign('id_category_legal')->references('id')->on('category_legals')
             ->onDelete('cascade')
             ->onUpdate('cascade');
      });

      Schema::table('regulations', function (Blueprint $table) {
      $table->foreign('id_category_regulation')->references('id')->on('category_regulations')
             ->onDelete('cascade')
             ->onUpdate('cascade');
      });

      Schema::table('teams', function (Blueprint $table) {
      $table->foreign('id_category_team')->references('id')->on('category_teams')
             ->onDelete('cascade')
             ->onUpdate('cascade');
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
      Schema::table('portfolios', function (Blueprint $table) {
        $table->dropForeign('portfolios'.'_'.'id_category'.'_foreign');
      });

      Schema::table('legals', function (Blueprint $table) {
        $table->dropForeign('legals'.'_'.'id_category_legal'.'_foreign');
      });

      Schema::table('regulations', function (Blueprint $table) {
        $table->dropForeign('regulations'.'_'.'id_category_regulation'.'_foreign');
      });
      Schema::table('teams', function (Blueprint $table) {
        $table->dropForeign('teams'.'_'.'id_category_team'.'_foreign');
      });

    }
}
