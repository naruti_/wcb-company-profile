<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRegulationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
     public function up()
     {
         Schema::create($this->tableName(), function (Blueprint $table)  {
             $table->increments('id');
             $table->integer('id_category_regulation')->unsigned();;
             $table->string('name',191);
             $table->string('filename',191);
             $table->string('fileext');
             $table->timestamps();
         });
     }

     public function tableName(){
       return 'regulations';
     }

     public function down()
     {
         Schema::dropIfExists($this->tableName());
     }
}
