<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

Route::group(['middleware' => ['auth']], function ($route) {
  $route->get('/admin', 'AdminController@index')->name('admin_dashboard');
  $route->get('/admin/category', 'CategoryController@index')->name('admin_category');
  $route->post('/admin/category/addCategory', 'CategoryController@storeCategory')->name('admin_addCategory');
  $route->get('/admin/category/editCategory/{id}', 'CategoryController@editCategory');
  $route->post('/admin/category/updateCategory/{id}', 'CategoryController@updateCategory');
  $route->get('/admin/category/deleteCategory/{id}', 'CategoryController@deleteCategory');


  $route->get('/admin/portfolio', 'PortfolioController@index')->name('admin_portfolio');
  $route->post('/admin/portfolio/addPortfolio', 'PortfolioController@storePortfolio')->name('admin_addPortfolio');
  $route->get('/admin/portfolio/editPortfolio/{id}', 'PortfolioController@editPortfolio');
  $route->post('/admin/portfolio/updatePortfolio/{id}', 'PortfolioController@updatePortfolio');
  $route->post('/admin/portfolio/deletePortfolio/{id}', 'PortfolioController@deletePortfolio');

  $route->get('/admin/structure', 'About\StructureController@index')->name('admin_strcuture');
  $route->post('/admin/structure/upload', 'About\StructureController@upload');
  $route->post('/admin/structure/delete', 'About\StructureController@delete');

  $route->get('/admin/certificate', 'About\CertificateController@index')->name('admin_strcuture');
  $route->post('/admin/certificate/addCertificate', 'About\CertificateController@store')->name('admin_addCertificate');
  $route->post('/admin/certificate/delete/{id}', 'About\CertificateController@delete');
  $route->get('/admin/certificate/edit/{id}', 'About\CertificateController@edit');
  $route->post('/admin/certificate/update/{id}', 'About\CertificateController@update');

  $route->get('/admin/gallery', 'Experience\GalleryController@index')->name('admin_gallery');
  $route->post('/admin/gallery/addGallery', 'Experience\GalleryController@store')->name('admin_addGallery');
  $route->post('/admin/gallery/delete/{id}', 'Experience\GalleryController@delete');
  $route->get('/admin/gallery/edit/{id}', 'Experience\GalleryController@edit');
  $route->post('/admin/gallery/update/{id}', 'Experience\GalleryController@update');

  // $route->get('/admin/category_regulation', 'CategoryController@index')->name('admin_category_regulation');
  $route->post('/admin/category_regulation/add', 'CategoryRegulationController@store')->name('admin_addCategoryRegulation');
  $route->get('/admin/category_regulation/edit/{id}', 'CategoryRegulationController@edit');
  $route->post('/admin/category_regulation/update/{id}', 'CategoryRegulationController@update');
  $route->get('/admin/category_regulation/delete/{id}', 'CategoryRegulationController@delete');

  $route->get('/admin/regulation', 'Experience\RegulationController@index')->name('admin_regulation');
  $route->post('/admin/regulation/addRegulation', 'Experience\RegulationController@store')->name('admin_addRegulation');
  $route->post('/admin/regulation/update/{id}', 'Experience\RegulationController@update');
  $route->get('/admin/regulation/edit/{id}', 'Experience\RegulationController@edit');
  $route->post('/admin/regulation/delete/{id}', 'Experience\RegulationController@delete');

  $route->post('/admin/category_team/add', 'CategoryTeamController@store')->name('admin_addCategoryTeam');
  $route->get('/admin/category_team/edit/{id}', 'CategoryTeamController@edit');
  $route->post('/admin/category_team/update/{id}', 'CategoryTeamController@update');
  $route->get('/admin/category_team/delete/{id}', 'CategoryTeamController@delete');

  $route->get('/admin/team', 'About\TeamController@index')->name('admin_team');
  $route->post('/admin/team/add', 'About\TeamController@store')->name('admin_addTeam');
  $route->get('/admin/team/edit/{id}', 'About\TeamController@edit');
  $route->post('/admin/team/update/{id}', 'About\TeamController@update');
  $route->post('/admin/team/delete/{id}', 'About\TeamController@delete');

  $route->post('/admin/category_legal/add', 'CategoryLegalController@store')->name('admin_addCategoryLegal');
  $route->get('/admin/category_legal/edit/{id}', 'CategoryLegalController@edit');
  $route->post('/admin/category_legal/update/{id}', 'CategoryLegalController@update');
  $route->get('/admin/category_legal/delete/{id}', 'CategoryLegalController@delete');

  $route->get('/admin/legal', 'About\LegalController@index')->name('admin_legal');
  $route->post('/admin/legal/add', 'About\LegalController@store')->name('admin_addLegal');
  $route->get('/admin/legal/edit/{id}', 'About\LegalController@edit');
  $route->post('/admin/legal/update/{id}', 'About\LegalController@update');
  $route->post('/admin/legal/delete/{id}', 'About\LegalController@delete');

});

// Route::get('/home', 'HomeController@index')->name('home');
Route::get('/beranda', 'FrontendController@index')->name('front_beranda');
Route::get('/', 'FrontendController@index')->name('front_beranda');

Route::get('/profil-perusahaan', 'FrontendController@profil')->name('front_profil');
Route::get('/struktur-organisasi', 'FrontendController@struktur')->name('front_struktur');
Route::get('/sertifikasi', 'FrontendController@sertifikat')->name('front_sertifikasi');
Route::get('/legalitas', 'FrontendController@legalitas')->name('front_legalitas');
Route::get('/tim-ahli', 'FrontendController@timAhli')->name('front_timAhli');
Route::get('/lingkup-pekerjaan', 'FrontendController@lingkupPekerjaan')->name('front_lingkupPekerjaan');
Route::get('/partner', 'FrontendController@partner')->name('front_partner');
Route::get('/kontak', 'FrontendController@kontak')->name('front_kontak');
Route::get('/galeri', 'FrontendController@galeri')->name('front_galeri');
Route::get('/portofolio', 'FrontendController@portofolio')->name('front_portofolio');
Route::get('/portofolio/{id}', 'FrontendController@portofolioFilter')->name('front_portofolioFilter');
Route::get('/peraturan', 'FrontendController@peraturan')->name('front_peraturan');
Route::get('/terms', 'FrontendController@terms')->name('front_terms');
Route::get('/privacy', 'FrontendController@privacy')->name('front_privacy');
