@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">

    <div class="section-heading">
      <h2>Tim Ahli</h2>
      <div class="breadcrumb-wcb">
        <p>Tentang Kami / Tim Ahli</p>
      </div>
    </div>
  </div>

  <div class="section-content">
    <div class="container-wcb">

      <div class="filterGroup">
        <label for="inputFilter">Pencarian</label>
        <input placeholder="Cari berdasarkan Nama Paket, Pengguna Jasa, Lokasi, atau Tahun" id="inputFilterWCB" type="text" name="inputFilter" value="">
      </div>
      <div class="table-wcb-container">
        <table id="tableWCB">
          <thead>
            <tr class="row-head">
              <th class="col-md-1">No</th>
              <th class="col-md-3">Bidang</th>
              <th class="col-md-4">Nama</th>
              <th class="col-md-4">Pendidikan</th>
            </tr>
          </thead>
          <tbody>
            <?php $i=1 ?>
            @foreach($categories as $category)
            <?php $j='a' ?>
              @foreach($category->teams as $team)
              <tr>
                <td class="col-md-1"><?php echo $i.'.'.$j ?></td>
                <td class="col-md-3">{{$team->bidang}}</td>
                <td class="col-md-4">{{$team->name}}</td>
                <td class="col-md-4">{{$team->pendidikan}}</td>
              </tr>
              <?php $j++ ?>
              @endforeach
              <?php $i++ ?>
            @endforeach
          </tbody>

          <!-- <tr>
            <td class="col-md-1">1-f</td>
            <td class="col-md-3">Ahli Biologi Aquatic</td>
            <td class="col-md-4">Haikal Suhaidi, S.Si</td>
            <td class="col-md-4">S1 Biologi, UNPAD</td>
          </tr>
          <tr>
            <td class="col-md-1">1-g</td>
            <td class="col-md-3">Ahli Biologi</td>
            <td class="col-md-4">Drs. Mohammad Taufiq Afiff, M.Sc</td>
            <td class="col-md-4">SS2 Biologi, ITB</td>
          </tr>
          <tr>
            <td class="col-md-1">1-f</td>
            <td class="col-md-3">Ahli Biologi Aquatic</td>
            <td class="col-md-4">Haikal Suhaidi, S.Si</td>
            <td class="col-md-4">S1 Biologi, UNPAD</td>
          </tr>
          <tr>
            <td class="col-md-1">1-g</td>
            <td class="col-md-3">Ahli Biologi</td>
            <td class="col-md-4">Drs. Mohammad Taufiq Afiff, M.Sc</td>
            <td class="col-md-4">SS2 Biologi, ITB</td>
          </tr>
          <tr>
            <td class="col-md-1">1-f</td>
            <td class="col-md-3">Ahli Biologi Aquatic</td>
            <td class="col-md-4">Haikal Suhaidi, S.Si</td>
            <td class="col-md-4">S1 Biologi, UNPAD</td>
          </tr>
          <tr>
            <td class="col-md-1">1-g</td>
            <td class="col-md-3">Ahli Biologi</td>
            <td class="col-md-4">Drs. Mohammad Taufiq Afiff, M.Sc</td>
            <td class="col-md-4">SS2 Biologi, ITB</td>
          </tr>
          <tr>
            <td class="col-md-1">1-f</td>
            <td class="col-md-3">Ahli Biologi Aquatic</td>
            <td class="col-md-4">Haikal Suhaidi, S.Si</td>
            <td class="col-md-4">S1 Biologi, UNPAD</td>
          </tr>
          <tr>
            <td class="col-md-1">1-g</td>
            <td class="col-md-3">Ahli Biologi</td>
            <td class="col-md-4">Drs. Mohammad Taufiq Afiff, M.Sc</td>
            <td class="col-md-4">SS2 Biologi, ITB</td>
          </tr> -->
        </table>
      </div>
    </div>
  </div>

</section>
@endsection

@section('more-js')
<script>
$(document).ready(function(){
  $("#inputFilterWCB").on("keyup", function() {
    console.log('test');
    var value = $(this).val().toLowerCase();
    $("#tableWCB tr:not(.row-head)").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection
