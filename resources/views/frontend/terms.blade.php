@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">

    <div class="section-heading">
      <h2>Term of Use</h2>
    </div>
  </div>

  <div class="section-content">
    <div class="container-wcb">
      <h3>Syarat Penggunaan</h3>
      <p>Selamat datang di website kami. Jika Anda terus menelusuri dan menggunakan situs web ini, Anda setuju untuk mematuhi persyaratan dan ketentuan penggunaan. Jika Anda tidak setuju dengan syarat dan ketentuan kami, mohon jangan gunakan situs web kami. Istilah ‘Widya Cipta Buana' atau 'kami' atau mengacu pada pemilik PT. Widya Cipta Buana. Istilah 'Anda' mengacu pada pengguna atau pemirsa situs web kami. Penggunaan situs web ini tunduk pada ketentuan penggunaan berikut:</p>
      <ul>
        <li>Isi halaman-halaman situs web ini adalah berisi informasi untuk Anda. Informasi dapat berubah tanpa pemberitahuan.</li>
        <li>Situs web ini menggunakan cookie untuk memantau preferensi browsing.</li>
        <li>Kami maupun pihak ketiga tidak memberikan jaminan, kinerja, kelengkapan, atau kesesuaian informasi dan materi di situs web untuk tujuan tertentu. Anda mengakui bahwa informasi dan materi tersebut mungkin berisi ketidakakuratan atau kesalahan dan ekstensi yang diizinkan oleh hukum.</li>
        <li>Situs web ini berisi materi yang dimiliki oleh atau dilisensikan kepada kami. Materi ini termasuk, tetapi tidak terbatas pada desain, tata letak, tampilan, penampilan dan grafik. Dilarang memperbanyak selain sesuai dengan pemberitahuan hak cipta, yang membentuk syarat dan ketentuan.</li>
        <li>Dari waktu ke waktu situs web ini juga dapat menyertakan tautan ke situs web lain. Tautan ini disediakan demi kenyamanan Anda untuk memberikan informasi lebih lanjut. Mereka tidak menandakan bahwa kami mendukung situs web atau produk dan layanan dari pemiliknya. Kami tidak bertanggung jawab atas isi dari situs web tertaut.</li>
      </ul>
    </div>
  </div>

</section>
@endsection
