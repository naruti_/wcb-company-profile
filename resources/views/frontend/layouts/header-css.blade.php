<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400" rel="stylesheet">
<link href="https://fonts.googleapis.com/icon?family=Material+Icons"
  rel="stylesheet">
<link rel="shortcut icon" sizes="32x32" href="{{asset('frontend/assets/img/title-logo.ico')}}" />

<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
<!-- <link rel="stylesheet" type="text/css" href="vendor/material-kit/assets/css/material-kit.min.css"> -->
<link rel="stylesheet" type="text/css" href="{{ asset('frontend/assets/css/main-style.css') }}">
<!-- Magnific Pop Up -->

<link rel="stylesheet" type="text/css" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">


  <!-- <link rel="stylesheet" type="text/css" href="assets/css/normalize.css"> -->
