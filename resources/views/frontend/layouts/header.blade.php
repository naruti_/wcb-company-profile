<header id="header" >
  <div class="header-main">
    <div class="box-left">
      <div class="logo-company-container">
        <span class="middle-helper"></span>
        <a href="{{route('front_beranda')}}"><img src="{{asset('frontend/assets/img/logo.png')}}" alt=""></a>
      </div>
      <button style="" type="button" class="wcb-navbar-toggler">
        <span><i class="material-icons">menu</i></span>
      </button>
    </div>
    <div class="box-right">
      <div class="items-container-right">
        <div class="wcb-navbar-collapse">
          <!-- <div class="form-control-header">
            <input class="bordered-circle" placeholder="Pencarian" type="text" name="" value="">
          </div> -->
          <a href="{{route('front_beranda')}}" class="header-items-link"><p>BERANDA</p></a>
          <div class="header-items-link dropdown-wcb">
            <p>TENTANG KAMI <span class="icons-dropdown"></span></p>
            <div class="dropdown-content">
              <a href="{{route('front_profil')}}">Profil Perusahaan</a>
              <a href="{{route('front_struktur')}}">Struktur Organisasi</a>
              <a href="{{route('front_sertifikasi')}}">Sertifikasi & Kompetensi</a>
              <a href="{{route('front_legalitas')}}">Legalitas</a>
              <a href="{{route('front_timAhli')}}">Tim Ahli</a>
              <a href="{{route('front_lingkupPekerjaan')}}">Lingkup Pekerjaan</a>
              <a href="{{route('front_partner')}}">Partner</a>
            </div>
          </div>
          <!-- <div class="header-items-link dropdown-wcb">
            <p>BISNIS <span class="icons-dropdown"></span></p>
            <div class="dropdown-content">
              <a href="lingkup.html">Lingkup Pekerjaan</a>
              <a href="partner.html">Partner</a>
            </div>
          </div> -->
          <div class="header-items-link dropdown-wcb">
            <p>PENGALAMAN <span class="icons-dropdown"></span></p>
            <div class="dropdown-content">
              <a href="{{route('front_galeri')}}">Galeri</a>
              <a href="{{route('front_portofolio')}}">Proyek</a>
              <!-- <a href="portofolio.html">Portofolio</a> -->
              <a href="{{route('front_peraturan')}}">Peraturan Lingkungan Hidup</a>
            </div>
          </div>
          <a href="{{route('front_kontak')}}" class="header-items-link items-kontak"><p>KONTAK </p></a>
        </div>
      </div>
    </div>
  </div>
</header>
