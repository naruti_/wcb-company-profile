<footer id="beranda-footer">
  <div class="container-fluid">
    <h2 class="footer-main_heading">Kontak Kami</h2>
    <img src="{{asset('frontend/assets/img/logo.png')}}" alt="">
    <div class="section-footer">
      <h3>Alamat Kantor Pusat :</h3>
      <p>Komp. Perkantoran Metro Jalan Venus Barat Kav. 15 - Bandung 40286
      (022) 7568445 & (022) 7509172</p>
    </div>
    <div class="footer-separator"></div>
    <div class="section-footer kantor-perwakilan">
      <h3>Kantor Perwakilan:</h3>
      <ul>
        <li><a href="#">Medan</a></li>
        <li><a href="#">Pekanbaru</a></li>
        <li><a href="#">Surabaya</a></li>
        <li><a href="#">Yogyakarta</a></li>
        <li><a href="#">Balikpapan</a></li>
        <li><a href="#">Pontianak</a></li>
      </ul>
    </div>
    <div class="footer-copyright">
      <p>Copyrights 2018 PT. Widya Cipta Buana</p>
      <p>Developed by <a class="link-developer" href="">Mumtaaz Studio</a></p>
    </div>
  </div>
</footer>
