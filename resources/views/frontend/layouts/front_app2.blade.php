<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@yield('title')</title>

    @include('frontend.layouts.header-css')
    @yield('more-css')
    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body>
  @include('frontend.layouts.header')
  @yield('content')
  @include('frontend.layouts.footer')
</body>
@include('frontend.layouts.footer-js')
@yield('more-js')
</html>
