
    <footer id="footer">
      <div class="container-fluid">
        <h2 class="footer-main_heading">Kontak Kami</h2>
        <!-- <a href="">Struktur Organisasi</a> -->
        <div class="row separator">
          <div class="col-md-4 col-8 no-padding">
            <h5 class="footer-sub_heading">Alamat Kantor Pusat</h5>
            <p class="footer-text-info">Komp. Perkantoran Metro <br>
            Jalan Venus Barat Kav. 15 - Bandung 40286 <br>
            (022) 7568445 & (022) 7509172</p>
          </div>
          <div class="col-md-3 col-4 no-padding">
            <h5 class="footer-sub_heading">Kantor Perwakilan</h5>
            <div class="row">
              <div class="col-md-5 no-padding">
                <ul class="no-margin no-padding">
                  <li>Medan</li>
                  <li>Pekanbaru</li>
                <!--   <li>Balikpapan</li> -->
                </ul>
              </div>
              <!-- <div class="col-md-5 col-2 no-padding">
                <ul class="no-margin no-padding">
                  <li>Surabaya</li>
                  <li>Yogyakarta</li>
                  <li>Pontianak</li>
                </ul>
              </div> -->
            </div>
          </div>
          <div class="col-md-3 col-8 no-padding">
            <h5 class="footer-sub_heading">Sosial Media</h5>
            <div class="row">
              <div class="col-md-12 no-padding">
                <ul class="no-margin no-padding">
                  <li><a href="">Facebook</a></li>
                  <li><a href="">Twitter</a></li>
                  <li><a href="">LinkedIn</a></li>
                </ul>
              </div>
            </div>
          </div>
          <div class="col-md-1 col-4 no-padding">
            <h5 class="footer-sub_heading">Legal</h5>
            <div class="row">
              <div class="col-md-12 no-padding">
                <ul class="no-margin no-padding">
                  <li><a href="{{route('front_terms')}}">Term</a></li>
                  <li><a href="{{route('front_privacy')}}">Privacy</a></li>
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="footer-copyright">
          <p>Copyrights 2018 PT. Widya Cipta Buana</p>
          <p>Developed by <a class="link-developer" href="">Mumtaaz Studio</a></p>
        </div>
      </div>
    </footer>
