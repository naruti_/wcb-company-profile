@extends('frontend.layouts.front_app')

@section('content')
<section id="section-top" class="section-full_page_bg">
  <div class="the-background beranda-the-background"></div>
  <div class="overlay">
    <div class="overlay-text-center">
      <h1 class="overlay-text-content overlay-text-judul"> Selamat Datang di Widya Cipta Buana</h1>
    </div>
  </div>
  </section>

  <section class="section-no_full_page_no_bg">
  <div class="container">
    <div class="section-heading">
      <h2>Profil Perusahaan</h2>
    </div>
    <div class="row" >
      <div class=" col-8 mr-auto ml-auto col-md-7">
        <div class="beranda-background-container"></div>
      </div>
      <div class="col-md-5">
        <div class="beranda-content-container">
          <div class="beranda-content-text">
            <p> Kegiatan pembangunan yang selalu berkembang dan kompleks, menuntut upaya dan partisipasi semua pihak untuk menjawab dan melaksanakan permasalahan yang senantiasa muncul sejalan dengan gerak pembangunan itu sendiri. Upaya dan partisipasi semua pihak diharapkan akan menimbulkan kesadaran dan rasa memiliki terhadap semua obyek pembangunan. Hal ini menuntut tanggung jawab moral dan keilmuan dari masing-masing pelaksanan pembangunan demi terciptanya tujuan yang diharapkan.</p>
          </div>
          <a href="profil.html" class="btn btn-mumtaaz btn-size-mumtaaz beranda-button-overlap btn-block btn-circled text-center">Selengkapnya</a>
        </div>
      </div>
    </div>

  </div>
</section>

<section class="section-no_full_page_no_bg bg-striped" style="" >
  <div class="container">
    <div class="beranda-content-container">
        <div class="section-heading text-center">
          <h2 class="no-margin">Lingkup Pekerjaan</h2>
        </div>
        <div style="margin-bottom: 20px" class="text-center beranda-content-text">
          <p>PT. Widya Cipta Buana ditangani oleh para tenaga ahli multi-disiplin ilmu dengan berpegang teguh kepada profesionalisme kerja dan itikad yang baik dalam menjalankan tugas, berikut lingkup pekerjaan profesionalitas kami.</p>
        </div>

        <div class="container-card-lingkup">
          <!-- <div class="bulet-bulet">

          </div> -->

          <a href="lingkup.html#analisis" class="beranda-ukuran-card">
            <div class="card-wcb">
              <div class="card-icon">
                <img src="{{asset('frontend/assets/img/aswan/icons/analisis.png')}}" alt="">
              </div>
              <div class="card-desc-right">
                <p>Analisis Mengenai Dampak Lingkungan, Audit Lingkungan, Pemantauan Lingkungan dan Sanitari</p>
              </div>
            </div>
          </a>

          <a href="lingkup.html#studi-sosial" class="beranda-ukuran-card">
            <div class="card-wcb">
              <div class="card-desc-left">
                <p>Studi Sosial, Ekonomi dan Budaya</p>
              </div>
              <div class="card-icon">
                <img src="{{asset('frontend/assets/img/aswan/icons/studi.png')}}" alt="">
              </div>
            </div>
          </a>

          <a href="lingkup.html#gioteknik" class="beranda-ukuran-card">
            <div class="card-wcb">
              <div class="card-icon">
                <img src="{{asset('frontend/assets/img/aswan/icons/investigasi.png')}}" alt="">
              </div>
              <div class="card-desc-right">
                <p>Gioteknik dan Investigasi</p>
              </div>
            </div>
          </a>

          <a href="lingkup.html#teknik" class="beranda-ukuran-card">
            <div class="card-wcb">
              <div class="card-desc-left">
                <p>Teknik Sipil dan Perencanaan</p>
              </div>
              <div class="card-icon">
                <img src="{{asset('frontend/assets/img/aswan/icons/engineer.png')}}" alt="">
              </div>
            </div>
          </a>

          <a href="lingkup.html#analisa" class="beranda-ukuran-card">
            <div class="card-wcb">
              <div class="card-icon">
                <img src="{{asset('frontend/assets/img/aswan/icons/molecule.png')}}" alt="">
              </div>
              <div class="card-desc-right">
                <p>Analisa Laboratorium</p>
              </div>
            </div>
          </a>

          <a href="lingkup.html#studi-kelayakan" class="beranda-ukuran-card">
            <div class="card-wcb">
              <div class="card-desc-left">
                <p>Studi kelayakan</p>
              </div>
              <div class="card-icon">
                <img src="{{asset('frontend/assets/img/aswan/icons/studi-kelayakan.png')}}" alt="">
              </div>
            </div>
        </a>
      </div>
    </div>
    <!-- End of beranda-content-container -->
  </div>
  <!-- End of Container -->
</section>
@endsection
