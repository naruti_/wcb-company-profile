@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">

    <div class="section-heading">
      <h2>Proyek</h2>
      <div class="breadcrumb-wcb">
        <p>Proyek / Pengalaman</p>
      </div>
    </div>
  </div>

  <div class="section-content">
    <div class="container-wcb">
      <div class="kategori-container">
        <h3 class="kategori-heading">Kategori :</h3>
        <ul>
          <li><a href="{{route('front_portofolio')}}" class="kategori-item">Semua</a></li>
          @foreach($categories as $category)
          <li><a href="{{route('front_portofolioFilter',$category->id)}}" class="kategori-item">{{$category->name}}</a></li>

          @endforeach
        </ul>
      </div>

      <div class="filterGroup">
        <label for="inputFilter">Pencarian</label>
        <input placeholder="Cari berdasarkan Nama Paket, Pengguna Jasa, Lokasi, atau Tahun" id="inputFilterWCB" type="text" name="inputFilter" value="">
      </div>
      <div class="table-wcb-container2">
        <table id="tableWCB">
          <thead>
            <tr class="row-head">
              <th class="col-md-1">No</th>
              <th class="col-md-3">Nama Paket Pekerjaan</th>
              <th class="col-md-3">Pengguna Jasa</th>
              <th class="col-md-3">Lokasi Kegiatan</th>
              <th class="col-md-2">Tahun</th>
            </tr>
          </thead>
          <tbody>

          <?php $i = 1 ?>
          @foreach($categories as $category)
            @foreach($category->portfolios as $portfolio)
            <tr class="">
              <!-- <div class="row"> -->
                <td class="col-md-1"><? echo $i?></td>
                <td class="col-md-3">{{$portfolio->name}}</td>
                <td class="col-md-3">{{$portfolio->client}}</td>
                <td class="col-md-3">{{$portfolio->location}}</td>
                <td class="col-md-2">{{$portfolio->year}}</td>
                <? $i++; ?>
              <!-- </div> -->
            </tr>
            @endforeach
          @endforeach

          </tbody>
        </table>

      </div>
    </div>
  </div>
</section>
@endsection

@section('more-js')
<script>
$(document).ready(function(){
  $("#inputFilterWCB").on("keyup", function() {
    console.log('test');
    var value = $(this).val().toLowerCase();
    $("#tableWCB tr:not(.row-head)").filter(function() {
      $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
    });
  });
});
</script>
@endsection
