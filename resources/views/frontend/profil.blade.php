@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top" class="section-no_full_page_bg">
  <div class="the-background profil-the-background"></div>
  <div class="overlay">
    <div class="overlay-text">
      <div class="overlay-text-container">
        <h1 class="overlay-text-content overlay-text-judul">Profil Perusahaan</h1>
        <div class="breadcrumb-wcb">
          <p>Tentang Kami / Profil</p>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="section-no_full_page_no_bg">
  <div class="container-wcb">
    <div class="content-box">
      <p>Kegiatan pembangunan yang selalu berkembang dan kompleks, menuntut upaya dan partisipasi semua pihak untuk menjawab dan melaksanakan permasalahan yang senantiasa muncul sejalan dengan gerak pembangunan itu sendiri. Upaya dan partisipasi semua pihak diharapkan akan menimbulkan kesadaran dan rasa memiliki terhadap semua obyek pembangunan. Hal ini menuntut tanggung jawab moral dan keilmuan dari masing-masing pelaksanan pembangunan demi terciptanya tujuan yang diharapkan.</p>
      <p>Bertitik tolak dari kesadaran untuk bertasipasi dalam gerak pembangunan, maka pada tahun 1992, berkedudukan di Bandung kami bersepakat untuk mendirikan sebuah perusahaan Perseroan Terbatas yang bergerak dalam bidang jasa konsultan.</p>
    </div>
  </div>
</section>
@endsection
