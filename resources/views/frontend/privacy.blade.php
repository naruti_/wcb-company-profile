@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">

    <div class="section-heading">
      <h2>Privacy</h2>
    </div>
  </div>

  <div class="section-content">
    <div class="container-wcb">
      <h3>Pribadi</h3>
      <p>Kebijakan privasi ini menetapkan bagaimana Widya Cipta Buana menggunakan dan melindungi informasi apa pun yang Anda berikan kepada kami saat Anda menggunakan situs web ini. kami berkomitmen untuk memastikan privasi Anda terlindungi. Jika kami meminta Anda untuk memberikan informasi tertentu yang dapat diidentifikasi ketika menggunakan situs web ini, Anda dapat memastikan bahwa informasi hanya akan digunakan sesuai dengan pernyataan privasi ini. Kami dapat mengubah kebijakan ini dari waktu ke waktu dengan memperbarui halaman ini. Anda harus memeriksa halaman ini dari waktu ke waktu untuk memastikan bahwa Anda senang dengan perubahan apa pun. Kebijakan ini berlaku mulai 21 Mei 2018.</p>

      <h4><b>Apa yang kami kumpulkan</b></h4>
      <p>Kami dapat mengumpulkan data berikut:</p>
      <ul>
        <li>Nama</li>
        <li>Informasi kontak termasuk alamat email</li>
        <li>
          Informasi demografis seperti kode pos dan negara
        </li>
        <li>
          Informasi perusahaan Anda
        </li>
        <li>
          Informasi lain yang relevan dengan survei dan / atau penawaran pelanggan
        </li>
      </ul>

      <h4><b>Apa yang kami lakukan dengan informasi yang kami dapatkan</b></h4>
      <p>Kami memerlukan informasi ini untuk memahami kebutuhan Anda dan memberi Anda layanan yang lebih baik, dan khususnya untuk alasan berikut:</p>
      <ul>
        <li>Penyimpanan catatan internal.</li>
        <li>Kami dapat menggunakan informasi untuk memberikan atau meningkatkan layanan kami.</li>
      </ul>

      <h4><b>Keamanan</b></h4>
      <p>Kami berkomitmen untuk memastikan bahwa informasi Anda aman. Untuk mencegah akses atau pengungkapan yang tidak sah, kami telah menempatkan prosedur fisik, elektronik, dan manajerial yang sesuai untuk menjaga dan mengamankan informasi yang kami kumpulkan secara online.</p>
      <h4><b>Melacak cookie</b></h4>
      <p>Cookie, atau pelacakan cookie adalah file kecil yang disimpan di PC Anda yang berisi informasi tentang sesi penjelajahan web Anda di situs kami. File-file cookie ini memungkinkan aplikasi web kami untuk menanggapi Anda sebagai individu. Aplikasi web dapat menyesuaikan operasinya dengan kebutuhan Anda, suka dan tidak suka dengan mengumpulkan dan </p>



    </div>
  </div>

</section>
@endsection
