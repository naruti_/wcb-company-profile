@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">

    <div class="section-heading">
      <h2>Peraturan Lingkungan Hidup</h2>
      <div class="breadcrumb-wcb">
        <p>Pengalaman / Peraturan Lingkungan Hidup</p>
      </div>
    </div>

    <div class="section-content">
      <div class="row">
        <div class="col-md-12">
          <!-- Accordion -->
          @foreach($categories as $category)
          <div class="accordion-wcb-container">
            <div class="accordion-box">
              <button class="accordion-button btn-mumtaaz">{{$category->name}}</button>
              <div class="accordion-panel">
                <ol class="accordion-ol">
                  <?php $i = 1; ?>
                  @foreach($category->regulations as $reg)
                  <li><?$i?><a target="_blank" href="{{ asset('storage/files/regulations/'.$reg->filename) }}">{{$reg->name}}</a></li>
                  <?$i++;?>
                  @endforeach
                </ol>
              </div>
            </div>

          </div>
          @endforeach

        </div>
      </div>
    </div>
  </div>
</section>
@endsection
