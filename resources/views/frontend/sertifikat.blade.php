@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">
    <div class="section-heading">
      <h2>Sertifikasi & Kompetensi</h2>
      <div class="breadcrumb-wcb">
        <p>Tentang Kami / Sertifikasi & Kompetensi</p>
      </div>
    </div>
    <div class="section-content">
      <ul class="certification-list">
        @foreach($sertifikats as $sertifikat)
        <li>
          <div class="box-person">
            <!-- <div class="person-photo">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div> -->
            <div class="person-namekta">
              <div class="box-name">
                <p class="box_attr-head">Nama Lengkap</p>
                <p>{{$sertifikat->name}}</p>
              </div>
              <div class="box-kta">
                <p>KTA/Registrasi</p>
                <p>{{$sertifikat->kta}}</p>
              </div>
            </div>
            <div class="person-certification">
              <p class="box_attr-head">Sertifikasi Profesi</p>
              <p>Nomor Sertifikat Kompetensi: {{$sertifikat->no_certificate}}</p>
              <p>Kualifikasi: {{$sertifikat->qualification}}</p>
            </div>
          </div>
        </li>
        @endforeach
        <!-- <li>
          <div class="box-person">
            <div class="person-photo">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div>
            <div class="person-namekta">
              <div class="box-name">
                <p class="box_attr-head">Nama Lengkap</p>
                <p>Iwan Setiawan</p>
              </div>
              <div class="box-kta">
                <p>KTA/Registrasi</p>
                <p>K.001.07.09.10.000005</p>
              </div>
            </div>
            <div class="person-certification">
              <p class="box_attr-head">Sertifikasi Profesi</p>
              <p>Nomor Sertifikat Kompetensi: 001465/SKPA-P2/LSK-INTAKINDO/IX/2015</p>
              <p>Kualifikasi: Ketua Tim Penyusun Dokumen AMDAL</p>
            </div>
          </div>
        </li> -->

      </ul>
    </div>


  </div>
</section>
@endsection
