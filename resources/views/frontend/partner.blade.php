@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">
    <div class="section-heading">
      <h2>Tentang Kami</h2>
      <div class="breadcrumb-wcb">
        <p>Tentang Kami / Partner</p>
      </div>
    </div>

    <div class="section-content">
      <div class="row">
          <div class="col-md-3">
            <!-- Card Vertical -->
            <div class="card-partner">
              <div class="card-img">
                <img src="{{asset('frontend/assets/img/bina-lab.png')}}" alt="">
              </div>
              <div class="card-content text-center">
                <h1 class="partner-name">Bina Lab</h1>
                <p class="partner-desc">Laboratorium Pengujian Kualitas Lingkungan</p>
                <hr>
                <p class="text-grey">LP 412-IDN | 0030/LPJ/LABLING-1/LRK/KLHK</p>
                <p>Jl. Venus Barat Kav. 15 Metro - Margahayu Raya Bandung 40286</p>
                <p class="text-grey">P (+62) 22 7561503 | F (+62) 227561324 | M (+62) 81224220062, (+62) 85797180955</p>
              </div>
            </div>
          </div>
          <!-- End of col-md-3 -->
          <div class="col-md-3">
            <!-- Card Vertical -->
            <div class="card-partner">
              <div class="card-img">
                <img src="{{asset('frontend/assets/img/bina-lab.png')}}" alt="">
              </div>
              <div class="card-content text-center">
                <h1 class="partner-name">Bina Lab</h1>
                <p class="partner-desc">Laboratorium Pengujian Kualitas Lingkungan</p>
                <hr>
                <p class="text-grey">LP 412-IDN | 0030/LPJ/LABLING-1/LRK/KLHK</p>
                <p>Jl. Venus Barat Kav. 15 Metro - Margahayu Raya Bandung 40286</p>
                <p class="text-grey">P (+62) 22 7561503 | F (+62) 227561324 | M (+62) 81224220062, (+62) 85797180955</p>
              </div>
            </div>
          </div>
          <!-- End of col-md-3 -->
          <div class="col-md-3">
            <!-- Card Vertical -->
            <div class="card-partner">
              <div class="card-img">
                <img src="{{asset('frontend/assets/img/bina-lab.png')}}" alt="">
              </div>
              <div class="card-content text-center">
                <h1 class="partner-name">Bina Lab</h1>
                <p class="partner-desc">Laboratorium Pengujian Kualitas Lingkungan</p>
                <hr>
                <p class="text-grey">LP 412-IDN | 0030/LPJ/LABLING-1/LRK/KLHK</p>
                <p>Jl. Venus Barat Kav. 15 Metro - Margahayu Raya Bandung 40286</p>
                <p class="text-grey">P (+62) 22 7561503 | F (+62) 227561324 | M (+62) 81224220062, (+62) 85797180955</p>
              </div>
            </div>
          </div>
          <!-- End of col-md-3 -->
          <div class="col-md-3">
            <!-- Card Vertical -->
            <div class="card-partner">
              <div class="card-img">
                <img src="{{asset('frontend/assets/img/bina-lab.png')}}" alt="">
              </div>
              <div class="card-content text-center">
                <h1 class="partner-name">Bina Lab</h1>
                <p class="partner-desc">Laboratorium Pengujian Kualitas Lingkungan</p>
                <hr>
                <p class="text-grey">LP 412-IDN | 0030/LPJ/LABLING-1/LRK/KLHK</p>
                <p>Jl. Venus Barat Kav. 15 Metro - Margahayu Raya Bandung 40286</p>
                <p class="text-grey">P (+62) 22 7561503 | F (+62) 227561324 | M (+62) 81224220062, (+62) 85797180955</p>
              </div>
            </div>
          </div>
          <!-- End of col-md-3 -->
        </div>
      </div>

    </div>
</section>
@endsection
