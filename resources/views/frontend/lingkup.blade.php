@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">
    <div class="section-heading">
      <h2>Lingkup Pekerjaan</h2>
      <div class="breadcrumb-wcb">
        <p>Tentang Kami / Lingkup Pekerjaan</p>
      </div>
    </div>
    <div class="section-content">
      <div id="analisis" class="lingkup-container">
        <div class="lingkup-box-img">
          <img src="{{asset('frontend/assets/img/aswan/icons/analisis.png')}}">
        </div>
        <div class="lingkup-box-description">
        <h3>Analisis Mengenai Dampak Lingkungan, Audit Lingkungan, Pemantauan Lingkungan dan Sanitari</h3>
        <p class="no-margin">Studi kelayakan lingkungan (AMDAL) untuk proyek-proyek pusat tenaga listrik, kawasan industri dan industri; Kehutanan (HPH); Pertanian; Pariwisata dan perhotelan; Perkantoran; Rumah Sakit; Transportasi (rencana pembangunan jalan); Pekerjaan Umum; Pertambangan dan lain lain.</p>
        <br>
        <p class="no-margin">Pemantauan lingkungan untuk proyek-proyek yang sedang dalam tahap konstruksi maupun yang sudah operasi berdasarkan dokumen lingkungan dengan izin lingkungan yang telah disetujui</p>
        <br>
        <p class="no-margin">Sanitari, Meliputi kegiatan perencanaan sistem jaringan airbersih, air kotor, perencanaan pembuangan akhir sampah dan limbah padat industri B-3 dan non B-3, IPLT ( Instalasi Pembuangan Limbah Tinja ), Water treatment Plant (WWTP) khususnya untuk industri dan perencanaan instalasi jaringan transmisi air minum </p>
        <br>
        </div>
      </div>
      <div id="studi-sosial" class="lingkup-container">
        <div class="lingkup-box-img">
          <img src="{{asset('frontend/assets/img/aswan/icons/studi.png')}}" alt="">
        </div>
        <div class="lingkup-box-description">
        <h3>Studi Sosial, Ekonomi dan Budaya.</h3>
        <p>Studi Community Development</p>
        <p>Studi Kajian Sosial terhadap berbagai dampak suatu proyek pembangunan</p>
        <p>Studi Resettlement Plant</p>
        </div>
      </div>
      <div id="gioteknik" class="lingkup-container">
        <div class="lingkup-box-img">
          <img src="{{asset('frontend/assets/img/aswan/icons/investigasi.png')}}" alt="">
        </div>
        <div class="lingkup-box-description">
          <h3>Geoteknik dan Investigasi</h3>
          <p>
            Penyelidikan Geologi Teknik dan Mekanika Tanah
          </p>
          <p>
            Studi dan Desain Perencanaan Lokasi
          </p>
          <p>
            Penyelidikan Geohidrologi
          </p>
          <p>
            Penelitian dan Survei Geologi/Geofisika
          </p>
          <p>
            Eksplorasi Bahan Tambang dan Bahan Bangunan
          </p>
          <p>
            Penelitian dan Survei Hidrologi
          </p>
        </div>
      </div>
      <div id="teknik" class="lingkup-container">
        <div class="lingkup-box-img">
          <img src="{{asset('frontend/assets/img/aswan/icons/engineer.png')}}" alt="">
        </div>
        <div class="lingkup-box-description">
          <h3>Teknik Sipil dan Perencanaan</h3>
          <p>Perencanaan Waste Water Treatment Industri (Instalasi Pengolahan Air Limbah)</p>
          <p>Perencanaan Water Treatment (Instalasi Pengolahan Air Bersih)</p>
          <p>Perencanaan Lokasi Pariwisata</p>
          <p>Perencanaan Lokasi Pemukiman</p>
        </div>
      </div>
      <div id="studi-kelayakan" class="lingkup-container">
        <div class="lingkup-box-img">
          <img src="{{asset('frontend/assets/img/aswan/icons/studi-kelayakan.png')}}" alt="">
        </div>
        <div class="lingkup-box-description">
        <h3>Studi Kelayakan</h3>
        <p>
          Kelayakan Teknik
        </p>
        <p>
          Kelayakan Ekonomis
        </p>
        <p>
          Kelayakan Legalitas (Perijinan dan Kesesuaian Tata Ruang)
        </p>
        </div>
      </div>
      <div id="analisa" class="lingkup-container lingkup-last">
        <div class="lingkup-box-img">
          <img src="{{asset('frontend/assets/img/aswan/icons/molecule.png')}}" alt="">
        </div>
        <div class="lingkup-box-description">
          <h3>Analisa Laboratorium</h3>
          <p>
            Analisa kualitas udara, debu, partikel logam dan kebisingan (ambien dan emisi)
          </p>
          <p>
            Analisa kualitas air tanah dan permukaan (parameter fisik, kimia dan biologi)
          </p>
          <p>
            Analisa limbah cair dan padat (sludge)
          </p>
          <p>
            Analisa laboratorium mekanika tanah dan batuan
          </p>
          <p>
            Analisa kimia tanah dan batuan
          </p>
        </div>
      </div>
    </div>
  </div>
</section>
@endsection
