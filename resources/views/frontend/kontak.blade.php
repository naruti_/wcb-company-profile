@extends('frontend.layouts.front_app2')

@section('more-css')
<style>
    #map {
       height: 480px;
       width: 96%;
       background-color: grey;
      }
  </style>
@endsection

@section('content')
<section id="section-top" class="section-no_full_page_bg section-kontak">
  <div class="the-background kontak-the-background"></div>
  <div class="overlay">
    <div class="overlay-text">
      <div class="overlay-text-container">
        <h1 class="overlay-text-content overlay-text-judul">Kontak</h1>
      </div>
    </div>
  </div>
</section>
<section class="section-no_full_page_no_bg">
    <div class="container-wcb">
      <div class="row" style="margin-top: 20px">
        <div class="col-md-7">
          <div id="map"></div>
        </div>
        <div class="col-md-5">
          <div class="beranda-content-container">
            <form action="">
              <div class="beranda-content-text">
                <p>Silahkan gunakan formulir berikut ini untuk mengirimkan pesan kepada kami</p>
                <div class="form-group-mumtaaz">
                  <input placeholder="Nama lengkap" class="form-mumtaaz-control" type="text" name="" value="">
                </div>
                <div class="form-group-mumtaaz">
                  <input placeholder="Nama perusahaan" class="form-mumtaaz-control" type="text" name="" value="">
                </div>
                <div class="form-group-mumtaaz">
                  <input placeholder="Telepon" class="form-mumtaaz-control" type="text" name="" value="">
                </div>
                <div class="form-group-mumtaaz">
                  <input placeholder="Email Perusahaan" class="form-mumtaaz-control" type="text" name="" value="">
                </div>
                <div class="form-group-mumtaaz">
                  <input placeholder="Alamat" class="form-mumtaaz-control" type="text" name="" value="">
                </div>
                <div class="form-group-mumtaaz">
                  <textarea placeholder="Pesan" class="form-mumtaaz-control" type="text" name="" value=""></textarea>
                </div>
              </div>
              <button type="submit" class="btn btn-mumtaaz btn-size-mumtaaz kontak-button-overlap btn-block btn-circled">Kirim Pesan</button>
          </form>
        </div>
      </div>
    </div>

  </div>
</section>
@endsection

@section('more-js')
<script>
  function initMap() {
    var uluru = {lat: -6.939725, lng: 107.665460};
    var map = new google.maps.Map(document.getElementById('map'), {
      zoom: 16,
      center: uluru
    });
    var marker = new google.maps.Marker({
      position: uluru,
      map: map,
      title:"Hello World!"
    });

    var infowindow = new google.maps.InfoWindow();
    var service = new google.maps.places.PlacesService(map);

      service.getDetails({
        placeId: 'ChIJN1t_tDeuEmsRUsoyG83frY4'
      }, function(place, status) {
        if (status === google.maps.places.PlacesServiceStatus.OK) {
          var marker = new google.maps.Marker({
            map: map,
            position: place.geometry.location
          });
          google.maps.event.addListener(marker, 'click', function() {
            infowindow.setContent('<div><strong>' + place.name + '</strong><br>' +
              'Place ID: ' + place.place_id + '<br>' +
              place.formatted_address + '</div>');
            infowindow.open(map, this);
          });
        }
      });
  }
</script>
<script async defer
src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDVHDXmItBZtQyOH4ij2pnsqRXBJTtgbL8&libraries=places&callback=initMap">
</script>
@endsection
