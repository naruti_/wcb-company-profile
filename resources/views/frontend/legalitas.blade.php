@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
      <div class="container-wcb">

        <div class="section-heading">
          <h2>Legalitas</h2>
          <div class="breadcrumb-wcb">
            <p>Tentang Kami / Legalitas</p>
          </div>
        </div>

        <div class="section-content">
          <div class="row">
            <div class="col-md-12">
              <!-- Accordion -->
              <div class="accordion-wcb-container">
                <?php
                $i = 1;
                 ?>
                @foreach($categories as $category)
                <div class="accordion-box">
                  <button class="accordion-button btn-mumtaaz"><?echo $i?>. {{$category->name}}</button>
                  <div class="accordion-panel">
                    <table>
                      <tr>
                        <th colspan="2" style="width:50%" >Legalitas</th>
                        <th style="width:30%" >Nomor</th>
                        <th style="width:20%" >Tanggal Terbit</th>
                      </tr>
                      <?php $j= 'a'?>
                      @foreach($category->legals as $legal)
                      <tr>
                        <td style="width:3%"><?echo $j ?></td>
                        <td style="width:47%">{{$legal->name}}</td>
                        <td style="width:30%">{{$legal->nomor}}</td>
                        <td style="width:20%">{{date('d F Y', strtotime($legal->tgl_terbit))}}</td>
                      </tr>
                      <?$j++;?>
                      @endforeach
                    </table>
                  </div>
                </div>
                <?$i++;?>
                @endforeach
                <!-- <div class="accordion-box">
                  <button class="accordion-button btn-mumtaaz">1. Kementrian Lingkungan Hidup</button>
                  <div class="accordion-panel">
                    <table>
                      <tr>
                        <th colspan="2" style="width:50%" >Legalitas</th>
                        <th style="width:30%" >Nomor</th>
                        <th style="width:20%" >Tanggal Terbit</th>
                      </tr>
                      <tr>
                        <td style="width:3%">a</td>
                        <td style="width:47%">Sertifikasi Tanda Registrasi Kompetensi</td>
                        <td style="width:30%">0002/LPJ/AMDAL-1/LRK/KLH</td>
                        <td style="width:20%">8 Maret 2016</td>
                      </tr>
                    </table>
                  </div>
                </div>
                <div class="accordion-box">
                  <button class="accordion-button btn-mumtaaz">2. Ikatan Nasional Konsultan Indonesia (INKINDO)</button>
                  <div class="accordion-panel">
                    <table>
                      <tr>
                        <th colspan="2" style="width:50%" >Legalitas</th>
                        <th style="width:30%" >Nomor</th>
                        <th style="width:20%" >Tanggal Terbit</th>
                      </tr>
                      <tr>
                        <td style="width:3%">a</td>
                        <td style="width:47%">Sertifikasi Tanda Registrasi Kompetensi</td>
                        <td style="width:30%">0002/LPJ/AMDAL-1/LRK/KLH</td>
                        <td style="width:20%">8 Maret 2016</td>
                      </tr>
                    </table>
                  </div>
                </div> -->
                <!-- <div class="accordion-box">
                  <button class="accordion-button btn-mumtaaz">3. Direktorat Jenderal Pajak</button>
                  <div class="accordion-panel">
                    <table>
                      <tr>
                        <th colspan="2" style="width:50%" >Legalitas</th>
                        <th style="width:30%" >Nomor</th>
                        <th style="width:20%" >Tanggal Terbit</th>
                      </tr>
                      <tr>
                        <td style="width:3%">a</td>
                        <td style="width:47%">Sertifikasi Tanda Registrasi Kompetensi</td>
                        <td style="width:30%">0002/LPJ/AMDAL-1/LRK/KLH</td>
                        <td style="width:20%">8 Maret 2016</td>
                      </tr>
                    </table>
                  </div>
                </div> -->
              </div>

            </div>
          </div>
        </div>



      </div>
    </section>
@endsection
