@extends('frontend.layouts.front_app2')

@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">
    <div class="section-heading">
      <h2>Struktur Organisasi</h2>
      <div class="breadcrumb-wcb">
        <p>Tentang Kami / Struktur Organisasi</p>
      </div>
    </div>

    <div class="section-content">
      <div class="container-struktur">
        @if(!empty($struktur))
        <a title="{{$struktur->name}}" class="popup-link" href="{{asset('storage/images/struktur/'.$struktur->name)}}">
          <img class="img-strukur-organisasi" src="{{asset('storage/images/struktur/'.$struktur->name)}}" alt="">
        </a>

        @else
        <p>Tidak ada</p>
        @endif
      </div>
    </div>

  </div>
</section>
@endsection

@section('more-js')
<script type="text/javascript">
$('.popup-link').magnificPopup({
  type: 'image',
// other options
  gallery:{
    enabled:true
  },
  image: {
     titleSrc: 'title'
     // this tells the script which attribute has your caption
    //  titleSrc: function(item) {
    //   return item.el.attr('title') + '<small>'+item.el.attr('title')+'</small>';
    // }
 }
});
</script>
@endsection
