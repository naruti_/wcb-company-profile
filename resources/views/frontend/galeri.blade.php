@extends('frontend.layouts.front_app2')


@section('content')
<section id="section-top-pushed" class="section-no_full_page_no_bg">
  <div class="container-wcb">

    <div class="section-heading">
      <h2>Galeri</h2>
      <div class="breadcrumb-wcb">
        <p>Pengalaman / Galeri</p>
      </div>
    </div>

    <div class="section-content">
      <div class="container-galeri">
        <div class="row">
          <!-- <div class="col-md-3 col-6 ">
            <div class="box-img">
              <a class="test-popup-link" href="assets/img/sertifikasi-foto.jpeg">
                <img src="assets/img/sertifikasi-foto.jpeg" alt="">
              </a>
            </div>
          </div> -->
          @foreach($galeries as $gal)
           <div class="col-md-3 col-6">
            <div class="box-img">
              <a title="{{ $gal->desc}}" class="test-popup-link" href="{{asset('storage/images/galeri/'.$gal->img_name)}}">
                <img src="{{asset('storage/images/galeri/'.$gal->img_name)}}" alt="{{$gal->name}}">
              </a>
            </div>
          </div>
          @endforeach
        <!--  <div class="col-md-3 col-6">
            <div class="box-img">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="box-img">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="box-img">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="box-img">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div>
          </div>
          <div class="col-md-3 col-6">
            <div class="box-img">
              <img src="assets/img/sertifikasi-foto.jpeg" alt="">
            </div>
          </div> -->
      </div>


      </div>
    </div>
  </div>
</section>
@endsection
@section('more-js')
<script type="text/javascript">
  $(document).ready(function(){
    $('.test-popup-link').magnificPopup({
      type: 'image',
    // other options
      gallery:{
        enabled:true
      },
      image: {
         titleSrc: 'title'
         // this tells the script which attribute has your caption
        //  titleSrc: function(item) {
        //   return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
        // }
     }
    });
  });
</script>
@endsection
