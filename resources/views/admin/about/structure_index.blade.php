@extends('admin.layouts.admin_app')

@section('content')
        <div class="container-fluid">

          <!-- Modal Delete-->
          <div id="modalDelete" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <form method="POST" action="{{ url('admin/structure/delete') }}">
                  {{ csrf_field() }}
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Kategori</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Apakah Anda yakin akan menghapus kategori ini?</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Ya</button>
                      <button data-dismiss="modal" style="margin: 0 0 10px 0" class="btn pull-right">Tidak</button>
                    </div>
                </div>
              </form>
            </div>
          </div>

          @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          <div class="row">
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header" data-background-color="purple">
                          <h4 class="title">Struktur Organisasi</h4>
                          <p class="category">- Struktur Organisasi berbentuk file gambar dengan format png atau jpg/jpeg</p>
                      </div>
                      <div class="card-content text-center">
                        <br>
                        @if(!empty($struktur))
                        <img src="{{asset('storage/images/struktur/'.$struktur->name)}}" alt="">
                        <hr>
                        <br>
                        <label class="label-wcb" for="img_structure">Update Struktur Organisasi</label>
                        @else
                        <p>Silahkan upload gambar struktur organisasi</p>
                        @endif
                        <form enctype="multipart/form-data" action="{{url('admin/structure/upload')}}" method="post">
                          {{ csrf_field() }}
                          <input class="col-centered" type="file" name="img_structure">
                          <button type="submit" class="btn btn-primary">Upload Struktur Organisasi</button>
                        </form>
                          @if(!empty($struktur))
                          <button data-toggle="modal" data-target="#modalDelete" class="btn btn-danger">Hapus Struktur Organisasi</button>
                          @endif
                      </div>
                  </div>
              </div>
          </div>
      </div>

@endsection
@section('more-js')

@if(Session::has('alert-message'))
<script type="text/javascript">
  $(document).ready(function(){
    var inputType = {{ Session::get('alert-type') }};
    var inputMessage = "<b> {{ Session::get('alert-message') }} </b>";
    var inputIcon = "{{ Session::get('alert-icon') }}";
    notif.showNotification('top','right', inputType, inputMessage, inputIcon);

  });
</script>
@endif
<script type="text/javascript">

  $(document).ready(function(){
    var dropdown_element = $('#nav_structure');
    console.log(dropdown_element);
    dropdown_element.addClass("active");
    var dropdown_element_parent = dropdown_element.parents("li");
    dropdown_element_parent.addClass("active");
    dropdown_element_parent.find("a.dropdown-wcb").addClass("opened");
    var panelScrollHeight = dropdown_element_parent.find("div.dropdown-wcb-container").prop("scrollHeight");
    dropdown_element_parent.find("div.dropdown-wcb-container").css("max-height",panelScrollHeight+1);
    dropdown_element_parent.find("a.dropdown-wcb i.fa").removeClass("fa-caret-down").addClass('fa-caret-up');
  });

</script>
@endsection
