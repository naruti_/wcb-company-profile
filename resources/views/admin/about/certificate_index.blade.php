@extends('admin.layouts.admin_app')

@section('content')
        <div class="container-fluid">
          <!-- Modal Add-->
          <div id="modalAdd" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form method="POST" action="{{ route('admin_addCertificate') }}">
                  {{ csrf_field() }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Sertifikasi</h4>
                  </div>
                  <div class="modal-body">
                      <div class="row">
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Nama Lengkap</label>
                                <input name="name" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Nomor Sertifikat Kompetensi</label>
                                <input name="no_certificate" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label class="control-label">Kualifikasi</label>
                                <input name="qualification" type="text" class="form-control">
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group label-floating">
                                <label class="control-label">KTA/Registrasi</label>
                                <input name="kta" type="text" class="form-control">
                            </div>
                        </div>

                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button  style="margin: 0 15px 10px 10px;" type="submit" class="btn btn-primary pull-right">Tambah</button>

                  </div>
                </form>
              </div>

          </div>
        </div>

        <!-- Modal Edit -->
        <div id="modalEdit" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <form method="POST" action="{{ url('admin/certificate/update/') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Portfolio</h4>
                </div>
                  <div class="modal-body">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group" style="margin-top:12px;">
                                  <label class="control-label">Nama Lengkap</label>
                                  <input name="name" type="text" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group" style="margin-top:12px;">
                                  <label class="control-label">Nomor Sertifikat Kompetensi</label>
                                  <input name="no_certificate" type="text" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group" style="margin-top:12px;">
                                  <label class="control-label">Kualifikasi</label>
                                  <input name="qualification" type="text" class="form-control">
                              </div>
                          </div>
                          <div class="col-md-12">
                              <div class="form-group" style="margin-top:12px;">
                                  <label class="control-label">KTA/Registrasi</label>
                                  <input name="kta" type="text" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Edit</button>
                  </div>
                </form>
              </div>
            </div>
          </div>

          <!-- Modal Delete-->
          <div id="modalDelete" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <form method="POST" action="{{ url('admin/certificate/delete/') }}">
                  {{ csrf_field() }}
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Sertifikasi</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Apakah Anda yakin akan menghapus sertifikasi ini?</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Ya</button>
                      <button data-dismiss="modal" style="margin: 0 0 10px 0" class="btn pull-right">Tidak</button>
                    </div>
                </div>
              </form>
            </div>
          </div>

          @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          <div class="row">
              <div class="col-md-2 col-md-offset-10">
                  <!-- Trigger the modal with a button -->
                <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAdd">Tambah Sertifikasi</button>
              </div>
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header" data-background-color="purple">
                          <h4 class="title">Sertifikasi dan Kompetensi</h4>
                          <!-- <p class="certificate">- Struktur Organisasi berbentuk file gambar dengan format png atau jpg/jpeg</p> -->
                      </div>
                      <div class="card-content table-responsive">
                        <table id="tabel_certificate" class="table">
                            <thead class="text-primary">
                                <th>No</th>
                                <th>Nama Lengkap</th>
                                <th>No Sertifikat Kompetensi</th>
                                <th>Kualifikasi</th>
                                <th>KTA/Registrasi</th>
                                <th>Aksi</th>

                            </thead>
                            <tbody>
                            <?php
                              $i = 1;
                             ?>
                            @foreach($certificates as $certificate)

                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>{{ $certificate->name}}</td>
                                    <td>{{ $certificate->no_certificate}}</td>
                                    <td>{{ $certificate->qualification}}</td>
                                    <td>{{ $certificate->kta}}</td>
                                    <td>
                                      <button class="btn btn-warning btn-sm btn-edit" data-id="{{ $certificate->id }}" type="button" name="button" data-toggle="modal" data-target="#modalEdit"><i class="material-icons">mode_edit</i></button>
                                      <button class="btn btn-danger btn-sm btn-delete" data-id="{{ $certificate->id }}" type="button" name="button" data-toggle="modal" data-target="#modalDelete"><i class="material-icons">delete</i></button>
                                    </td>
                                    <!-- <td class="text-primary">$36,738</td> -->
                                </tr>
                                <?php $i += 1;?>
                              @endforeach
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>

@endsection
@section('more-js')

<script type="text/javascript">
  $(document).ready(function(){
    $('#tabel_certificate').DataTable({
      "pageLength": 10,
      lengthMenu: [[5, 10, 25,100, -1], [5, 10, 25,100, "All"]],
      "language": {
         "lengthMenu": "Menampilkan _MENU_ data per halaman",
         "search":         "Pencarian :",
         "zeroRecords": "Nothing found - sorry",
         "info": "Menampilkan data _START_ hingga _END_ dari _TOTAL_ total data",
         "infoEmpty": "No records available",
         "infoFiltered": "(difilter dari _MAX_ total data)"
          },
    });
  });
</script>

@if(Session::has('alert-message'))
<script type="text/javascript">
  $(document).ready(function(){
    var inputType = {{ Session::get('alert-type') }};
    var inputMessage = "<b> {{ Session::get('alert-message') }} </b>";
    var inputIcon = "{{ Session::get('alert-icon') }}";
    notif.showNotification('top','right', inputType, inputMessage, inputIcon);

  });
</script>
@endif

<script type="text/javascript">

  $(document).ready(function(){
    var dropdown_element = $('#nav_certificate');
    console.log(dropdown_element);
    dropdown_element.addClass("active");
    var dropdown_element_parent = dropdown_element.parents("li");
    dropdown_element_parent.addClass("active");
    dropdown_element_parent.find("a.dropdown-wcb").addClass("opened");
    var panelScrollHeight = dropdown_element_parent.find("div.dropdown-wcb-container").prop("scrollHeight");
    dropdown_element_parent.find("div.dropdown-wcb-container").css("max-height",panelScrollHeight+1);
    dropdown_element_parent.find("a.dropdown-wcb i.fa").removeClass("fa-caret-down").addClass('fa-caret-up');


    var APP_URL = {!! json_encode(url('/')) !!};
    var routeUpdate = $('#modalEdit').find("form").attr("action");
    $(document).on('click','.btn-edit', function () {
      // console.log(APP_URL + 'admin/category/editCategory/1');
      var id = $(this).attr("data-id");
      // console.log(id);
      $.ajax({
        method: 'GET',
        url: APP_URL + '/admin/certificate/edit/'+id,
        success : function(data){
          newRouteUpdate =  routeUpdate + '/' + id;
          console.log(data);
          var id_category = data.id_category;

          $('#modalEdit').find("input[name=name]").attr('value',data.name);
          $('#modalEdit').find("input[name=no_certificate]").attr('value',data.no_certificate);
          $('#modalEdit').find("input[name=qualification]").attr('value',data.qualification);
          $('#modalEdit').find("input[name=kta]").attr('value',data.kta);

          $('#modalEdit').find("form").attr("action",newRouteUpdate);
          console.log(newRouteUpdate);
        }
      });
    });


    var routeDelete = $('#modalDelete').find("form").attr("action");
    $(document).on('click','.btn-delete', function () {
      var id = $(this).attr("data-id");
      // console.log(id);
      newRouteDelete = routeDelete+ '/' + id;
      console.log(newRouteDelete);
      $('#modalDelete').find("form").attr("action",newRouteDelete);
      // console.log(newRouteDelete);

    });
  });

</script>
@endsection
