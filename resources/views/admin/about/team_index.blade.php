@extends('admin.layouts.admin_app')

@section('content')
        <div class="container-fluid">
            <!-- Trigger the modal with a button  btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

          <!-- Modal -->
          <!-- Modal Prevention-->
          <div id="modalPrevention" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Perhatian</h4>
                  </div>
                  <div class="modal-body">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <p>Silahkan isi data kategori terlebih dahulu sebelum mengisi data peraturan lingkungan hidup</p>
                              </div>
                          </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button data-dismiss="modal" style="margin: 0 15px 10px 10px; padding-left: 25px; padding-right:25px" class="btn pull-right">Tutup</button>
                  </div>
              </div>

          </div>
        </div>

          <!-- Modal AddCategory-->
          <div id="modalAddCategory" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form method="POST" action="{{ route('admin_addCategoryTeam') }}">
                  {{ csrf_field() }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Kategori</h4>
                  </div>
                  <div class="modal-body">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group label-floating">
                                  <label class="control-label">Nama Kategori</label>
                                  <input name="name" type="text" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button  style="margin: 0 15px 10px 10px;" type="submit" class="btn btn-primary pull-right">Tambah</button>

                  </div>
                </form>
              </div>

          </div>
        </div>

        <!-- Modal EditCategory -->
        <div id="modalEditCategory" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
              <form method="POST" action="{{ url('/admin/category_team/update/') }}">
                {{ csrf_field() }}
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Edit Kategori</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="control-label">Nama Kategori</label>
                                <input name="name" type="text" class="form-control">
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                  <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Edit</button>

                </div>
              </form>
            </div>

          </div>
        </div>

        <!-- Modal DeleteCategory-->
        <div id="modalDeleteCategory" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Delete Kategori</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p>Apakah Anda yakin akan menghapus kategori ini?</p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                  <a href="{{ url('/admin/category_team/delete/') }}" style="margin: 0 15px 10px 10px; padding-left: 25px; padding-right:25px" type="submit" class="btn btn-danger pull-right">Ya</a>
                  <button data-dismiss="modal" style="margin: 0 0 10px 0" class="btn pull-right">Tidak</button>
                </div>
            </div>

        </div>
      </div>

          <!-- Modal Add-->
          <div id="modalAdd" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form method="POST" action="{{ route('admin_addTeam') }}">
                  {{ csrf_field() }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Anggota Tim</h4>
                  </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama</label>
                                    <input required name="name" type="text" class="form-control">
                                </div>
                            </div>

                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                  <label class="control-label">Kategori</label>
                                  <select required class="form-control" name="id_category_team">
                                    @foreach($categories as $category)
                                      <option value="{{ $category->id }}">{{$category->name}}</option>
                                    @endforeach
                                  </select>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Bidang</label>
                                    <input required name="bidang" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Pendidikan</label>
                                    <input required name="pendidikan" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Tambah</button>

                    </div>
                  </form>
                </div>
              </div>
            </div>

            <!-- Modal Edit -->
            <div id="modalEdit" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <form method="POST" action="{{ url('admin/team/update/') }}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Tambah Anggota Tim</h4>
                    </div>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" style="margin-top:12px;">
                                    <label class="control-label">Nama</label>
                                    <input required name="name" type="text" class="form-control">
                                </div>
                            </div>

                              <div class="col-md-12">
                                  <div class="form-group label-floating">
                                    <label class="control-label">Kategori</label>
                                    <select required class="form-control" name="id_category_team">
                                      @foreach($categories as $category)
                                        <option value="{{ $category->id }}">{{$category->name}}</option>
                                      @endforeach
                                    </select>
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group" style="margin-top:12px;">
                                      <label class="control-label">Bidang</label>
                                      <input required name="bidang" type="text" class="form-control">
                                  </div>
                              </div>
                              <div class="col-md-12">
                                  <div class="form-group" style="margin-top:12px;">
                                      <label class="control-label">Pendidikan</label>
                                      <input required name="pendidikan" type="text" class="form-control">
                                  </div>
                              </div>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Edit</button>

                      </div>
                    </form>
                </div>
              </div>
          </div>

          <!-- Modal Delete-->
          <div id="modalDelete" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <form method="POST" action="{{ url('admin/team/delete/') }}">
                  {{ csrf_field() }}
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Peraturan Lingkungan Hidup</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Apakah Anda yakin akan menghapus peraturan ini?</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Ya</button>
                      <button data-dismiss="modal" style="margin: 0 0 10px 0" class="btn pull-right">Tidak</button>
                    </div>
                </div>
              </form>
            </div>
          </div>

          @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif

          <div class="row">
            @if(Session::has('alert-message'))
          {{--  <div class="{{ Session::get('alert-class') }}">
              <div class="container-fluid">
                <div class="alert-icon">
                <i class="material-icons">{{ Session::get('alert-icon') }}</i>
                </div>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <b> {{ Session::get('alert-message') }}</b>
              </div>
            </div> --}}
            @endif
            <div class="col-md-2 col-md-push-10">
                <!-- Trigger the modal with a button -->
              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAddCategory">Tambah Kategori</button>
            </div>
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header" data-background-color="purple">
                          <h4 class="title">Tabel Kategori</h4>
                          <!-- <p class="category">Daftar kategori yang digunakan untuk mengisi field kategori pada peraturan lingkungan hidup</p> -->
                      </div>
                      <div class="card-content table-responsive">

                        <table id="tabel_category" class="table">
                            <thead class="text-primary">
                                <th>No</th>
                                <th>Nama</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                            <?php
                              $i = 1;
                             ?>
                            @foreach($categories as $category)

                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>{{ $category->name}}</td>
                                    <td>
                                      <button class="btn btn-warning btn-sm btn-editCategory" data-id="{{ $category->id }}" type="button" name="button" data-toggle="modal" data-target="#modalEditCategory"><i class="material-icons">mode_edit</i></button>
                                      <button class="btn btn-danger btn-sm btn-deleteCategory" data-id="{{ $category->id }}" type="button" name="button" data-toggle="modal" data-target="#modalDeleteCategory"><i class="material-icons">delete</i></button>
                                    </td>
                                    <!-- <td class="text-primary">$36,738</td> -->
                                </tr>
                                <?php $i += 1;?>
                              @endforeach
                            </tbody>
                        </table>
                      </div>
                  </div>
              </div>
            <div class="col-md-2 col-md-push-10">
                <!-- Trigger the modal with a button -->
              @if($categories->isEmpty())
              <!-- {{$categories}} -->
              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalPrevention">Tambah Tim</button>
              @else
              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAdd">Tambah Tim</button>
              @endif
            </div>
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header" data-background-color="purple">
                          <h4 class="title">Tabel Tim Ahli</h4>
                          <p class="category"></p>
                          <p class="category"></p>
                      </div>
                      <div class="card-content table-responsive">

                          <table id="tabel_portfolio" class="table">
                              <thead class="text-primary">
                                  <th>No</th>
                                  <th>Nama</th>
                                  <th>Kategori</th>
                                  <th>Bidang</th>
                                  <th>Pendidikan</th>
                                  <th>Aksi</th>
                              </thead>
                              <tbody>
                                <?php
                                  $i = 1;
                                 ?>
                                 @foreach($teams as $team)

                                     <tr>
                                         <td><?php echo $i;?></td>
                                         <td>{{ $team->name}}</td>
                                         <td>{{ $team->category_team()->first()->name }}</td>
                                         <td>{{ $team->bidang}}</td>
                                         <td>{{ $team->pendidikan}}</td>
                                         <td>
                                           <button class="btn btn-warning btn-sm btn-edit" data-id="{{ $team->id}}" type="button" name="button" data-toggle="modal" data-target="#modalEdit"><i class="material-icons">mode_edit</i></button>
                                           <button class="btn btn-danger btn-sm btn-delete" data-id="{{ $team->id}}" type="button" name="button" data-toggle="modal" data-target="#modalDelete"><i class="material-icons">delete</i></button>
                                         </td>
                                         <!-- <td class="text-primary">$36,738</td> -->
                                     </tr>
                                     <?php $i += 1;?>
                                   @endforeach

                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>

@endsection
@section('more-js')
<script type="text/javascript">
  $(document).ready(function(){
    $('#tabel_portfolio, #tabel_category').DataTable({
      "pageLength": 10,
      lengthMenu: [[5, 10, 25,100, -1], [5, 10, 25,100, "All"]],
      "language": {
         "lengthMenu": "Menampilkan _MENU_ data per halaman",
         "search":         "Pencarian :",
         "zeroRecords": "Nothing found - sorry",
         "info": "Menampilkan data _START_ hingga _END_ dari _TOTAL_ total data",
         "infoEmpty": "No records available",
         "infoFiltered": "(difilter dari _MAX_ total data)"
          },
    });

  });
</script>

  @if(Session::has('alert-message'))
  <script type="text/javascript">
    $(document).ready(function(){
      var inputType = {{ Session::get('alert-type') }};
      var inputMessage = "<b> {{ Session::get('alert-message') }} </b>";
      var inputIcon = "{{ Session::get('alert-icon') }}";
      notif.showNotification('top','right', inputType, inputMessage, inputIcon);

    });
  </script>
  @endif

  <script type="text/javascript">
    $(document).ready(function(){
      var dropdown_element = $('#nav_team');
      dropdown_element.addClass("active");
      var dropdown_element_parent = dropdown_element.parents("li");
      dropdown_element_parent.addClass("active");
      dropdown_element_parent.find("a.dropdown-wcb").addClass("opened");
      var panelScrollHeight = dropdown_element_parent.find("div.dropdown-wcb-container").prop("scrollHeight");
      dropdown_element_parent.find("div.dropdown-wcb-container").css("max-height",panelScrollHeight+1);
      dropdown_element_parent.find("a.dropdown-wcb i.fa").removeClass("fa-caret-down").addClass('fa-caret-up');



      var APP_URL = {!! json_encode(url('/')) !!};
      var routeUpdate = $('#modalEdit').find("form").attr("action");
      var routeDelete = $('#modalDelete').find("form").attr("action");
      var urlFile = $('#modalEdit').find("input[name=file_regulation]").prev("a").attr("href");
      // console.log(urlFile);


      $(document).on('click','.btn-edit', function () {
        // console.log(APP_URL + 'admin/category/editCategory/1');
        var id = $(this).attr("data-id");
        // console.log(id);
        $.ajax({
          method: 'GET',
          url: APP_URL + '/admin/team/edit/'+id,
          success : function(data){
            newRouteUpdate =  routeUpdate + '/' + id;
            console.log(data);
            var id_category = data.id_category;

            $('#modalEdit').find("input[name=name]").attr('value',data.name);
            $('#modalEdit').find("input[name=bidang]").attr('value',data.bidang);
            $('#modalEdit').find("input[name=pendidikan]").attr('value',data.pendidikan);

            $('#modalEdit').find("select[name=id_category_team]").val(data.id_category_team);
            $('#modalEdit').find("form").attr("action",newRouteUpdate);
            console.log(newRouteUpdate);
          }
        });
      });

      $(document).on('click','.btn-delete', function () {
        var id = $(this).attr("data-id");
        // console.log(id);
        newRouteDelete = routeDelete+ '/' + id;
        console.log(newRouteDelete);
        $('#modalDelete').find("form").attr("action",newRouteDelete);
        // console.log(newRouteDelete);

      });

      var routeUpdateCategory = $('#modalEditCategory').find("form").attr("action");
      var routeDeleteCategory = $('#modalDeleteCategory').find("a").attr("href");
      console.log(routeUpdateCategory);
      console.log(routeDeleteCategory);

      $(document).on('click','.btn-editCategory', function () {
        // console.log(APP_URL + 'admin/category/editCategory/1');
        var id = $(this).attr("data-id");
        // console.log(id);
        $.ajax({
          method: 'GET',
          url: APP_URL + '/admin/category_team/edit/'+id,
          success : function(data){
            newRouteUpdateCategory =  routeUpdateCategory + '/' + id;
            console.log(data);
            $('#modalEditCategory').find("input[name=name]").val(data.name);
            $('#modalEditCategory').find("form").attr("action",newRouteUpdateCategory);
            console.log(newRouteUpdateCategory);
          }
        });
      });

      $(document).on('click','.btn-deleteCategory', function () {
        var id = $(this).attr("data-id");
        // console.log(id);
        newRouteDeleteCategory = routeDeleteCategory+ '/' + id;
        $('#modalDeleteCategory').find("a").attr("href",newRouteDeleteCategory);
        console.log(newRouteDeleteCategory);

      });

    });
  </script>
@endsection
