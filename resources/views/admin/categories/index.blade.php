@extends('admin.layouts.admin_app')

@section('more-css')
  <link href="{{ asset('dashboard/test/magicsuggest/magicsuggest-min.css') }}" rel="stylesheet">
  <link href="{{ asset('dashboard/test/magicsuggest/magicsuggest-new.css') }}" rel="stylesheet">
@endsection

@section('content')
        <div class="container-fluid">
            <!-- Trigger the modal with a button  btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

            <!-- Modal Add-->
            <div id="modalAdd" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <form method="POST" action="{{ route('admin_addCategory') }}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Tambah Kategori</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama Kategori</label>
                                    <input name="name" type="text" class="form-control">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button  style="margin: 0 15px 10px 10px;" type="submit" class="btn btn-primary pull-right">Tambah</button>

                    </div>
                  </form>
                </div>

            </div>
          </div>

          <!-- Modal Edit -->
          <div id="modalEdit" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form method="POST" action="{{ url('/admin/category/updateCategory/') }}">
                  {{ csrf_field() }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Edit Kategori</h4>
                  </div>
                  <div class="modal-body">
                      <div class="row">
                          <div class="col-md-12">
                              <div class="form-group">
                                  <label class="control-label">Nama Kategori</label>
                                  <input name="name" type="text" class="form-control">
                              </div>
                          </div>
                      </div>
                      <div class="clearfix"></div>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Edit</button>

                  </div>
                </form>
              </div>

          </div>
        </div>

        <!-- Modal Delete-->
        <div id="modalDelete" class="modal fade" role="dialog">
          <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                  <button type="button" class="close" data-dismiss="modal">&times;</button>
                  <h4 class="modal-title">Delete Kategori</h4>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <p>Apakah Anda yakin akan menghapus kategori ini?</p>
                            </div>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="modal-footer">
                  <a href="{{ url('/admin/category/deleteCategory/') }}" style="margin: 0 15px 10px 10px; padding-left: 25px; padding-right:25px" type="submit" class="btn btn-danger pull-right">Ya</a>
                  <button data-dismiss="modal" style="margin: 0 0 10px 0" class="btn pull-right">Tidak</button>
                </div>
            </div>

        </div>
      </div>

        <div class="row">
          <!-- <div class="col-md-12">
            <input class="containerCoba" id="mcoba"/>
          </div> -->
          @if(Session::has('alert-message'))
          <div class="{{ Session::get('alert-class') }}">
            <div class="container-fluid">
              <div class="alert-icon">
            	<i class="material-icons">{{ Session::get('alert-icon') }}</i>
              </div>
              <button type="button" class="close" data-dismiss="alert" aria-label="Close">
            	<span aria-hidden="true"><i class="material-icons">clear</i></span>
              </button>
              <b> {{ Session::get('alert-message') }}</b>
            </div>
          </div>
          @endif
          <div class="col-md-2 col-md-offset-10">
              <!-- Trigger the modal with a button -->
            <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAdd">Tambah Kategori</button>
          </div>
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header" data-background-color="purple">
                        <h4 class="title">Tabel Kategori</h4>
                        <p class="category">Kategori yang tersedia</p>
                    </div>
                    <div class="card-content table-responsive">
                        <table class="table">
                            <thead class="text-primary">
                                <th>No</th>
                                <th>Nama</th>
                                <th>Tanggal Diperbaharui</th>
                                <th>Aksi</th>
                            </thead>
                            <tbody>
                            <?php
                              $i = 1;
                             ?>
                            @foreach($categories as $category)

                                <tr>
                                    <td><?php echo $i;?></td>
                                    <td>{{ $category->name}}</td>
                                    <td>{{ $category->updated_at}}</td>
                                    <td>
                                      <button class="btn btn-warning btn-sm btn-edit" data-id="{{ $category->id }}" type="button" name="button" data-toggle="modal" data-target="#modalEdit"><i class="material-icons">mode_edit</i></button>
                                      <button class="btn btn-danger btn-sm btn-delete" data-id="{{ $category->id }}" type="button" name="button" data-toggle="modal" data-target="#modalDelete"><i class="material-icons">delete</i></button>
                                    </td>
                                    <!-- <td class="text-primary">$36,738</td> -->
                                </tr>
                                <?php $i += 1;?>
                              @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
      </div>

@endsection
@section('more-js')

  @if(Session::has('alert-message'))
  <script type="text/javascript">
    $(document).ready(function(){
      var inputType = {{ Session::get('alert-type') }};
      var inputMessage = "<b> {{ Session::get('alert-message') }} </b>";
      var inputIcon = "{{ Session::get('alert-icon') }}";
      notif.showNotification('top','right', inputType, inputMessage, inputIcon);

    });
  </script>
  @endif

  <script type="text/javascript">
    $(document).ready(function(){
      $('#nav_categories').addClass("active");
      var APP_URL = {!! json_encode(url('/')) !!};
      var routeUpdate = $('#modalEdit').find("form").attr("action");
      var routeDelete = $('#modalDelete').find("form").attr("action");

      $(document).on('click','.btn-edit', function () {
        // console.log(APP_URL + 'admin/category/editCategory/1');
        var id = $(this).attr("data-id");
        // console.log(id);
        $.ajax({
          method: 'GET',
          url: APP_URL + '/admin/category/editCategory/'+id,
          success : function(data){
            newRouteUpdate =  routeUpdate + '/' + id;
            console.log(data);
            $('#modalEdit').find("input[name=name]").val(data.name);
            $('#modalEdit').find("form").attr("action",newRouteUpdate);
            console.log(newRouteUpdate);
          }
        });
      });

      $(document).on('click','.btn-delete', function () {
        var id = $(this).attr("data-id");
        // console.log(id);
        newRouteDelete = routeDelete+ '/' + id;
        $('#modalDelete').find("form").attr("action",newRouteDelete);
        // console.log(newRouteDelete);

      });

    });
    //   $('#nav_categories').addClass("active");
    //   console.log($('.ms-res-ctn').attr("style"));
    //   $('.ms-res-ctn').removeAttr("style");
    //   $('#mcoba').magicSuggest({
    //     // allowFreeEntries: false,
    //     cls : 'containerCoba',
    //     minChars: 2,
    //     minCharsRenderer: function(v){
    //       return 'Be more precise Spiderman!!';
    //     },
    //     data: ['Paris', 'New York', 'Gotham','test1','test2','test3','test4','test5','test6','test7','test8','test9','test10'],
    //     maxDropHeight: 100
    //   });
  </script>
@endsection
