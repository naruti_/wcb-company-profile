@extends('admin.layouts.admin_app')

@section('content')
        <div class="container-fluid">
            <!-- Trigger the modal with a button  btn-lg" data-toggle="modal" data-target="#myModal">Open Modal</button>

            <!-- Modal -->
          <!-- Modal Add-->
          <div id="modalAdd" class="modal fade" role="dialog">
            <div class="modal-dialog">

              <!-- Modal content-->
              <div class="modal-content">
                <form enctype="multipart/form-data" method="POST" action="{{ route('admin_addGallery') }}">
                  {{ csrf_field() }}
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Tambah Gambar</h4>
                  </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Nama Gambar</label>
                                    <input required name="name" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="">
                                    <label class="control-label">Gambar</label>
                                    <input required type="file" name="img_gallery">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group label-floating">
                                    <label class="control-label">Deskripsi Singkat</label>
                                    <textarea required name="desc" type="text" class="form-control"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Tambah</button>

                    </div>
                  </form>
                </div>
              </div>
            </div>

            <!-- Modal Edit -->
            <div id="modalEdit" class="modal fade" role="dialog">
              <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                  <form enctype="multipart/form-data" method="POST" action="{{ url('admin/gallery/update/') }}">
                    {{ csrf_field() }}
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Edit Gambar</h4>
                    </div>
                      <div class="modal-body">
                          <div class="row">
                            <div class="col-md-12">
                                <div class="form-group" style="margin-top:12px;">
                                    <label class="control-label">Nama Paket Pekerjaan</label>
                                    <input name="name" type="text" class="form-control">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="">
                                    <label class="control-label">File</label>
                                    <img id="imageEdit" src="" alt="">
                                    <input type="file" name="img_gallery">
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="form-group">
                                    <label class="control-label">Deskripsi Singkat</label>
                                    <textarea required name="desc" type="text" class="form-control"></textarea>
                                </div>
                            </div>
                          </div>
                          <div class="clearfix"></div>
                      </div>
                      <div class="modal-footer">
                        <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Edit</button>

                      </div>
                    </form>
                  </div>

            </div>
          </div>

          <!-- Modal Delete-->
          <div id="modalDelete" class="modal fade" role="dialog">
            <div class="modal-dialog">
              <form method="POST" action="{{ url('admin/gallery/delete/') }}">
                  {{ csrf_field() }}
                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                      <button type="button" class="close" data-dismiss="modal">&times;</button>
                      <h4 class="modal-title">Delete Gambar</h4>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="form-group">
                                    <p>Apakah Anda yakin akan menghapus Gambar ini?</p>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="modal-footer">
                      <button type="submit" style="margin: 0 15px 10px 10px;" class="btn btn-primary pull-right">Ya</button>
                      <button data-dismiss="modal" style="margin: 0 0 10px 0" class="btn pull-right">Tidak</button>
                    </div>
                </div>
              </form>
            </div>
          </div>
          @if ($errors->any())
            <div class="alert alert-danger">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                  <span aria-hidden="true"><i class="material-icons">clear</i></span>
                </button>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
          @endif
          <div class="row">
            <div class="col-md-2 col-md-push-10">
                <!-- Trigger the modal with a button -->
              <button class="btn btn-primary pull-right" data-toggle="modal" data-target="#modalAdd">Tambah Gambar</button>
            </div>
              <div class="col-md-12">
                  <div class="card">
                      <div class="card-header" data-background-color="purple">
                          <h4 class="title">Tabel Galeri</h4>
                      </div>
                      <div class="card-content table-responsive">
                          <table id="tabel_portfolio" class="table table-wcb">
                              <thead class="text-primary">
                                  <th>No</th>
                                  <th>Nama Gambar</th>
                                  <th>Gambar</th>
                                  <th>Deskripsi</th>
                                  <th>Aksi</th>
                              </thead>
                              <tbody>
                                <?php
                                  $i = 1;
                                 ?>
                                 @foreach($galeri as $gal)

                                     <tr>
                                         <td><?php echo $i;?></td>
                                         <td>{{ $gal->name}}</td>
                                         <td>
                                           <a title="{{ $gal->desc}}"href="{{asset('storage/images/galeri/'.$gal->img_name)}}" class="popup-link" href="#">
                                             <img src="{{asset('storage/images/galeri/'.$gal->img_name)}}" alt=""> </td>
                                           </a>
                                         <td>{{ $gal->desc}}</td>
                                         <td>
                                           <button class="btn btn-warning btn-sm btn-edit" data-img ="{{$gal->img_name}}" data-id="{{ $gal->id }}" type="button" name="button" data-toggle="modal" data-target="#modalEdit"><i class="material-icons">mode_edit</i></button>
                                           <button class="btn btn-danger btn-sm btn-delete" data-id="{{ $gal->id }}" type="button" name="button" data-toggle="modal" data-target="#modalDelete"><i class="material-icons">delete</i></button>
                                         </td>
                                         <!-- <td class="text-primary">$36,738</td> -->
                                     </tr>
                                     <?php $i += 1;?>
                                   @endforeach
                              </tbody>
                          </table>
                      </div>
                  </div>
              </div>
          </div>
      </div>

@endsection
@section('more-js')
<script type="text/javascript">
  $(document).ready(function(){
    $('#tabel_portfolio').DataTable({
      "pageLength": 10,
      lengthMenu: [[5, 10, 25,100, -1], [5, 10, 25,100, "All"]],
      "language": {
         "lengthMenu": "Menampilkan _MENU_ data per halaman",
         "search":         "Pencarian :",
         "zeroRecords": "Nothing found - sorry",
         "info": "Menampilkan data _START_ hingga _END_ dari _TOTAL_ total data",
         "infoEmpty": "No records available",
         "infoFiltered": "(difilter dari _MAX_ total data)"
          },
    });
  });
</script>

  @if(Session::has('alert-message'))
  <script type="text/javascript">
    $(document).ready(function(){
      var inputType = {{ Session::get('alert-type') }};
      var inputMessage = "<b> {{ Session::get('alert-message') }} </b>";
      var inputIcon = "{{ Session::get('alert-icon') }}";
      notif.showNotification('top','right', inputType, inputMessage, inputIcon);

    });
  </script>
  @endif

  <script type="text/javascript">
    $(document).ready(function(){
      var dropdown_element = $('#nav_gallery');
      dropdown_element.addClass("active");
      var dropdown_element_parent = dropdown_element.parents("li");
      dropdown_element_parent.addClass("active");
      dropdown_element_parent.find("a.dropdown-wcb").addClass("opened");
      var panelScrollHeight = dropdown_element_parent.find("div.dropdown-wcb-container").prop("scrollHeight");
      dropdown_element_parent.find("div.dropdown-wcb-container").css("max-height",panelScrollHeight+1);
      dropdown_element_parent.find("a.dropdown-wcb i.fa").removeClass("fa-caret-down").addClass('fa-caret-up');



      var APP_URL = {!! json_encode(url('/')) !!};
      var routeUpdate = $('#modalEdit').find("form").attr("action");
      var routeDelete = $('#modalDelete').find("form").attr("action");

      var imageUpdate = {!! json_encode(asset('storage/images/galeri/')) !!};
      // console.log(imageUpdate);


      $(document).on('click','.btn-edit', function () {
        // console.log(APP_URL + 'admin/category/editCategory/1');
        var id = $(this).attr("data-id");
        var imgName = $(this).attr("data-img");

        // console.log(id);
        $.ajax({
          method: 'GET',
          url: APP_URL + '/admin/gallery/edit/'+id,
          success : function(data){
            newRouteUpdate =  routeUpdate + '/' + id;
            newImageUpdate =  imageUpdate + '/' + imgName;

            console.log(data);
            var id_category = data.id_category;

            $('#modalEdit').find("input[name=name]").attr('value',data.name);
            $('#modalEdit').find("textarea[name=desc]").val(data.desc);
            // $('#modalEdit').find("input[name=name]").attr('value',data.name);
            // $('#modalEdit').attr("selected",true);

            $('#modalEdit').find("form").attr("action",newRouteUpdate);
            $('#imageEdit').attr("src",newImageUpdate);
            // console.log(newRouteUpdate);
            // console.log(newImageUpdate);

          }
        });
      });

      $(document).on('click','.btn-delete', function () {
        var id = $(this).attr("data-id");
        // console.log(id);
        newRouteDelete = routeDelete+ '/' + id;
        // console.log(newRouteDelete);
        $('#modalDelete').find("form").attr("action",newRouteDelete);
        // console.log(newRouteDelete);

      });

      $('.popup-link').magnificPopup({
        type: 'image',
      // other options
        gallery:{
          enabled:true
        },
        image: {
           titleSrc: 'title'
           // this tells the script which attribute has your caption
          //  titleSrc: function(item) {
          //   return item.el.attr('title') + '<small>'+item.el.attr('title')+'</small>';
          // }
       }
      });

    });
  </script>
@endsection
