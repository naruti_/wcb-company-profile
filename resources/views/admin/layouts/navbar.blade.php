<div class="sidebar" data-color="purple" data-image="{{ asset('dashboard/img/sidebar-8.jpg') }}">
    <!--
Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

Tip 2: you can also add an image using data-image tag
-->

    <div class="logo">
        <a href="http://www.creative-tim.com" class="simple-text">
            PT. Widya Cipta Buana
        </a>
    </div>
    <div class="sidebar-wrapper">
        <ul class="nav">
            <li id="nav_dashboard">
                <a href="{{route('admin_dashboard')}}">
                    <i class="material-icons">dashboard</i>
                    <p>Dashboard</p>
                </a>
            </li>
            <li id="nav_group_tentang">
                <a class="dropdown-wcb">
                    <i class="material-icons">people</i>
                    <p>Tentang Kami<i class="fa fa-right fa-caret-down"></i></p>
                </a>
                <div class="dropdown-wcb-container">
                  <a id="nav_structure" class="" href="{{url('/admin/structure')}}">Struktur Organisasi</a>
                  <a id="nav_certificate" href="{{url('/admin/certificate')}}">Sertifikasi & Kompetensi</a>
                  <a id="nav_legal" href="{{route('admin_legal')}}">Legalitas</a>
                  <a id="nav_team" href="{{route('admin_team')}}">Tim Ahli</a>
                  <a href="#">Partner</a>
                </div>
            </li>
            <!-- <li id="nav_dashboard">
                <a class="dropdown-wcb">
                    <i class="material-icons">business</i>
                    <p>Bisnis <i class="fa fa-right fa-caret-down"></i></p>
                </a>
                <div class="dropdown-wcb-container">
                  <a href="#">Partner</a>
                </div>
            </li> -->
            <li id="nav_group_pengalaman">
                <a class="dropdown-wcb">
                    <i class="material-icons">explore</i>
                    <p>Pengalaman <i class="fa fa-right fa-caret-down"></i></p>
                </a>
                <div class="dropdown-wcb-container">
                  <a id="nav_gallery" href="{{url('/admin/gallery')}}">Galeri</a>
                  <a id="nav_project" href="{{url('/admin/portfolio')}}">Proyek</a>
                  <!-- <a id="nav_portfolio" href="#">Portofolio</a> -->
                  <a id="nav_regulation" href="{{route('admin_regulation')}}">Peraturan Lingkungan Hidup</a>
                </div>
            </li>


            <!-- <li class="active-pro">
                <a href="upgrade.html">
                    <i class="material-icons">unarchive</i>
                    <p>Upgrade to PRO</p>
                </a>
            </li> -->
        </ul>
    </div>
</div>
