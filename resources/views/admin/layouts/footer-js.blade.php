<!--   Core JS Files   -->
  <script src="{{ asset('dashboard/js/jquery-3.2.1.min.js') }}" type="text/javascript"></script>
<!-- DataTables-->
  <script type="text/javascript" src="{{ asset('dashboard/datatables/datatables.js') }}"></script>
  <!-- APP PROJECT JS -->
    <script src="{{ asset('dashboard/js/app-wcb.js') }}"></script>

  <script src="{{ asset('dashboard/js/bootstrap.min.js') }}" type="text/javascript"></script>
  <script src="{{ asset('dashboard/js/material.min.js') }}" type="text/javascript"></script>
<!--  Charts Plugin -->
  <script src="{{ asset('dashboard/js/chartist.min.js') }}"></script>
<!--  Dynamic Elements plugin -->
  <script src="{{ asset('dashboard/js/arrive.min.js') }}"></script>
<!--  PerfectScrollbar Library -->
  <script src="{{ asset('dashboard/js/perfect-scrollbar.jquery.min.js') }}"></script>
<!--  Notifications Plugin    -->
  <script src="{{ asset('dashboard/js/bootstrap-notify.js') }}"></script>
<!--  Google Maps Plugin    -->
  <!-- <script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=YOUR_KEY_HERE"></script> -->
<!-- Material Dashboard javascript methods -->
  <script src="{{ asset('dashboard/js/material-dashboard.js?v=1.2.0') }}"></script>
<!-- Magnific Pop Up -->
<script type="text/javascript" src="{{asset('vendor/magnific-popup/magnific-popup.min.js')}}"></script>
<!--  -->
