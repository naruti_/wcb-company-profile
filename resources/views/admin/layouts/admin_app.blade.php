<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Admin WCB</title>

    @include('admin.layouts.header-css')
    @yield('more-css')
    <!-- Styles -->
    <!-- <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->

</head>
<body>
  <div class="wrapper">
    @include('admin.layouts.navbar')
    <div class="main-panel">
      @include('admin.layouts.navtop')
      <div class="content">
        @yield('content')
      </div>
      @include('admin.layouts.navfoot')
    </div>

  </div>


</body>
@include('admin.layouts.footer-js')
@yield('more-js')
</html>
