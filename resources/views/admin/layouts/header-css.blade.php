<link href="{{ asset('dashboard/css/bootstrap.min.css') }}" rel="stylesheet" />
<!--  Material Dashboard CSS    -->
<link href="{{ asset('dashboard/css/material-dashboard.css?v=1.2.0') }}" rel="stylesheet" />
<!--  CSS for Demo Purpose, don't include it in your project     -->
<link href="{{ asset('dashboard/css/demo.css') }}" rel="stylesheet" />
<!--     Fonts and iconssssss     -->
<link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>

<!-- FontAwesome -->
<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css" rel="stylesheet">

<!-- DataTables -->
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard/datatables/datatables.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('dashboard/datatables/new-datatables-styles.css') }}">

<!-- Magnific Pop Up -->
<link rel="stylesheet" type="text/css" href="{{ asset('vendor/magnific-popup/magnific-popup.css') }}">
