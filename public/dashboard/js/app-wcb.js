type = ['', 'info', 'success', 'warning', 'danger'];


notif = {

    showNotification: function(from, align, inputType, inputMessage, inputIcon) {
        // color = Math.floor((Math.random() * 4) + 1);

        $.notify({
            icon: inputIcon,
            message: inputMessage

        }, {
            type: type[inputType],
            timer: 5000,
            placement: {
                from: from,
                align: align
            }
        });
    }

}


$(document).ready(function() {
  $('.dropdown-wcb').on('click', function(){
    if($(this).hasClass("opened")){
      $(this).removeClass("opened");
      $(this).next().css("max-height",0);
      $(this).find('i.fa').removeClass("fa-caret-up").addClass('fa-caret-down');
    }
    else{
      $(this).addClass("opened");
      var panelScrollHeight = $(this).next().prop("scrollHeight");
      $(this).next().css("max-height",panelScrollHeight+1);
      $(this).find('i.fa').removeClass("fa-caret-down").addClass('fa-caret-up');
    }

  });

});
